<?php

/**
 * IMPORTS
 */
require_once 'MySQL_PST.php';

/**
 * FACTORY TO CHOOSE WHICH SGDATABASE TO HANDLE(MYSQL, SQL SEVER, ORACLE, etc)
 */
class FactoryDataBasePersistence {

    static function persistData($SGDB, $db_server, $db_database, $db_username, $db_password) {

        if ($SGDB == "mysql") {

            $sgdb = new MySQL_PST($db_server, $db_database, $db_username, $db_password);
            return $sgdb;
        }
    }

}
