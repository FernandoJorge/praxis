<?php

/**
 * CLASS THAT MODELS A PROPOSAL OF AN OFFER
 */
class Proposal {
    
    // PROPOSAL ATTRIBUTES
    private $proposal_title = null;
    private $proposal_description = null;
    private $proposal_study_areas = null;
    private $proposal_study_degrees = null;
    private $proposal_targets = null;
    private $proposal_languages = null;
    private $proposal_country = null;
    private $proposal_city = null;
    private $proposal_contact = null;
    private $proposal_supervisor_fst_name = null;
    private $proposal_supervisor_lst_name = null;
    private $proposal_supervisor_email = null;
    private $proposal_duration = null;
    private $proposal_start_date = null;
    private $proposal_end_date = null;
    private $proposal_vacancies = null;
    private $proposal_payment = null;
    private $proposal_global_skills = null;
    private $proposal_job_skills = null;
    private $proposal_required_skills = null;
    private $proposal_promoted_skills = null;
    private $proposal_benefits = null;
    private $proposal_valid_until = null;
    
    // PROPOSAL METHODS
    function setProposalTitle($title) {

        $this->proposal_title = $title;
    }

    function setProposalDescription($description) {

        $this->proposal_description = $description;
    }

    function setProposalStudyAreas($areas) {

        $this->proposal_study_areas = $areas;
    }

    function setProposalStudyDegrees($degrees) {

        $this->proposal_study_degrees = $degrees;
    }

    function setProposalTargets($targets) {

        $this->proposal_targets = $targets;
    }

    function setProposalLanguages($languages) {

        $this->proposal_languages = $languages;
    }

    function setProposalCountry($country) {

        $this->proposal_country = $country;
    }

    function setProposalCity($city) {

        $this->proposal_city = $city;
    }

    function setProposalContact($contact) {

        $this->proposal_contact = $contact;
    }

    function setProposalSupervisorFstName($fstName) {

        $this->proposal_supervisor_fst_name = $fstName;
    }

    function setProposalSupervisorLstName($lstName) {

        $this->proposal_supervisor_lst_name = $lstName;
    }

    function setProposalSupervisorEmail($email) {

        $this->proposal_supervisor_email = $email;
    }

    function setProposalDuration($duration) {

        $this->proposal_duration = $duration;
    }

    function setProposalStartDate($date) {

        $this->proposal_start_date = $date;
    }

    function setProposalEndDate($date) {

        $this->proposal_end_date = $date;
    }

    function setProposalVacancies($vacancies) {

        $this->proposal_vacancies = $vacancies;
    }

    function setProposalPayment($payment) {

        $this->proposal_payment = $payment;
    }

    function setProposalGlobalSkills($skills) {

        $this->proposal_global_skills = $skills;
    }

    function setProposalJobSkills($skills) {

        $this->proposal_job_skills = $skills;
    }

    function setProposalRequiredSkills($skills) {

        $this->proposal_required_skills = $skills;
    }

    function setProposalPromotedSkills($skills) {

        $this->proposal_promoted_skills = $skills;
    }

    function setProposalBenefits($benefits) {

        $this->proposal_benefits = $benefits;
    }

    function setProposalValidUntil($date) {

        $this->proposal_valid_until = $date;
    }

    function getProposalTitle() {

        return $this->proposal_title;
    }

    function getProposalDescription() {

        return $this->proposal_description;
    }

    function getProposalStudyAreas() {

        return $this->proposal_study_areas;
    }

    function getProposalStudyDegrees() {

        return $this->proposal_study_degrees;
    }

    function getProposalTargets() {

        return $this->proposal_targets;
    }

    function getProposalLanguages() {

        return $this->proposal_languages;
    }

    function getProposalCountry() {

        return $this->proposal_country;
    }

    function getProposalCity() {

        return $this->proposal_city;
    }

    function getProposalContact() {

        return $this->proposal_contact;
    }

    function getProposalSupervisorFstName() {

        return $this->proposal_supervisor_fst_name;
    }

    function getProposalSupervisorLstName() {

        return $this->proposal_supervisor_lst_name;
    }

    function getProposalSupervisorEmail() {

        return $this->proposal_supervisor_email;
    }

    function getProposalDuration() {

        return $this->proposal_duration;
    }

    function getProposalStartDate() {

        return $this->proposal_start_date;
    }

    function getProposalEndDate() {

        return $this->proposal_end_date;
    }

    function getProposalVacancies() {

        return $this->proposal_vacancies;
    }

    function getProposalPayment() {

        return $this->proposal_payment;
    }

    function getProposalGlobalSkills() {

        return $this->proposal_global_skills;
    }

    function getProposalJobSkills() {

        return $this->proposal_job_skills;
    }

    function getProposalRequiredSkills() {

        return $this->proposal_required_skills;
    }

    function getProposalPromotedSkills() {

        return $this->proposal_promoted_skills;
    }

    function getProposalBenefits() {

        return $this->proposal_benefits;
    }

    function getProposalValidUntil() {

        return $this->proposal_valid_until;
    }
}
