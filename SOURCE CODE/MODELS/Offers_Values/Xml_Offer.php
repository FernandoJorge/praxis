<?php

require_once 'OfferValues.php';
require_once '/../Offer/Offer.php';

/**
 * CLASS OF SERVICE TO MANAGE XML OFFER DOCUMENT´S DOM
 */
class Xml_Offer implements OfferValues {
    /**
     * PUBLIC METHODS
     */

    /**
     * 
     * GETS ALL THE ELEMENT´S NODES VALUES OF THE OFFER
     * 
     * @param type $xmlPath - Path of the Xml document
     * @return Offer - Offer object with all values of the offer
     */
    function getOfferFieldsValues($xmlPath, $schemaDocPath = '') {

        $offerNodesValues = null;
        $offer = new Offer();

        // LOAD FILE AND POINTS TO THE ROOT ELEMENT
        $offerRoot = simplexml_load_file($xmlPath);

        // GETS ALL NODES´ VALUES OF THE OFFER´S PROPOSAL
        $offerNodesValues = self::getOfferProposalNodesValues($offerRoot);
        $offer->getProposal()->setProposalTitle($offerNodesValues['title']);
        $offer->getProposal()->setProposalDescription($offerNodesValues['description']);
        $offer->getProposal()->setProposalStudyAreas($offerNodesValues['study_areas']);
        $offer->getProposal()->setProposalStudyDegrees($offerNodesValues['study_degrees']);
        $offer->getProposal()->setProposalTargets($offerNodesValues['targets']);
        $offer->getProposal()->setProposalLanguages($offerNodesValues['languages']);
        $offer->getProposal()->setProposalCountry($offerNodesValues['country']);
        $offer->getProposal()->setProposalCity($offerNodesValues['city']);
        $offer->getProposal()->setProposalContact($offerNodesValues['contact']);
        $offer->getProposal()->setProposalSupervisorFstName($offerNodesValues['supervisor_first_name']);
        $offer->getProposal()->setProposalSupervisorLstName($offerNodesValues['supervisor_last_name']);
        $offer->getProposal()->setProposalSupervisorEmail($offerNodesValues['supervisor_email']);
        $offer->getProposal()->setProposalDuration($offerNodesValues['duration']);
        $offer->getProposal()->setProposalStartDate($offerNodesValues['start_date']);
        $offer->getProposal()->setProposalEndDate($offerNodesValues['end_date']);
        $offer->getProposal()->setProposalVacancies($offerNodesValues['vacancies']);
        $offer->getProposal()->setProposalPayment($offerNodesValues['payment']);
        $offer->getProposal()->setProposalGlobalSkills($offerNodesValues['global_skills']);
        $offer->getProposal()->setProposalJobSkills($offerNodesValues['job_skills']);
        $offer->getProposal()->setProposalRequiredSkills($offerNodesValues['required_skills']);
        $offer->getProposal()->setProposalPromotedSkills($offerNodesValues['promoted_skills']);
        $offer->getProposal()->setProposalBenefits($offerNodesValues['benefits']);
        $offer->getProposal()->setProposalValidUntil($offerNodesValues['valid_until']);

        // GETS ALL NODES´ VALUES OF THE OFFER´S INSTITUTION
        $offerNodesValues = self::getOfferInstitutionNodesValues($offerRoot);
        $offer->getInstitution()->setInstitutionName($offerNodesValues['name']);
        $offer->getInstitution()->setInstitutionDescription($offerNodesValues['description']);
        $offer->getInstitution()->setInstitutionType($offerNodesValues['type']);
        $offer->getInstitution()->setInstitutionContactFstName($offerNodesValues['contact_first_name']);
        $offer->getInstitution()->setInstitutionContactLstName($offerNodesValues['contact_last_name']);
        $offer->getInstitution()->setInstitutionCountry($offerNodesValues['country']);
        $offer->getInstitution()->setInstitutionCity($offerNodesValues['city']);
        $offer->getInstitution()->setInstitutionPhone($offerNodesValues['phone']);
        $offer->getInstitution()->setInstitutionEmail($offerNodesValues['email']);
        $offer->getInstitution()->setInstitutionWebsite($offerNodesValues['website']);

        // GETS ALL NODES´VALUES OF THE OFFER´S HOST
        $offerNodesValues = self::getOfferHostNodesValues($offerRoot);
        $offer->getHost()->setInstitutionName($offerNodesValues['name']);
        $offer->getHost()->setInstitutionDescription($offerNodesValues['description']);
        $offer->getHost()->setInstitutionType($offerNodesValues['type']);
        $offer->getHost()->setInstitutionContactFstName($offerNodesValues['contact_first_name']);
        $offer->getHost()->setInstitutionContactLstName($offerNodesValues['contact_last_name']);
        $offer->getHost()->setInstitutionCountry($offerNodesValues['country']);
        $offer->getHost()->setInstitutionCity($offerNodesValues['city']);
        $offer->getHost()->setInstitutionPhone($offerNodesValues['phone']);
        $offer->getHost()->setInstitutionEmail($offerNodesValues['email']);
        $offer->getHost()->setInstitutionWebsite($offerNodesValues['website']);

        return $offer;
    }

    /**
     * AUXILIAR METHODS
     */

    /**
     * 
     * GETS ALL THE ELEMENT NODES´ VALUES OF OFFER´S PROPOSAL
     * 
     * @param type $initialRootSearch - Initial element root to begin the search
     * @return array - Values of the element´s nodes
     */
    private function getOfferProposalNodesValues($initialRootSearch) {

        $offerValues;

        // SINGLE 'TITLE' NODE´S VALUE
        $values = self::searchNode($initialRootSearch, "PROPOSAL_TITLE");

        if (count($values) > 0) {

            $offerValues['title'] = $values[0];
        } else {

            $offerValues['title'] = NULL;
        }

        // SINGLE 'DESCRIPTION' NODE´S VALUE
        $values = self::searchNode($initialRootSearch, "PROPOSAL_DESCRIPTION");

        if (count($values) > 0) {

            $offerValues['description'] = $values[0];
        } else {

            $offerValues['description'] = NULL;
        }

        // VARIOUS 'STUDY AREAS' NODES´ VALUES
        $values = self::searchNode($initialRootSearch, "PROPOSAL_STUDY_AREA");
        $offerValues['study_areas'] = $values;

        // VARIOUS 'STUDY DEGREES' NODES´ VALUES
        $values = self::searchNode($initialRootSearch, "PROPOSAL_STUDY_DEGREE");
        $offerValues['study_degrees'] = $values;

        // VARIOUS 'TARGETS' NODES´ VALUES
        $values = self::searchNode($initialRootSearch, "PROPOSAL_TARGET");
        $offerValues['targets'] = $values;

        // VARIOUS 'LANGUAGES' NODES´ VALUES
        $values = self::searchNode($initialRootSearch, "PROPOSAL_LANGUAGE");
        $offerValues['languages'] = $values;

        // SINGLE 'COUNTRY' NODE´S VALUE
        $values = self::searchNode($initialRootSearch, "PROPOSAL_COUNTRY");

        if (count($values) > 0) {

            $offerValues['country'] = $values[0];
        } else {

            $offerValues['country'] = NULL;
        }

        // SINGLE 'CITY' NODE´S VALUE
        $values = self::searchNode($initialRootSearch, "PROPOSAL_CITY");

        if (count($values) > 0) {

            $offerValues['city'] = $values[0];
        } else {

            $offerValues['city'] = NULL;
        }

        // SINGLE 'CONTACT' NODE´S VALUE
        $values = self::searchNode($initialRootSearch, "PROPOSAL_CONTACT");

        if (count($values) > 0) {

            $offerValues['contact'] = $values[0];
        } else {

            $offerValues['contact'] = NULL;
        }

        // SINGLE 'SUPERVISOR FIRST NAME' NODE´S VALUE
        $values = self::searchNode($initialRootSearch, "PROPOSAL_SUPERVISORFSTNAME");

        if (count($values) > 0) {

            $offerValues['supervisor_first_name'] = $values[0];
        } else {

            $offerValues['supervisor_first_name'] = NULL;
        }

        // SINGLE 'SUPERVISOR LAST NAME' NODE´S VALUE
        $values = self::searchNode($initialRootSearch, "PROPOSAL_SUPERVISORLSTNAME");

        if (count($values) > 0) {

            $offerValues['supervisor_last_name'] = $values[0];
        } else {

            $offerValues['supervisor_last_name'] = NULL;
        }

        // SINGLE 'SUPERVISOR EMAIL' NODE´S VALUE
        $values = self::searchNode($initialRootSearch, "PROPOSAL_SUPERVISOREMAIL");

        if (count($values) > 0) {

            $offerValues['supervisor_email'] = $values[0];
        } else {

            $offerValues['supervisor_email'] = NULL;
        }

        // SINGLE 'DURATION' NODE´S VALUE
        $values = self::searchNode($initialRootSearch, "PROPOSAL_DURATION");

        if (count($values) > 0) {

            $offerValues['duration'] = $values[0];
        } else {

            $offerValues['duration'] = NULL;
        }

        // SINGLE 'START DATE' NODE´S VALUE
        $values = self::searchNode($initialRootSearch, "PROPOSAL_STARTDATE");

        if (count($values) > 0) {

            $offerValues['start_date'] = $values[0];
        } else {

            $offerValues['start_date'] = NULL;
        }

        // SINGLE 'END DATE' NODE´S VALUE
        $values = self::searchNode($initialRootSearch, "PROPOSAL_ENDDATE");

        if (count($values) > 0) {

            $offerValues['end_date'] = $values[0];
        } else {

            $offerValues['end_date'] = NULL;
        }

        // SINGLE 'VACANCIES' NODE´S VALUE
        $values = self::searchNode($initialRootSearch, "PROPOSAL_VACANCIES");

        if (count($values) > 0) {

            $offerValues['vacancies'] = $values[0];
        } else {

            $offerValues['vacancies'] = NULL;
        }

        // SINGLE 'PAYMENT' NODE´S VALUE
        $values = self::searchNode($initialRootSearch, "PROPOSAL_PAYMENT");

        if (count($values) > 0) {

            $offerValues['payment'] = $values[0];
        } else {

            $offerValues['payment'] = NULL;
        }

        // VARIOUS 'GLOBAL SKILLS' NODES'  VALUES
        $values = self::searchNode($initialRootSearch, "PROPOSAL_GLOBALSKILL");
        
        if(count($values) == 1){
        
            $offerValues['global_skills'] = $values[0];
        }else{
            
            $offerValues['global_skills'] = implode(";", $values);
        }
        

        // VARIOUS 'JOB SKILLS' NODES´ VALUES
        $values = self::searchNode($initialRootSearch, "PROPOSAL_JOBSKILL");
        
        if(count($values) == 1){
            
            $offerValues['job_skills'] = $values[0];
        }else{
            
            $offerValues['job_skills'] = implode(";", $values);
        }
        

        // VARIOUS 'REQUIRED SKILLS' NODES´ VALUES
        $values = self::searchNode($initialRootSearch, "PROPOSAL_REQUIREDSKILL");
        $offerValues['required_skills'] = $values;

        // VARIOUS 'PROMOTED SKILLS' NODES´ VALUES
        $values = self::searchNode($initialRootSearch, "PROPOSAL_PROMOTEDSKILL");
        $offerValues['promoted_skills'] = $values;

        // VARIOUS 'BENEFITS' NODES´ VALUES
        $values = self::searchNode($initialRootSearch, "PROPOSAL_BENEFIT");
        
        if(count($values) == 1){
            
            $offerValues['benefits'] = $values[0];
        }else{
            
            $offerValues['benefits'] = implode(";", $values);
        }
        
        
        // SINGLE 'VALID UNTIL DATE' NODE´S VALUE
        $values = self::searchNode($initialRootSearch, "PROPOSAL_VALIDUNTIL");

        if (count($values) > 0) {

            $offerValues['valid_until'] = $values[0];
        } else {

            $offerValues['valid_until'] = NULL;
        }

        return $offerValues;
    }

    /**
     * 
     * GETS ALL THE ELEMENT NODES´ VALUES OF OFFER´S INSTITUTION
     * 
     * @param type $initialRootSearch - Initial element root to begin the search
     * @return array - Values of the element´s nodes
     */
    private function getOfferInstitutionNodesValues($initialRootSearch) {

        $offerValues;

        // SINGLE 'NAME' NODE´S VALUE
        $values = self::searchNode($initialRootSearch, "INSTITUTION_NAME");

        if (count($values) > 0) {

            $offerValues['name'] = $values[0];
        } else {

            $offerValues['name'] = NULL;
        }

        // SINGLE 'DESCRIPTION' NODE´S VALUE
        $values = self::searchNode($initialRootSearch, "INSTITUTION_DESCRIPTION");

        if (count($values) > 0) {

            $offerValues['description'] = $values[0];
        } else {

            $offerValues['description'] = NULL;
        }

        // SINGLE 'TYPE' NODE´S VALUE
        $values = self::searchNode($initialRootSearch, "INSTITUTION_TYPE");

        if (count($values) > 0) {

            $offerValues['type'] = $values[0];
        } else {

            $offerValues['type'] = NULL;
        }


        // SINGLE 'CONTACT FIRST NAME' NODE´S VALUE
        $values = self::searchNode($initialRootSearch, "INSTITUTION_CONTACTFSTNAME");

        if (count($values) > 0) {

            $offerValues['contact_first_name'] = $values[0];
        } else {

            $offerValues['contact_first_name'] = NULL;
        }

        // SINGLE 'CONTACT LAST NAME' NODE´S VALUE
        $values = self::searchNode($initialRootSearch, "INSTITUTION_CONTACTLSTNAME");

        if (count($values) > 0) {

            $offerValues['contact_last_name'] = $values[0];
        } else {

            $offerValues['contact_last_name'] = NULL;
        }

        // SINGLE 'COUNTRY' NODE´S VALUE
        $values = self::searchNode($initialRootSearch, "INSTITUTION_COUNTRY");

        if (count($values) > 0) {

            $offerValues['country'] = $values[0];
        } else {

            $offerValues['country'] = NULL;
        }

        // SINGLE 'CITY' NODE´S VALUE
        $values = self::searchNode($initialRootSearch, "INSTITUTION_CITY");

        if (count($values) > 0) {

            $offerValues['city'] = $values[0];
        } else {

            $offerValues['city'] = NULL;
        }

        // SINGLE 'PHONE' NODE´S VALUE
        $values = self::searchNode($initialRootSearch, "INSTITUTION_PHONE");

        if (count($values) > 0) {

            $offerValues['phone'] = $values[0];
        } else {

            $offerValues['phone'] = NULL;
        }

        // SINGLE 'EMAIL' NODE´S VALUE
        $values = self::searchNode($initialRootSearch, "INSTITUTION_EMAIL");

        if (count($values) > 0) {

            $offerValues['email'] = $values[0];
        } else {

            $offerValues['email'] = NULL;
        }

        // SINGLE 'WEB SITE' NODE´S VALUE
        $values = self::searchNode($initialRootSearch, "INSTITUTION_WEBSITE");

        if (count($values) > 0) {

            $offerValues['website'] = $values[0];
        } else {

            $offerValues['website'] = NULL;
        }

        return $offerValues;
    }

    /**
     * 
     * GETS ALL THE ELEMENT NODES´ VALUES OF OFFER´S HOST
     * 
     * @param type $initialRootSearch - Initial element root to begin the search
     * @return array - Values of the element´s nodes
     */
    private function getOfferHostNodesValues($initialRootSearch) {

        $offerValues;

        // SINGLE 'NAME' NODE´S VALUE
        $values = self::searchNode($initialRootSearch, "HOST_NAME");

        if (count($values) > 0) {

            $offerValues['name'] = $values[0];
        } else {

            $offerValues['name'] = NULL;
        }

        // SINGLE 'DESCRIPTION' NODE´S VALUE
        $values = self::searchNode($initialRootSearch, "HOST_DESCRIPTION");

        if (count($values) > 0) {

            $offerValues['description'] = $values[0];
        } else {

            $offerValues['description'] = NULL;
        }

        // SINGLE 'TYPE' NODE´S VALUE
        $values = self::searchNode($initialRootSearch, "HOST_TYPE");

        if (count($values) > 0) {

            $offerValues['type'] = $values[0];
        } else {

            $offerValues['type'] = NULL;
        }

        // SINGLE 'CONTACT FIRST NAME' NODE´S VALUE
        $values = self::searchNode($initialRootSearch, "HOST_CONTACTFSTNAME");

        if (count($values) > 0) {

            $offerValues['contact_first_name'] = $values[0];
        } else {

            $offerValues['contact_first_name'] = NULL;
        }

        // SINGLE 'CONTACT LAST NAME' NODE´S VALUE
        $values = self::searchNode($initialRootSearch, "HOST_CONTACTLSTNAME");

        if (count($values) > 0) {

            $offerValues['contact_last_name'] = $values[0];
        } else {

            $offerValues['contact_last_name'] = NULL;
        }

        // SINGLE 'COUNTRY' NODE´S VALUE
        $values = self::searchNode($initialRootSearch, "HOST_COUNTRY");

        if (count($values) > 0) {

            $offerValues['country'] = $values[0];
        } else {

            $offerValues['country'] = NULL;
        }

        // SINGLE 'CITY' NODE´S VALUE
        $values = self::searchNode($initialRootSearch, "HOST_CITY");

        if (count($values) > 0) {

            $offerValues['city'] = $values[0];
        } else {

            $offerValues['city'] = NULL;
        }

        // SINGLE 'PHONE' NODE´S VALUE
        $values = self::searchNode($initialRootSearch, "HOST_PHONE");

        if (count($values) > 0) {

            $offerValues['phone'] = $values[0];
        } else {

            $offerValues['phone'] = NULL;
        }

        // SINGLE 'EMAIL' NODE´S VALUE
        $values = self::searchNode($initialRootSearch, "HOST_EMAIL");

        if (count($values) > 0) {

            $offerValues['email'] = $values[0];
        } else {

            $offerValues['email'] = NULL;
        }

        // SINGLE 'WEB SITE' NODE´S VALUE
        $values = self::searchNode($initialRootSearch, "HOST_WEBSITE");

        if (count($values) > 0) {

            $offerValues['website'] = $values[0];
        } else {

            $offerValues['website'] = NULL;
        }

        return $offerValues;
    }

    /**
     * 
     * MAKES A VERTICAL SEARCH OF THE VALUE OF AN ELEMENT´S NODE
     * 
     * @param type $initialRootSearch - Initial element root to begin the search
     * @param type $nodeToSearch - Name of the element´s node to search
     * @return array - All the values from the element´s node serached
     */
    private function searchNode($initialRootSearch, $nodeToSearch) {

        $values1 = array();

        // VERTICAL SEARCH
        foreach ($initialRootSearch->children() as $child) {

            if ($child->getName() == $nodeToSearch) {

                array_push($values1, trim($child));
            } else {

                $values2 = self::searchNode($child, $nodeToSearch);
                array_merge($values1, $values2);
            }
        }

        return $values1;
    }

}

?>
