function authorize() {

    $("#editorAuthentication").ajaxSubmit({
        url: 'editor',
        method: 'POST',
        datatype: 'json',
        success: function (msg) {

            if (msg['result'] == 'failed') {

                $("#username").val("");
                $("#password").val("");

                $("#modal-message-header").html("Authentication&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>Authentication failed...</p>");

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'authorizationError') {

                $("#username").val("");
                $("#password").val("");

                $("#modal-message-header").html("Authentication&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>It occurred an error while tryng to authenticate...</p>");

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'authorized') {

                document.location.href = "http://" + location.host + "/PRAXIS/PROVIDERS";
            }
        },
        error: function (xhr, status, error) {

            console.log(" xhr.responseText: " + xhr.responseText + " \n\nSTATUS:\n " + status + " \n\nERROR:\n " + error);

            $("#modal-message-header").html("Request error&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
            $("#modal-message-content").html("<p>It was not possible to make the request...</p>");

            $("#message").modal({
                zIndex: 9999
            });
            $("#message").modal('show');
        }
    });
}


