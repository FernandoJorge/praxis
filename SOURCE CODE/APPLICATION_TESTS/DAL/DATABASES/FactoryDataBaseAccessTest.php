<?php

require_once '../../../DAL/DATABASES/FactoryDataBaseAccess.php';
require_once '../../../DAL/DATABASES/DataBaseAccess.php';

class FactoryDataBaseAccessTest extends PHPUnit_Framework_TestCase {

    public function testGetSGDBConnection() {

        echo "testGetSGDBConnection(1)\n";
        $this->assertInstanceOf('DataBaseAccess', FactoryDataBaseAccess::getSGDBConnection("mysql", "localhost:3306", "praxis", "root", ""));
        echo "testGetSGDBConnection(2)\n";
        $this->assertNotInstanceOf('DataBaseAccess', FactoryDataBaseAccess::getSGDBConnection("oracle", "localhost:3306", "praxis", "root", ""));
        echo "testGetSGDBConnection(3)\n";
        $this->assertNotInstanceOf('DataBaseAccess', FactoryDataBaseAccess::getSGDBConnection("msserver", "localhost:3306", "praxis", "root", ""));
        echo "testGetSGDBConnection(4)\n";
        $this->assertNotInstanceOf('DataBaseAccess', FactoryDataBaseAccess::getSGDBConnection("sqlserver", "localhost:3306", "praxis", "root", ""));

        echo "testGetSGDBConnection(OK!)\n\n";
    }

}
