<?php

/**
 * IMPORTS
 */
require_once 'DataBasePersistence.php';
require_once '/../../Offers_Values/Xml_Offer.php';
require_once '/../../../LOGS/Logs.php';
require_once '/../../../DAL/DATABASES/FactoryDataBaseAccess.php';
require_once '/../../Offer/Offer.php';

/**
 * CLASS TO PERSIST THE OFFER´S VALUES IN MYSQL DATABASE
 */
class MySQL_PST implements DataBasePersistence {

    private $mysqlDB;

    /**
     * PUBLIC METHODS
     */

    /**
     * 
     * CONSTRUCTOR
     * 
     * @param String $db_server - SGDB server
     * @param String $db_database - Database´s name
     * @param String $db_username - Database´s username
     * @param String $db_password - Database´s password
     */
    function __construct($db_server, $db_database, $db_username, $db_password) {

        // INSTANCIATES A MYSQL DATABASE ACCESS HANDLER
        $this->mysqlDB = FactoryDataBaseAccess::getSGDBConnection("mysql", $db_server, $db_database, $db_username, $db_password);
    }

    function saveOffer($OfferValues) {

        // SAVES WHOLE OFFER IN THE MYSQL DATABASE
        // SAVES INSTITUTION OFFER´S DATA
        $institution;

        if (!empty($OfferValues->getInstitution()->getInstitutionCountry())) {

            $SQL = "SELECT id FROM country WHERE TRIM(name) = '" . $OfferValues->getInstitution()->getInstitutionCountry() . "'";
            $info = $this->mysqlDB->select($SQL);

            if (!$info && !is_array($info)) {

                $this->mysqlDB->closeDBConnection();
                Logs::writeLog("FAILED TO INSERT NEW PRAXIS OFFER´S INSTITUTION RECORD INTO 'INSTITUTION' TABLE OF 'PRAXIS' DATABASE", true);
                return array("result" => 'saveOfferError');
            }

            $institution['country_id'] = (int) $info[0]['id'];
        } else {
            $institution['country_id'] = "NULL";
        }

        if (!empty($OfferValues->getInstitution()->getInstitutionCity())) {

            $SQL = "SELECT id FROM city WHERE TRIM(name) = '" . $OfferValues->getInstitution()->getInstitutionCity() . "'";
            $info = $this->mysqlDB->select($SQL);

            if (!$info && !is_array($info)) {

                $this->mysqlDB->closeDBConnection();
                Logs::writeLog("FAILED TO INSERT NEW PRAXIS OFFER´S INSTITUTION RECORD INTO 'INSTITUTION' TABLE OF 'PRAXIS' DATABASE", true);
                return array("result" => 'saveOfferError');
            }

            $institution['city_id'] = (int) $info[0]['id'];
        } else {

            $institution['city_id'] = "NULL";
        }

        if (!empty($OfferValues->getInstitution()->getInstitutionType())) {

            $SQL = "SELECT id FROM institutiontype WHERE TRIM(name) = '" . $OfferValues->getInstitution()->getInstitutionType() . "'";
            $info = $this->mysqlDB->select($SQL);

            if (!$info && !is_array($info)) {

                $this->mysqlDB->closeDBConnection();
                Logs::writeLog("FAILED TO INSERT NEW PRAXIS OFFER´S INSTITUTION RECORD INTO 'INSTITUTION' TABLE OF 'PRAXIS' DATABASE", true);
                return array("result" => 'saveOfferError');
            }

            $institution['type_id'] = $info[0]['id'];
        } else {

            $institution['type_id'] = "NULL";
        }

        if (!empty($OfferValues->getInstitution()->getInstitutionPhone())) {

            $institution['phone'] = $OfferValues->getInstitution()->getInstitutionPhone();
        } else {

            $institution['phone'] = "NULL";
        }

        if (!empty($OfferValues->getInstitution()->getInstitutionEmail())) {

            $institution['email'] = "'" . $OfferValues->getInstitution()->getInstitutionEmail() . "'";
        } else {

            $institution['email'] = "NULL";
        }

        if (!empty($OfferValues->getInstitution()->getInstitutionName())) {

            $institution['name'] = "'" . $OfferValues->getInstitution()->getInstitutionName() . "'";
        } else {

            $institution['name'] = "NULL";
        }

        if (!empty($OfferValues->getInstitution()->getInstitutionContactFstName())) {

            $institution['contact_first_name'] = "'" . $OfferValues->getInstitution()->getInstitutionContactFstName() . "'";
        } else {

            $institution['contact_first_name'] = "NULL";
        }

        if (!empty($OfferValues->getInstitution()->getInstitutionContactLstName())) {

            $institution['contact_last_name'] = "'" . $OfferValues->getInstitution()->getInstitutionContactLstName() . "'";
        } else {

            $institution['contact_last_name'] = "NULL";
        }

        if (!empty($OfferValues->getInstitution()->getInstitutionWebsite())) {

            $institution['website'] = "'" . $OfferValues->getInstitution()->getInstitutionWebsite() . "'";
        } else {

            $institution['website'] = "NULL";
        }

        $SQL = "INSERT INTO institution "
                . "( "
                . "country_id , "
                . "city_id , "
                . "type_id , "
                . "phone , "
                . "email , "
                . "name , "
                . "contactFirstName , "
                . "contactLastName , "
                . "website ) "
                . "VALUES ( "
                . "" . $institution['country_id'] . " , "
                . "" . $institution['city_id'] . " , "
                . "" . $institution['type_id'] . " , "
                . "" . $institution['phone'] . " , "
                . "" . $institution['email'] . " , "
                . "" . $institution['name'] . " , "
                . "" . $institution['contact_first_name'] . " , "
                . "" . $institution['contact_last_name'] . " , "
                . "" . $institution['website'] . " )";

        $institutionID = $this->mysqlDB->insert($SQL);

        if (!$institutionID) {

            $this->deleteOfferData($this->mysqlDB);
            Logs::writeLog("FAILED TO INSERT NEW PRAXIS OFFER´S INSTITUTION RECORD INTO 'INSTITUTION' TABLE OF 'PRAXIS' DATABASE", true);
            return array("result" => 'saveOfferError');
        } else {

            $this->mysqlDB->commitDBChanges();
            Logs::writeLog("NEW PRAXIS OFFER´S INSTITUTION RECORD INSERTED INTO 'INSTITUTION' TABLE OF 'PRAXIS' DATABASE");
        }

        // SAVES HOST OFFER´S DATA
        $host;

        if (!empty($OfferValues->getHost()->getInstitutionCountry())) {

            $SQL = "SELECT id FROM country WHERE TRIM(name) = '" . $OfferValues->getHost()->getInstitutionCountry() . "'";
            $info = $this->mysqlDB->select($SQL);

            if (!$info && !is_array($info)) {

                $this->mysqlDB->rollbackDBChanges();
                $this->mysqlDB->closeDBConnection();
                Logs::writeLog("FAILED TO INSERT NEW PRAXIS OFFER´S HOST RECORD INTO 'INSTITUTION' TABLE OF 'PRAXIS_DB' DATABASE", true);
                return array("result" => 'saveOfferError');
            }

            $host['country_id'] = $info[0]['id'];
        } else {
            $host['country_id'] = "NULL";
        }

        if (!empty($OfferValues->getHost()->getInstitutionCity())) {

            $SQL = "SELECT id FROM city WHERE TRIM(name) = '" . $OfferValues->getHost()->getInstitutionCity() . "'";
            $info = $this->mysqlDB->select($SQL);

            if (!$info && !is_array($info)) {

                $this->mysqlDB->rollbackDBChanges();
                $this->mysqlDB->closeDBConnection();
                Logs::writeLog("FAILED TO INSERT NEW PRAXIS OFFER´S HOST RECORD INTO 'INSTITUTION' TABLE OF 'PRAXIS_DB' DATABASE", true);
                return array("result" => 'saveOfferError');
            }

            $host['city_id'] = $info[0]['id'];
        } else {
            $host['city_id'] = "NULL";
        }

        if (!empty($OfferValues->getHost()->getInstitutionType())) {

            $SQL = "SELECT id FROM institutiontype WHERE TRIM(name) = '" . $OfferValues->getHost()->getInstitutionType() . "'";
            $info = $this->mysqlDB->select($SQL);

            if (!$info && !is_array($info)) {

                $this->mysqlDB->rollbackDBChanges();
                $this->mysqlDB->closeDBConnection();
                Logs::writeLog("FAILED TO INSERT NEW PRAXIS OFFER´S HOST RECORD INTO 'INSTITUTION' TABLE OF 'PRAXIS_DB' DATABASE", true);
                return array("result" => 'saveOfferError');
            }

            $host['type_id'] = $info[0]['id'];
        } else {
            $host['type_id'] = "NULL";
        }

        if (!empty($OfferValues->getHost()->getInstitutionPhone())) {

            $host['phone'] = $OfferValues->getHost()->getInstitutionPhone();
        } else {

            $host['phone'] = "NULL";
        }

        if (!empty($OfferValues->getHost()->getInstitutionEmail())) {

            $host['email'] = "'" . $OfferValues->getHost()->getInstitutionEmail() . "'";
        } else {

            $host['email'] = "NULL";
        }

        if (!empty($OfferValues->getHost()->getInstitutionName())) {

            $host['name'] = "'" . $OfferValues->getHost()->getInstitutionName() . "'";
        } else {

            $host['name'] = "NULL";
        }

        if (!empty($OfferValues->getHost()->getInstitutionContactFstName())) {

            $host['contact_first_name'] = "'" . $OfferValues->getHost()->getInstitutionContactFstName() . "'";
        } else {

            $host['contact_first_name'] = "NULL";
        }

        if (!empty($OfferValues->getHost()->getInstitutionContactLstName())) {

            $host['contact_last_name'] = "'" . $OfferValues->getHost()->getInstitutionContactLstName() . "'";
        } else {

            $host['contact_last_name'] = "NULL";
        }

        if (!empty($OfferValues->getHost()->getInstitutionWebsite())) {

            $host['website'] = "'" . $OfferValues->getHost()->getInstitutionWebsite() . "'";
        } else {

            $host['website'] = "NULL";
        }

        $SQL = "INSERT INTO institution "
                . "( "
                . "country_id , "
                . "city_id , "
                . "type_id , "
                . "phone , "
                . "email , "
                . "name , "
                . "contactFirstName , "
                . "contactLastName , "
                . "website ) "
                . "VALUES ( "
                . "" . $host['country_id'] . " , "
                . "" . $host['city_id'] . " , "
                . "" . $host['type_id'] . " , "
                . "" . $host['phone'] . " , "
                . "" . $host['email'] . " , "
                . "" . $host['name'] . " , "
                . "" . $host['contact_first_name'] . " , "
                . "" . $host['contact_last_name'] . " , "
                . "" . $host['website'] . " )";

        $hostID = $this->mysqlDB->insert($SQL);

        if (!$hostID) {

            $this->deleteOfferData($this->mysqlDB, $institutionID);
            Logs::writeLog("FAILED TO INSERT NEW PRAXIS OFFER´S HOST RECORD INTO 'INSTITUTION' TABLE OF 'PRAXIS_DB' DATABASE", true);
            return array("result" => 'saveOfferError');
        } else {

            $this->mysqlDB->commitDBChanges();
            Logs::writeLog("NEW PRAXIS OFFER`S HOST RECORD INSERTED INTO 'INSTITUTION' TABLE OF 'PRAXIS_DB' DATABASE");
        }

        // SAVES PROPOSAL OFFER´S DATA
        $proposal;

        if (!empty($OfferValues->getProposal()->getProposalCountry())) {

            $SQL = "SELECT id FROM country WHERE TRIM(name) = '" . $OfferValues->getProposal()->getProposalCountry() . "'";
            $info = $this->mysqlDB->select($SQL);

            if (!$info && !is_array($info)) {

                $this->deleteOfferData($this->mysqlDB, $institutionID, $hostID);
                Logs::writeLog("FAILED TO INSERT NEW PRAXIS OFFER´S PROPOSAL RECORD INTO 'PROPOSAL' TABLE OF 'PRAXIS' DATABASE", true);
                return array("result" => 'saveOfferError');
            }

            $proposal['country_id'] = $info[0]['id'];
        } else {
            $proposal['country_id'] = "NULL";
        }

        if (!empty($OfferValues->getProposal()->getProposalCity())) {

            $SQL = "SELECT id FROM city WHERE TRIM(name) = '" . $OfferValues->getProposal()->getProposalCity() . "'";
            $info = $this->mysqlDB->select($SQL);

            if (!$info && !is_array($info)) {

                $this->deleteOfferData($this->mysqlDB, $institutionID, $hostID);
                Logs::writeLog("FAILED TO INSERT NEW PRAXIS OFFER´S PROPOSAL RECORD INTO 'PROPOSAL' TABLE OF 'PRAXIS' DATABASE", true);
                return array("result" => 'saveOfferError');
            }

            $proposal['city_id'] = $info[0]['id'];
        } else {
            $proposal['city_id'] = "NULL";
        }

        if (!empty($OfferValues->getProposal()->getProposalGlobalSkills())) {

            $proposal['global_skills'] = "'" . $OfferValues->getProposal()->getProposalGlobalSkills() . "'";
        } else {
            $proposal['global_skills'] = "NULL";
        }

        if (!empty($OfferValues->getProposal()->getProposalJobSkills())) {

            $proposal['job_skills'] = "'" . $OfferValues->getProposal()->getProposalJobSkills() . "'";
        } else {
            $proposal['job_skills'] = "NULL";
        }

        if ($OfferValues->getProposal()->getProposalBenefits() != null) {

            $proposal['benefits'] = "'" . $OfferValues->getProposal()->getProposalBenefits() . "'";
        } else {
            $proposal['benefits'] = "NULL";
        }

        if (!empty($OfferValues->getProposal()->getProposalSupervisorFstName())) {

            $proposal['supervisor_first_name'] = "'" . $OfferValues->getProposal()->getProposalSupervisorFstName() . "'";
        } else {
            $proposal['supervisor_first_name'] = "NULL";
        }

        if (!empty($OfferValues->getProposal()->getProposalSupervisorLstName())) {

            $proposal['supervisor_last_name'] = "'" . $OfferValues->getProposal()->getProposalSupervisorLstName() . "'";
        } else {
            $proposal['supervisor_last_name'] = "NULL";
        }

        if (!empty($OfferValues->getProposal()->getProposalSupervisorEmail())) {

            $proposal['supervisor_email'] = "'" . $OfferValues->getProposal()->getProposalSupervisorEmail() . "'";
        } else {
            $proposal['supervisor_email'] = "NULL";
        }

        if (!empty($OfferValues->getProposal()->getProposalTitle())) {

            $proposal['title'] = "'" . $OfferValues->getProposal()->getProposalTitle() . "'";
        } else {
            $proposal['title'] = "NULL";
        }

        if (!empty($OfferValues->getProposal()->getProposalDescription())) {

            $proposal['description'] = "'" . $OfferValues->getProposal()->getProposalDescription() . "'";
        } else {
            $proposal['description'] = "NULL";
        }

        if (!empty($OfferValues->getProposal()->getProposalStartDate())) {

            $proposal['start_date'] = "'" . $OfferValues->getProposal()->getProposalStartDate() . "'";
        } else {
            $proposal['start_date'] = "NULL";
        }

        if (!empty($OfferValues->getProposal()->getProposalEndDate())) {

            $proposal['end_date'] = "'" . $OfferValues->getProposal()->getProposalEndDate() . "'";
        } else {
            $proposal['end_date'] = "NULL";
        }

        if (!empty($OfferValues->getProposal()->getProposalPayment())) {

            $proposal['payment'] = "'" . $OfferValues->getProposal()->getProposalPayment() . "'";
        } else {
            $proposal['payment'] = "NULL";
        }

        if (!empty($OfferValues->getProposal()->getProposalVacancies())) {

            $proposal['vacancies'] = "'" . $OfferValues->getProposal()->getProposalVacancies() . "'";
        } else {
            $proposal['vacancies'] = "NULL";
        }

        if (!empty($OfferValues->getProposal()->getProposalContact())) {

            $proposal['contact'] = "'" . $OfferValues->getProposal()->getProposalContact() . "'";
        } else {
            $proposal['contact'] = "NULL";
        }

        if (!empty($OfferValues->getProposal()->getProposalDuration())) {

            $proposal['duration'] = $OfferValues->getProposal()->getProposalDuration();
        } else {
            $proposal['duration'] = "NULL";
        }

        if (!empty($OfferValues->getProposal()->getProposalValidUntil())) {

            $proposal['valid_until'] = "'" . $OfferValues->getProposal()->getProposalValidUntil() . "'";
        } else {
            $proposal['valid_until'] = "CURDATE() + INTERVAL + 6 MONTH";
        }

        $SQL = "INSERT INTO proposal "
                . "( "
                . "host_id , "
                . "country_id , "
                . "city_id , "
                . "institutiontype_id , "
                . "institution_id , "
                . "supervisorFirstName , "
                . "supervisorLastName , "
                . "supervisorEmail , "
                . "title , "
                . "description , "
                . "startDate , "
                . "endDate , "
                . "submitDate , "
                . "paid , "
                . "vacancies , "
                . "providedBy_id , "
                . "visible , "
                . "globalSkills , "
                . "jobRelatedSkills , "
                . "benefits , "
                . "contact , "
                . "duration , "
                . "applyOnPraxis , "
                . "validTo ) "
                . "VALUES ( "
                . "" . $hostID . " , "
                . "" . $proposal['country_id'] . " , "
                . "" . $proposal['city_id'] . " , "
                . "" . $institution['type_id'] . " , "
                . "" . $institutionID . " , "
                . "" . $proposal['supervisor_first_name'] . " , "
                . "" . $proposal['supervisor_last_name'] . " , "
                . "" . $proposal['supervisor_email'] . " , "
                . "" . $proposal['title'] . " , "
                . "" . $proposal['description'] . " , "
                . "" . $proposal['start_date'] . " , "
                . "" . $proposal['end_date'] . " , "
                . "CURDATE() , "
                . "" . $proposal['payment'] . " , "
                . "" . $proposal['vacancies'] . " , "
                . "" . $_SESSION['providerID'] . " , "
                . "1 , "
                . "" . $proposal['global_skills'] . " , "
                . "" . $proposal['job_skills'] . " , "
                . "" . $proposal['benefits'] . " , "
                . "" . $proposal['contact'] . " , "
                . "" . $proposal['duration'] . " , "
                . "1 , "
                . "" . $proposal['valid_until'] . ")";

        $proposalID = $this->mysqlDB->insert($SQL);

        if (!$proposalID) {

            // DELETES PREVIOUS INSTITUTION AND HOST RECORDS CREATED IN THE DATABASE
            $this->deleteOfferData($this->mysqlDB, $institutionID, $hostID);
            Logs::writeLog("FAILED TO INSERT NEW PRAXIS OFFER´S PROPOSAL RECORD INTO 'PROPOSAL' TABLE OF 'PRAXIS' DATABASE", true);
            return array("result" => 'saveOfferError');
        } else {

            $this->mysqlDB->commitDBChanges();
            Logs::writeLog("NEW PRAXIS OFFER`S PROPOSAL RECORD INSERTED INTO 'PROPOSAL' TABLE OF 'PRAXIS' DATABASE");
        }

        // SAVES PROPOSAL´S LANGUAGES IF THERE´S SOME
        if (count($OfferValues->getProposal()->getProposalLanguages()) > 0) {

            foreach ($OfferValues->getProposal()->getProposalLanguages() as $language) {

                if (!empty($language)) {

                    $SQL = "SELECT id FROM language WHERE TRIM(name) = '" . $language . "'";
                    $languages = $this->mysqlDB->select($SQL);

                    if (!$languages && !is_array($languages)) {

                        $this->deleteOfferData($this->mysqlDB, $institutionID, $hostID, $proposalID);
                        Logs::writeLog("FAILED TO INSERT NEW PRAXIS OFFER´S LANGUAGE RECORD INTO 'PROPOSAL_LANGUAGE' TABLE OF 'PRAXIS' DATABASE", true);
                        return array("result" => 'saveOfferError');
                    }

                    $SQL = "INSERT INTO proposal_language "
                            . "( "
                            . "proposal_id , "
                            . "language_id ) "
                            . "VALUES ( "
                            . "" . $proposalID . " , "
                            . "" . (int) $languages[0]['id'] . " )";

                    $languageID = $this->mysqlDB->insert($SQL, false);

                    if (!$languageID) {

                        // DELETES PREVIOUS APPLICANT RECORD CREATED IN THE DATABASE
                        // DELETES PREVIOUS INSTITUTION AND HOST RECORDS CREATED IN THE DATABASE
                        // DELETES PREVIOUS PROPOSAL RECORD CREATED IN THE DATABASE
                        $this->deleteOfferData($this->mysqlDB, $institutionID, $hostID, $proposalID);
                        Logs::writeLog("FAILED TO INSERT NEW PRAXIS OFFER´S LANGUAGE RECORD INTO 'PROPOSAL_LANGUAGE' TABLE OF 'PRAXIS' DATABASE", true);
                        return array("result" => 'saveOfferError');
                    } else {

                        $this->mysqlDB->commitDBChanges();
                        Logs::writeLog("NEW PRAXIS OFFER`S LANGUAGE RECORD INSERTED INTO 'PROPOSAL_LANGUAGE' TABLE OF 'PRAXIS' DATABASE");
                    }
                }
            }
        }

        // SAVES STUDY DEGREES IF THERE`S SOME
        if (count($OfferValues->getProposal()->getProposalStudyDegrees()) > 0) {

            foreach ($OfferValues->getProposal()->getProposalStudyDegrees() as $degree) {

                if (!empty($degree)) {

                    $SQL = "SELECT id FROM studydegree WHERE TRIM(name) = '" . $degree . "'";
                    $degrees = $this->mysqlDB->select($SQL);

                    if (!$degrees && !is_array($degrees)) {

                        $this->deleteOfferData($this->mysqlDB, $institutionID, $hostID, $proposalID);
                        Logs::writeLog("FAILED TO INSERT NEW PRAXIS OFFER´S STUDY DEGREE RECORD INTO 'PROPOSAL_STUDYDEGREE' TABLE OF 'PRAXIS' DATABASE", true);
                        return array("result" => 'saveOfferError');
                    }

                    $SQL = "INSERT INTO proposal_studydegree "
                            . "( "
                            . "proposal_id , "
                            . "studydegree_id ) "
                            . "VALUES ( "
                            . "" . $proposalID . " , "
                            . "" . (int) $degrees[0]['id'] . " )";

                    $degreeID = $this->mysqlDB->insert($SQL, false);

                    if (!$degreeID) {

                        // DELETES PREVIOUS APPLICANT RECORD CREATED IN THE DATABASE
                        // DELETES PREVIOUS LANGUAGE RECORDS IN THE DATABASE IF THERE ARE SOME
                        // DELETES PREVIOUS INSTITUTION AND HOST RECORDS CREATED IN THE DATABASE
                        // DELETES PREVIOUS PROPOSAL RECORD CREATED IN THE DATABASE
                        $this->deleteOfferData($this->mysqlDB, $institutionID, $hostID, $proposalID);
                        Logs::writeLog("FAILED TO INSERT NEW PRAXIS OFFER´S STUDY DEGREE RECORD INTO 'PROPOSAL_STUDYDEGREE' TABLE OF 'PRAXIS' DATABASE", true);
                        return array("result" => 'saveOfferError');
                    } else {

                        $this->mysqlDB->commitDBChanges();
                        Logs::writeLog("NEW PRAXIS OFFER`S STUDY DEGREE RECORD INSERTED INTO 'PROPOSAL_STUDYDEGREE' TABLE OF 'PRAXIS' DATABASE");
                    }
                }
            }
        }

        // SAVES STUDY AREAS IF THERE ARE SOME
        if (count($OfferValues->getProposal()->getProposalStudyAreas()) > 0) {

            foreach ($OfferValues->getProposal()->getProposalStudyAreas() as $area) {

                if (!empty($area)) {

                    $SQL = "SELECT id FROM studyarea WHERE TRIM(name) = '" . $area . "'";
                    $areas = $this->mysqlDB->select($SQL);

                    if (!$areas && !is_array($areas)) {

                        $this->deleteOfferData($this->mysqlDB, $institutionID, $hostID, $proposalID);
                        Logs::writeLog("FAILED TO INSERT NEW PRAXIS OFFER´S STUDY AREA RECORD INTO 'PROPOSAL_STUDYAREA' TABLE OF 'PRAXIS' DATABASE", true);
                        return array("result" => 'saveOfferError');
                    }

                    $SQL = "INSERT INTO proposal_studyarea "
                            . "( "
                            . "proposal_id , "
                            . "studyarea_id ) "
                            . "VALUES ( "
                            . "" . $proposalID . " , "
                            . "" . (int) $areas[0]['id'] . " )";

                    $areaID = $this->mysqlDB->insert($SQL, false);

                    if (!$areaID) {

                        // DELETES PREVIOUS APPLICANT RECORD CREATED IN THE DATABASE
                        // DELETES PREVIOUS LANGUAGE RECORDS IN THE DATABASE IF THERE ARE SOME
                        // DELETES PREVIOUS STUDY DEGRESS RECORDS IN THE DATABASE IF THERE ARE SOME
                        // DELETES PREVIOUS INSTITUTION AND HOST RECORDS CREATED IN THE DATABASE
                        // DELETES PREVIOUS PROPOSAL RECORD CREATED IN THE DATABASE
                        $this->deleteOfferData($this->mysqlDB, $institutionID, $hostID, $proposalID);
                        Logs::writeLog("FAILED TO INSERT NEW PRAXIS OFFER´S STUDY AREA RECORD INTO 'PROPOSAL_STUDYAREA' TABLE OF 'PRAXIS' DATABASE", true);
                        return array("result" => 'saveOfferError');
                    } else {

                        $this->mysqlDB->commitDBChanges();
                        Logs::writeLog("NEW PRAXIS OFFER`S STUDY AREA RECORD INSERTED INTO 'PROPOSAL_STUDYAREA' TABLE OF 'PRAXIS' DATABASE");
                    }
                }
            }
        }

        // SAVES TARGETS IF THERE ARE SOME
        if (count($OfferValues->getProposal()->getProposalTargets()) > 0) {

            foreach ($OfferValues->getProposal()->getProposalTargets() as $target) {

                if (!empty($target)) {

                    $SQL = "SELECT id FROM target WHERE TRIM(name) = '" . $target . "'";
                    $targets = $this->mysqlDB->select($SQL);

                    if (!$targets && !is_array($targets)) {

                        $this->deleteOfferData($this->mysqlDB, $institutionID, $hostID, $proposalID);
                        Logs::writeLog("FAILED TO INSERT NEW PRAXIS OFFER´S TARGET RECORD INTO 'PROPOSAL_TARGET' TABLE OF 'PRAXIS' DATABASE", true);
                        return array("result" => 'saveOfferError');
                    }

                    $SQL = "INSERT INTO proposal_target "
                            . "( "
                            . "proposal_id , "
                            . "target_id ) "
                            . "VALUES ( "
                            . "" . $proposalID . " , "
                            . "" . (int) $targets[0]['id'] . " )";

                    $targetID = $this->mysqlDB->insert($SQL, false);

                    if (!$targetID) {

                        // DELETES PREVIOUS APPLICANT RECORD CREATED IN THE DATABASE
                        // DELETES PREVIOUS LANGUAGE RECORDS IN THE DATABASE IF THERE ARE SOME
                        // DELETES PREVIOUS STUDY DEGRESS RECORDS IN THE DATABASE IF THERE ARE SOME
                        // DELETES PREVIOUS STUDY AREAS RECORDS IN THE DATABASE IF THERE ARE SOME
                        // DELETES PREVIOUS INSTITUTION AND HOST RECORDS CREATED IN THE DATABASE
                        // DELETES PREVIOUS PROPOSAL RECORD CREATED IN THE DATABASE
                        $this->deleteOfferData($this->mysqlDB, $institutionID, $hostID, $proposalID);
                        Logs::writeLog("FAILED TO INSERT NEW PRAXIS OFFER´S TARGET RECORD INTO 'PROPOSAL_TARGET' TABLE OF 'PRAXIS' DATABASE", true);
                        return array("result" => 'saveOfferError');
                    } else {

                        $this->mysqlDB->commitDBChanges();
                        Logs::writeLog("NEW PRAXIS OFFER`S TARGET RECORD INSERTED INTO 'PROPOSAL_TARGET' TABLE OF 'PRAXIS' DATABASE");
                    }
                }
            }
        }

        // SAVES PROMOTED SKILLS IF THERE ARE SOME
        if (count($OfferValues->getProposal()->getProposalPromotedSkills()) > 0) {

            foreach ($OfferValues->getProposal()->getProposalPromotedSkills() as $skill) {

                if (!empty($skill)) {

                    $SQL = "SELECT id FROM skill WHERE TRIM(name) = '" . $skill . "'";
                    $skills = $this->mysqlDB->select($SQL);

                    if (!$skills && !is_array($skills)) {

                        $this->deleteOfferData($this->mysqlDB, $institutionID, $hostID, $proposalID);
                        Logs::writeLog("FAILED TO INSERT NEW PRAXIS OFFER´S SKILL TO PROMOTE RECORD INTO 'PROMOTEDSKILL' TABLE OF 'PRAXIS' DATABASE", true);
                        return array("result" => 'saveOfferError');
                    }

                    $SQL = "INSERT INTO promotedskill "
                            . "( "
                            . "proposals_id , "
                            . "skill_id ) "
                            . "VALUES ( "
                            . "" . $proposalID . " , "
                            . "" . (int) $skills[0]['id'] . " )";

                    $skillID = $this->mysqlDB->insert($SQL, false);

                    if (!$skillID) {

                        // DELETES PREVIOUS APPLICANT RECORD CREATED IN THE DATABASE
                        // DELETES PREVIOUS LANGUAGE RECORDS IN THE DATABASE IF THERE ARE SOME
                        // DELETES PREVIOUS STUDY DEGRESS RECORDS IN THE DATABASE IF THERE ARE SOME
                        // DELETES PREVIOUS STUDY AREAS RECORDS IN THE DATABASE IF THERE ARE SOME
                        // DELETES PREVIOUS TARGET RECORDS IN THE DATABASE IF THERE ARE SOME
                        // DELETES PREVIOUS INSTITUTION AND HOST RECORDS CREATED IN THE DATABASE
                        // DELETES PREVIOUS PROPOSAL RECORD CREATED IN THE DATABASE
                        $this->deleteOfferData($this->mysqlDB, $institutionID, $hostID, $proposalID);
                        Logs::writeLog("FAILED TO INSERT NEW PRAXIS OFFER´S SKILL TO PROMOTE RECORD INTO 'PROMOTEDSKILL' TABLE OF 'PRAXIS' DATABASE", true);
                        return array("result" => 'saveOfferError');
                    } else {

                        $this->mysqlDB->commitDBChanges();
                        Logs::writeLog("NEW PRAXIS OFFER`S SKILL TO PROMOTE RECORD INSERTED INTO 'PROMOTEDSKILL' TABLE OF 'PRAXIS' DATABASE");
                    }
                }
            }
        }

        // SAVES REQUIRED SKILLS IF THERE ARE SOME
        if (count($OfferValues->getProposal()->getProposalRequiredSkills()) > 0) {

            foreach ($OfferValues->getProposal()->getProposalRequiredSkills() as $skill) {

                if (!empty($skill)) {

                    $SQL = "SELECT id FROM skill WHERE TRIM(name) = '" . $skill . "'";
                    $skills = $this->mysqlDB->select($SQL);

                    if (!$skills && !is_array($skills)) {

                        $this->deleteOfferData($this->mysqlDB, $institutionID, $hostID, $proposalID);
                        Logs::writeLog("FAILED TO INSERT NEW PRAXIS OFFER´S SKILL TO PROMOTE RECORD INTO 'REQUIREDSKILL' TABLE OF 'PRAXIS' DATABASE", true);
                        return array("result" => 'saveOfferError');
                    }

                    $SQL = "INSERT INTO requiredskill "
                            . "( "
                            . "proposals_id , "
                            . "skill_id ) "
                            . "VALUES ( "
                            . "" . $proposalID . " , "
                            . "" . (int) $skills[0]['id'] . " )";

                    $skillID = $this->mysqlDB->insert($SQL, false);

                    if (!$skillID) {

                        // DELETES PREVIOUS APPLICANT RECORD CREATED IN THE DATABASE
                        // DELETES PREVIOUS LANGUAGE RECORDS IN THE DATABASE IF THERE ARE SOME
                        // DELETES PREVIOUS STUDY DEGRESS RECORDS IN THE DATABASE IF THERE ARE SOME
                        // DELETES PREVIOUS STUDY AREAS RECORDS IN THE DATABASE IF THERE ARE SOME
                        // DELETES PREVIOUS TARGET RECORDS IN THE DATABASE IF THERE ARE SOME
                        // DELETES PREVIOUS PROMOTED SKILLS RECORDS IN THE DATABASE IF THERE ARE SOME
                        // DELETES PREVIOUS INSTITUTION AND HOST RECORDS CREATED IN THE DATABASE
                        // DELETES PREVIOUS PROPOSAL RECORD CREATED IN THE DATABASE
                        $this->deleteOfferData($this->mysqlDB, $institutionID, $hostID, $proposalID);
                        Logs::writeLog("FAILED TO INSERT NEW PRAXIS OFFER´S SKILL TO PROMOTE RECORD INTO 'REQUIREDSKILL' TABLE OF 'PRAXIS' DATABASE", true);
                        return array("result" => 'saveOfferError');
                    } else {

                        $this->mysqlDB->commitDBChanges();
                        Logs::writeLog("NEW PRAXIS OFFER`S SKILL TO PROMOTE RECORD INSERTED INTO 'REQUIREDSKILL' TABLE OF 'PRAXIS' DATABASE");
                    }
                }
            }
        }

        $this->mysqlDB->closeDBConnection();

        return array("result" => 'offerSaved');
    }

    /**
     * 
     * @param Object $dbConnection - Database connection reference
     * @param int $institutionID - Institution´s ID
     * @param int $hostID - Host´s ID
     * @param int $proposalID - Proposal´s ID
     */
    private function deleteOfferData($dbConnection, $institutionID = 'NULL', $hostID = 'NULL', $proposalID = 'NULL') {

        // ROLLBACK ANY CHANGES
        $dbConnection->rollbackDBChanges();

        $SQLS = array(
            "DELETE FROM promotedskill WHERE proposals_id = " . $proposalID,
            "DELETE FROM requiredskill WHERE proposals_id = " . $proposalID,
            "DELETE FROM proposal_language WHERE proposal_id = " . $proposalID,
            "DELETE FROM proposal_studyarea WHERE proposal_id = " . $proposalID,
            "DELETE FROM proposal_studydegree WHERE proposal_id = " . $proposalID,
            "DELETE FROM proposal_target WHERE proposal_id = " . $proposalID,
            "DELETE FROM proposal WHERE id = " . $proposalID,
            "DELETE FROM institution WHERE id = " . $institutionID . " OR id = " . $hostID
        );

        foreach ($SQLS as $SQL) {

            $dbConnection->delete($SQL);
        }

        $dbConnection->commitDBChanges();
        $dbConnection->closeDBConnection();
    }

}

?>