<?php

class CustomTests extends PHPUnit_Framework_TestCase {

    /**
     * 
     * TESTS IF ALL ATRIBUTES OF AN OFFER'S OBJECT ARE NULL
     * 
     * @param type $offerObject - Object reference
     */
    public function assertAllAttributesNull($offerObject) {

        // GETS ALL ATRIBUTES OF AN OFFER'S OBJECT
        $offerVars = get_object_vars($offerObject);
        $numVarsNull = 0;

        // ITERATE EACH OFFER'S ATTRIBUTES
        foreach ($offerVars as $key => $offerVar) {

            $varsFromEachVar = get_object_vars($offerVars[$offerVar]);

            // ITERATES EACH ATTRIBUTE OF ONE ATTRIBUTE'S OFFER
            foreach ($varsFromEachVar as $key => $value) {

                if (is_null($varsFromEachVar[$value])) {

                    $numVarsNull++;
                }
            }
        }

        if ($numVarsNull == count($offerVars)) {
            return true;
        }

        return false;
    }

    /**
     * 
     * TESTS IF ALL ATRIBUTES OF AN OFFER'S OBJECT ARE NOT NULL
     * 
     * @param type $offerObject
     * @return boolean
     */
    public function assertAllAttributesFilled($offerObject) {

        $offerVars = get_object_vars($offerObject);
        $numVarsFilled = 0;

        foreach ($offerVars as $key => $offerVar) {

            $varsFromEachVar = get_object_vars($offerVars[$offerVar]);

            foreach ($varsFromEachVar as $key => $value) {

                if (!is_null($varsFromEachVar[$value])) {

                    $numVarsFilled++;
                }
            }
        }

        if ($numVarsFilled == count($offerVars)) {
            return true;
        }

        return false;
    }

}

?>
