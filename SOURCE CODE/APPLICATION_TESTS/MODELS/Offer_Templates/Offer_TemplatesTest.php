<?php

require_once '../../../MODELS/Offer_Templates/Offer_Templates.php';

class Offer_TemplatesTest extends PHPUnit_Framework_TestCase {

    public function testGenerateXML() {

        $currentDir = getcwd() . "\\";

        // WRONG XML SCHEMA FILE PATH TEST
        echo "testGenerateXML(1)\n";
        $this->assertEquals(false, Offer_Templates::generateXML($currentDir . "something.xsd"));

        // RIGHT XML SCHEMA PATH AND 'WRONG' XML SCHEMA FILE CONTENT TEST
        echo "testGenerateXML(2)\n";
        $this->assertEquals(false, Offer_Templates::generateXML($currentDir . "schema_wrong.xsd"));

        // RIGHT XML SCHEMA FILE PATH AND 'RIGHT' XML SCHEMA FILE CONTENT TEST
        echo "testGenerateXML(3)\n";
        $this->assertStringStartsWith("<?xml version='1.0' encoding='UTF-8'?>", Offer_Templates::generateXML($currentDir . "schema.xsd"));

        echo "testGenerateXML(OK!)\n\n";
    }

    public function testGeneratePDF() {

        $currentDir = getcwd() . "\\";

        // WRONG XML SCHEMA FILE PATH TEST
        echo "testGeneratePDF(1)\n";
        $this->assertEquals(false, Offer_Templates::generateXML($currentDir . "something.xsd"));

        // RIGHT XML SCHEMA PATH AND 'WRONG' XML SCHEMA FILE CONTENT TEST
        echo "testGeneratePDF(2)\n";
        $this->assertEquals(false, Offer_Templates::generateXML($currentDir . "schema_wrong.xsd"));

        // RIGHT XML SCHEMA FILE PATH AND 'RIGHT' XML SCHEMA FILE CONTENT TEST
        echo "testGeneratePDF(3)\n";
        $this->assertInternalType('string', Offer_Templates::generateXML($currentDir . "schema.xsd"));

        echo "testGeneratePDF(OK!)\n\n";
    }

    public function testGetXmlFieldNames() {

        // GETS ALL OFFER'S NODES
        $offerNodes = SchemaNodes::getOfferNodes();

        // XML 'WRONG' TEST FILE(without proper information) TEST
        $xml = simplexml_load_file("provider_wrong.xml");

        echo "testGetXmlFieldNames(1)\n";
        $this->assertInternalType('array', Offer_Templates::getXmlFieldNames($xml, $offerNodes));
        
        echo "testGetXmlFieldNames(2)\n";
        $this->assertEmpty(Offer_Templates::getXmlFieldNames($xml, $offerNodes));

        // XML 'RIGHT' FILE TEST
        $xml = simplexml_load_file("provider.xml");

        echo "testGetXmlFieldNames(3)\n";
        $this->assertInternalType('array', Offer_Templates::getXmlFieldNames($xml, $offerNodes));
        
        echo "testGetXmlFieldNames(4)\n";
        $this->assertNotEmpty(Offer_Templates::getXmlFieldNames($xml, $offerNodes));

        echo "testGetXmlFieldNames(OK!)\n\n";
    }

}

?>