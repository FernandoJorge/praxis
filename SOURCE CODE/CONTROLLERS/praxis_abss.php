<?php

/**
 * IMPORTS
 */
require_once '../CONFIGS/twig_loader.php';

/**
 * REQUEST FOR REDIRECT TO PROJECTS SUBMISSION AUTHENTICATION PAGE
 */
if (isset($_REQUEST['projects']) && $_REQUEST['projects'] == '1') {

    echo $twig->render('PROJECT/authentic.html');
    exit;
}

/**
 * REQUEST FOR REDIRECT TO EDITORS REGISTRATION PAGE
 */
if (isset($_REQUEST['register_editor']) && $_REQUEST['register_editor'] == '1') {

    echo $twig->render('EDITOR_REGISTRATION/register_editor.html');
    exit;
}

/**
 * REQUEST FOR REDIRECT TO NEWSLETTERS SUBSCRIPTION PAGE
 */
if (isset($_REQUEST['subscription']) && $_REQUEST['subscription'] == '1') {

    echo $twig->render('NEWSLETTERS/newsletter.html');
    exit;
}

/**
 * REQUEST FOR REDIRECT TO XML SHEMA EDITOR AUTHENTICATION PAGE
 */
if (isset($_REQUEST['xml_schemas']) && $_REQUEST['xml_schemas'] == '1') {

    echo $twig->render('XML_SCHEMA_EDITOR/authentic.html');
    exit;
}

/**
 * REQUEST TO LOGOUT
 */
if (isset($_REQUEST['logout']) && $_REQUEST['logout'] == '1') {
    
    // ENDS USER´S SESSION
    session_start();
    
    if(isset($_SESSION['providerID'])){
        echo $twig->render('PROJECT/authentic.html');
    }
    
    if(isset($_SESSION['editorID'])){
        echo $twig->render('XML_SCHEMA_EDITOR/authentic.html');
    }
    
    session_unset();

    if (ini_get("session.use_cookies")) {
        $params = session_get_cookie_params();
        setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]
        );
    }

    session_destroy();

    
    exit;
}
?>
