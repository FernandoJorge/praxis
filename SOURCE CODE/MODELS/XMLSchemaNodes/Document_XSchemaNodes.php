<?php

/**
 * CLASS OF SERVICES THAT ALLOWS TO GET THE DECLARATION OF A XML SCHEMA STRUCTURAL NODE´S ELEMENTS
 */
class Document_XSchemaNodes {

    /** STATIC VARIABLES * */
    static private $dataTypes = array(
        "string",
        "normalizedString",
        "token",
        "byte",
        "unsignedByte",
        "base64Binary",
        "hexBinary",
        "integer",
        "positiveInteger",
        "negativeInteger",
        "nonNegativeInteger",
        "nonPositiveInteger",
        "int",
        "unsignedInt",
        "long",
        "unsignedLong",
        "short",
        "unsignedShort",
        "decimal",
        "float",
        "double",
        "boolean",
        "time",
        "dateTime",
        "duration",
        "date",
        "gMonth",
        "gYear",
        "gYearMonth",
        "gDay",
        "gMonthDay",
        "name",
        "QName",
        "NCName");
    static private $restrictionOperators = array(
        "minInclusive",
        "maxInclusive",
        "length",
        "totaldigits",
        "pattern",
        "fractionDigits",
        "enumeration");

    /** STATIC METHODS * */

    /**
     * 
     * GETS ELEMENT NODE´S DECLARATION OF A XSD DOCUMENT´S ELEMENT NODE
     * 
     * @param int $nodeID
     * @return String - Element node´s declaration
     */
    static function getDeclarationNode($nodeID) {

        switch ($nodeID) {
            case 0:

                return self::getDocumentNode();
                break;

            case 1:

                return self::getSimpleElementNode();
                break;

            case 2:

                return self::getLocalSimpleTypeNode();
                break;

            case 3:

                return self::getGlobalSimpleTypeNode();
                break;

            case 4:

                return self::getLocalComplexTypeNode();
                break;

            case 5:

                return self::getGlobalComplexTypeNode();
                break;

            case 6:

                return self::getGroupNode();
                break;

            case 7:

                return self::getSequenceNode();
                break;

            case 8:

                return self::getChoiceNode();
                break;

            case 9:

                return self::getRestrictionOperatorNode();
                break;

            case 10:

                return self::getDataTypeValue();
                break;
        }
    }

    /**
     * 
     * GETS DATA TYPES DECLARATONS
     * 
     * @return Array - DataTypes of the elements´ nodes
     */
    static function getDataTypes() {

        return self::$dataTypes;
    }

    /**
     * 
     * GETS RESTRICTION OPERATORS DECLARATIONS
     * 
     * @return Array - Restriction strings for restriction operators
     */
    static function getRestrictionOperators() {

        return self::$restrictionOperators;
    }

    /**
     * 
     * GETS XML SCHEMA STRUCUTURE DECLARATION
     * 
     * @return String - Node element declaration that represents the document structure of XML Schema
     */
    static function getDocumentNode() {

        $documentRoot = "<xs:schema elementFormDefault='qualified' attributeFormDefault='unqualified' xmlns:xs='http://www.w3.org/2001/XMLSchema'>\n\n"
                . "<xs:element name='OFFER'>\n"
                . "<xs:complexType>\n"
                . "<xs:sequence>\n\n\n<!-- REPLACE THIS COMMENT WITH OTHER ELEMENTS -->\n\n\n"
                . "</xs:sequence>\n"
                . "</xs:complexType>\n"
                . "</xs:element>\n\n"
                . "</xs:schema>";

        return $documentRoot;
    }

    /**
     * 
     * GETS SIMPLE ELEMENT DECLARATION
     * 
     * @return String - Node element declaration that represents a simple node element
     */
    static function getSimpleElementNode() {

        $simpleElement = "<xs:element name='<1-- set the name of the element -->'\n "
                . "type='xs:<!-- set global type -->'\n "
                . "ref='<!-- set global element declaration -->'\n "
                . "form='<!-- set qualified or unqualified -->'\n "
                . "minOccurs='<!-- set non negative number -->'\n "
                . "maxOccurs='<!-- set non negative number or unbounded string -->'\n "
                . "default='<!-- set default value -->'\n "
                . "fixed='<!--set fixed value -->'/>";

        return $simpleElement;
    }

    /**
     * 
     * GETS LOCAL COMPLEX TYPE ELMENT DECLARATION
     * 
     * @return String - Node element declaration that represents a local complexType
     */
    static function getLocalComplexTypeNode() {

        $localComplexType = "<xs:element name='<!-- set name of the element -->'>\n
                <xs:complexType>\n
                    <xs:sequence>\n
                        <xs:element name='<!-- set name of the element 1 -->' type='xs:<!-- set global type -->'/>\n
                        <xs:element name='<!-- set name of the element 2 -->' type='xs:<!-- set global type -->'/>\n
                    </xs:sequence>\n
                    <xs:attribute name='<!-- set name of the attribute -->' type='xs:<!-- set global type -->'/>\n
                </xs:complexType>\n
            </xs:element>";

        return $localComplexType;
    }

    /**
     * 
     * GETS GLOBAL COMPLEX TYPE ELEMENT DECLARATION
     * 
     * @return String - Node element declaration that represents a global complexType
     */
    static function getGlobalComplexTypeNode() {

        $localComplexType = "<xs:complexType name='<!-- set name of the complexType element -->'>\n
                    <xs:sequence>\n
                        <!-- Insert a sequence elements -->
                    </xs:sequence>\n
                    <xs:attribute name='<!-- set name of the attribute>' type='xs:<set global type -->'/>\n
                </xs:complexType>";

        return $localComplexType;
    }

    /**
     * 
     * GETS LOCAL SIMPLE TYPE ELEMENT DECLARATION
     * 
     * @return String - Node element declaration that represents a local simpleType
     */
    static function getLocalSimpleTypeNode() {

        $localSimpleType = "<xs:element name='<!-- set name of the element -->'>\n
                <xs:simpleType>\n
                    <xs:restriction base='xs:<!-- set global type -->'>\n
                        <!-- set restriction elements -->
                    </xs:restriction>\n
                </xs:simpleType>\n
            </xs:element>";

        return $localSimpleType;
    }

    /**
     * 
     * GETS GLOBAL SIMPLE TYPE ELEMENT DECLARATION
     * 
     * @return String - Node element declaration that represents a global simpleType
     */
    static function getGlobalSimpleTypeNode() {

        $globalSimpleType = "<xs:simpleType name='<!-- set name of the simpleType -->'>\n
                    <xs:restriction base='xs:<!-- set global type -->'\n
                        <!-- set restriction elements -->
                    </xs:restriction>\n
                </xs:simpleType>";

        return $globalSimpleType;
    }

    /**
     * 
     * GETS GROUP ELEMENT DECLARATION
     * 
     * @return String - Node element declaration that represents a Group of elements
     */
    static function getGroupNode() {

        $group = "<xs:group name='<!-- set name of the group -->' ref='<!-- set global group definition -->' minOccurs='<!-- non negative number -->' maxOccurs='<!-- non negative number or unbounded string -->'>\n
                <xs:sequence>\n
                    <!-- set sequence of elemnets -->
                </xs:sequence>\n
            </xs:group>";

        return $group;
    }

    /**
     * 
     * GETS SEQUENCE ELEMENT DECLARATION
     * 
     * @return String - Node element declaration that represents a sequence of elements
     */
    static function getSequenceNode() {

        $sequence = "<xs:sequence minOccurs='<!-- non negative number -->' maxOccurs='<!-- non negative number or unbounded -->'>\n"
                . "<!-- set sequence of elements -->"
                . "</xs:sequence>";

        return $sequence;
    }

    /**
     * 
     * GETS CHOICE ELEMENT DECLARATION
     * 
     * @return String - Node element declaration that represents a choice of elements
     */
    static function getChoiceNode() {

        $choice = "<xs:choice minOccurs='<!-- non negative number -->' maxOccurs='<!-- non negative number or unbounded -->'>\n"
                . "<!-- set sequence of elements -->"
                . "</xs:choice>";

        return $choice;
    }

    /**
     * 
     * GETS DATA TYPE ELEMENT DECLARATION
     * 
     * @return String - Represents the value for a data type
     */
    static function getDataTypeValue() {

        $types = "<!-- Pick one => ";

        foreach (self::$dataTypes as $type) {

            $types .= $type . " | ";
        }

        $types .="-->";

        return "xs:" . $types;
    }

    /**
     * 
     * GETS RESTRICTION OPERATOR ELEMENT DECLARATION
     * 
     * @return String - Node element that represents the value for a restriction operator
     */
    static function getRestrictionOperatorNode() {

        $options = "<!-- Pick one => ";

        foreach (self::$restrictionOperators as $restr) {

            $options .= $restr . " | ";
        }

        $options .="-->";

        return "<xs " . $options . " value='<!-- set the value of the restriction -->' />";
    }

}
