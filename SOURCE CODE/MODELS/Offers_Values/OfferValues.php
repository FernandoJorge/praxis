<?php

/**
 * INTERFACE TO HANDLE THE DATA OF THE OFFER
 */
interface OfferValues {

    function getOfferFieldsValues($filePath, $schemaDocPath);
}
