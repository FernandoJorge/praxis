/**
 * SCRIPT TO HANDLE EVENTS OF THE 'submit_project.html' page
 */

// SAVE THE INTERNSHIP/PROJECT IF THE FILE IS WELL PARSED AND VALID
function submitFile() {

    if ($("#projectFile")[0].files.length == 0) {

        $("#modal-message-header").html("Internship/Project File submission&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
        $("#modal-message-content").html("<p>Choose a file...</p>");

        $("#message").modal({
            zIndex: 9999
        });
        $("#message").modal('show');

        return;
    }

    $("#fileForm").ajaxSubmit({
        url: 'project',
        datatype: 'json',
        async: false,
        success: function (msg) {

            if (msg['result'] == 'invalid') {

                $("#modal-message-header").html("Internship/Project File submission...");
                $("#modal-message-content").html("<p>File is not valid&nbsp;<span class='glyphicon glyphicon-thumbs-down'></span></p><br><h5><u>Errors:</u></h5>");

                for (var i = 0; i < msg['errors'].length; i++) {

                    $("#modal-message-content").append("<br>" + msg['errors'][i]);
                }

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'fileCopyError' || msg['result'] == 'providerSchemaAccessError') {

                $("#modal-message-header").html("Internship/Project File submission&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p><It was not possible to submit the file...</p>");

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'noSchemaFile' || msg['result'] == 'noProviderSchemaError') {

                $("#modal-message-header").html("Internship/Project File submission&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>You can´t submit your offer because there´s no validation process stablished for your submissions...<br><br>\n\
                                                  For help click here >> <a href='http://www.praxisnetwork.eu/contact/'>PRAXIS CONTACT</a></p>");

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'invalid_country_proposal') {

                $("#modal-message-header").html("Internship/Project File submission&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>The country´s name of the offer´s proposal is not valid...</p>");

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'invalid_city_proposal') {

                $("#modal-message-header").html("Internship/Project File submission&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>The city´s name of the offer´s proposal is not valid...</p>");

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'no_match_country_city_proposal') {

                $("#modal-message-header").html("Internship/Project File submission&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>The city doesn´t belong to the country of the offer´s proposal...</p>");

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'invalid_country_institution') {

                $("#modal-message-header").html("Internship/Project File submission&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>The country´s name of the offer´s institution is not valid...</p>");

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'invalid_city_institution') {

                $("#modal-message-header").html("Internship/Project File submission&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>The city´s name of the offer´s institution is not valid...</p>");

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'no_match_country_city_institution') {

                $("#modal-message-header").html("Internship/Project File submission&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>The city doesn´t belong to the country of the offer´s institution...</p>");

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'invalid_country_host') {

                $("#modal-message-header").html("Internship/Project File submission&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>The country´s name of the offer´s host is not valid...</p>");

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'invalid_city_host') {

                $("#modal-message-header").html("Internship/Project File submission&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>The city´s name of the offer´s host is not valid...</p>");

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'no_match_country_city_host') {

                $("#modal-message-header").html("Internship/Project File submission&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>The city doesn´t belong to the country of the offer´s host...</p>");

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'invalid_institution_type') {

                $("#modal-message-header").html("Internship/Project File submission&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>The type´s name of the offer´s institution is not valid...</p>");

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'invalid_host_type') {

                $("#modal-message-header").html("Internship/Project File submission&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>The type´s name of the offer´s host is not valid...</p>");

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'invalid_languages_proposal') {

                $("#modal-message-header").html("Internship/Project File submission&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>The language name(s) of the offer´s proposal is(are) not valid...</p><br><u>Invalid Languages:</u><br>");


                for (var i = 0; i < msg['languages'].length; i++) {

                    $("#modal-message-content").append("<br>" + msg['languages'][i]);
                }

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'invalid_study_degrees_proposal') {

                $("#modal-message-header").html("Internship/Project File submission&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>The study degree name(s) of the offer´s proposal is(are) not valid...</p><br><u>Invalid study degrees:</u><br>");


                for (var i = 0; i < msg['degrees'].length; i++) {

                    $("#modal-message-content").append("<br>" + msg['degrees'][i]);
                }

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'invalid_study_areas_proposal') {

                $("#modal-message-header").html("Internship/Project File submission&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>The study area name(s) of the offer´s proposal is(are) not valid...</p><br><u>Invalid study areas:</u><br>");


                for (var i = 0; i < msg['areas'].length; i++) {

                    $("#modal-message-content").append("<br>" + msg['areas'][i]);
                }

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'invalid_targets_proposal') {

                $("#modal-message-header").html("Internship/Project File submission&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>The  target name(s) of the offer´s proposal is(are) not valid...</p><br><u>Invalid targets:</u><br>");


                for (var i = 0; i < msg['targets'].length; i++) {

                    $("#modal-message-content").append("<br>" + msg['targets'][i]);
                }

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'invalid_promoted_skills_proposal') {

                $("#modal-message-header").html("Internship/Project File submission&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>The skills to promote name(s) of the offer´s proposal is(are) not valid...</p><br><u>Invalid skills:</u><br>");


                for (var i = 0; i < msg['skills'].length; i++) {

                    $("#modal-message-content").append("<br>" + msg['skills'][i]);
                }

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'invalid_required_skills_proposal') {

                $("#modal-message-header").html("Internship/Project File submission&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>The required skills name(s) of the offer´s proposal is(are) not valid...</p><br><u>Invalid skills:</u><br>");


                for (var i = 0; i < msg['skills'].length; i++) {

                    $("#modal-message-content").append("<br>" + msg['skills'][i]);
                }

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'incomplete_institution' || msg['result'] == 'incomplete_host') {

                $("#modal-message-header").html("Internship/Project File submission.&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>The information below is required to submit the offer:</p><br><u>Missing values:</u><br>");

                for (var i = 0; i < msg['fields'].length; i++) {

                    $("#modal-message-content").append("<br>" + msg['fields'][i]);
                }

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'start_date_wrong_date_format') {

                $("#modal-message-header").html("Internship/Project File submission&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>Offer proposal´s start date wrong format...<br>The correct format is YYYY-MM-DD(Ex: 2014-05-13)</p>");

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'start_date_invalid_date') {

                $("#modal-message-header").html("Internship/Project File submission&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>Offer proposal´s start date is not a valid gregorian date.</p>");

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'end_date_wrong_date_format') {

                $("#modal-message-header").html("Internship/Project File submission&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>Offer proposal´s end date wrong format...<br>The correct format is YYYY-MM-DD(Ex: 2014-05-13)</p>");

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'end_date_invalid_date') {

                $("#modal-message-header").html("Internship/Project File submission&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>Offer proposal´s end date is not a valid gregorian date.</p>");

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'invalid_date_period') {

                $("#modal-message-header").html("Internship/Project File submission&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>Offer proposal´s period set by start date and end date is invalid.</p>");

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'expiration_date_wrong_date_format') {

                $("#modal-message-header").html("Internship/Project File submission&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>Apllication´s expiration date wrong format...<br>The correct format is YYYY-MM-DD(Ex: 2014-05-13)</p>");

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'expiration_date_invalid_date') {

                $("#modal-message-header").html("Internship/Project File submission&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>Application´s expiration date is not a valid gregorian date.</p>");

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'invalid_start_submission_date') {

                $("#modal-message-header").html("Internship/Project File submission&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>Proposal´s start date is prior to the current submission´s date.</p>");

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'invalid_expiration_submission_date') {

                $("#modal-message-header").html("Internship/Project File submission&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>Application´s expiration date is prior to the current submission´s date.</p>");

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'invalid_expiration_start_date') {

                $("#modal-message-header").html("Internship/Project File submission&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>Application´s expiration date must be prior to proposal´s start date.</p>");

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'invalid_duration_proposal') {

                $("#modal-message-header").html("Internship/Project File submission.&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>The number of months for the duration of the project doesn´t match the period set by start date and end date.<br><br>Additional information:<br>EX: Start Date: 2014-01-01 => End Date: 2014-01-12 is understood as one month.</p>");

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'saveOfferError') {

                $("#modal-message-header").html("Internship/Project File submission&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>Occurred an error while saving the offer...</p>");

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'offerSaved') {

                $("#modal-message-header").html("Internship/Project File submission...");
                $("#modal-message-content").html("<p>The offer was saved with success!!!&nbsp;<span class='glyphicon glyphicon-thumbs-up'></span></p>");

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'corruptedTXTFile') {

                $("#modal-message-header").html("Internship/Project File submission&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>The txt offer´s file is corrupted...<br><br>Save PDF file again as TXT format using Acrobat Reader and submit.</p>");

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'errorTXTValidation') {

                $("#modal-message-header").html("Internship/Project File submission&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>Occurred an error while validating the offer...</p>");

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'InvalidFormatSubmission') {

                $("#modal-message-header").html("Internship/Project File submission&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>The file´s format is invalid...<br><br>Only XML and TXT file format are valid for offer submissions.<br>If you downloaded a PDF offer template read the instructions available in the zip file to correctly submit your offer.</p>");

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            $("#fileForm")[0].reset();
        },
        error: function (xhr, status, error) {

            console.log(" xhr.responseText: " + xhr.responseText + " \n\nSTATUS:\n " + status + " \n\nERROR:\n " + error);

            $("#modal-message-header").html("Request error&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
            $("#modal-message-content").html("<p>It was not possible to make the request...</p>");

            $("#message").modal({
                zIndex: 9999
            });
            $("#message").modal('show');
        }
    });
}