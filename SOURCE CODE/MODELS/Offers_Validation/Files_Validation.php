<?php

/**
 * IMPORTS
 */
require_once 'XML_XSDValidation.php';
require_once 'TXTValidation.php';

/**
 * CLASS OF SERVICE TO VALIDATE VARIOUS FILES DOCUMENTS FORMATS
 */
class Files_Validation {

    /**
     * 
     * VALIDATES A OFFER FILE
     * 
     * @param String $templateOfferPath - Abosulte path of the file to validate
     * @param String $xmlSchemaDocPath - Abosulte path of the Xml Schema document
     * @return Array | string | boolean - Errors during the parsing and validation of the xml file | 
     * 'corruptedFile' if txt file is not well formed | 
     * 'errorTXTValidation' if it is not possible to validate file | 
     * 'valid' if file is valid | 
     * In case of txt file returns false if the it was not possible to generate xml template
     */
    static function validateFileFormat($templateOfferPath, $xmlSchemaDocPath) {

        // SPLIT THE NAME´S FILE TO GET THE EXTENSION FORMAT
        $extension = explode(".", $templateOfferPath);
        $extension = end($extension);

        // XML FORMAT VALIDATION
        if (strcasecmp($extension, "xml") == 0) {

            // VALIDATES XML SYNTAX
            $syntax = XML_XSDValidation::checkDocSyntax($templateOfferPath);

            // TESTS IF THERE ARE ANY ERRORS
            if (count($syntax) > 0) {

                return array("result" => 'invalid', 'errors' => $syntax);
            }

            // VALIDATES XML STRUCTURE AGAINST THE XML SCHEMA
            $valid = XML_XSDValidation::checkXMLSchemaValidation($templateOfferPath, $xmlSchemaDocPath);

            // TESTS IF THERE ARE ANY ERRORS
            if (count($valid) > 0) {

                return array("result" => 'invalid', 'errors' => $valid);
            }

            return array("result" => 'valid');
        }

        // TXT FORMAT VALIDATION
        if (strcasecmp($extension, "txt") == 0) {

            $valid = TXTValidation::fieldsNamesValidation($templateOfferPath, $xmlSchemaDocPath);

            // TESTS IF THE FILE IS CORRUPTED
            if ($valid == "corruptedTXTFile") {

                return array("result" => 'corruptedTXTFile');
            }

            // TESTS IF FILE IS VALID
            if ($valid == "errorTXTValidation") {

                return array("result" => 'errorTXTValidation');
            }

            return array("result" => 'valid');
        }
    }

}

?>
