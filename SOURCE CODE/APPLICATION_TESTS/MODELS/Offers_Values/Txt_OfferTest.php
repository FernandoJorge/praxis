<?php

require_once '../../../MODELS/Offers_Values/Txt_Offer.php';
require_once '../../../MODELS/Offer/Offer.php';
require_once '/../../CUSTOM_TESTS/CustomTests.php';

class Txt_OfferTest extends PHPUnit_Framework_TestCase {

    public function testGetOfferFieldsValues() {

        // WARNING //
        // At the method 'getOfferFieldsValues()'
        // of '/PRAXIS/MODELS/Offers_Values/Txt_Offer.php' file, it must 
        // be replaced all the instructions that are using '$_SERVER['DOCUMENT_ROOT']' reference by
        // the actual real absolute path in order to perform this test

        $CTests = new CustomTests();
        $txtOffer = new Txt_Offer();

        $currentDir = getcwd() . "\\";

        // 'RIGHT TXT FILE CONTENT WITH ALL THE ELEMENTS FILLED AND 'WRONG' XML SCHEMA FILE PATH TEST
        echo "testGetOfferFieldsValues(1)\n";
        $this->assertEquals(false, $txtOffer->getOfferFieldsValues($currentDir . "form_correct.txt", $currentDir . "something.xsd"));

        // 'WRONG' TXT FILE CONTENT AND 'WRONG' XML SCHEMA FILE TEST
        echo "testGetOfferFieldsValues(2)\n";
        $this->assertEquals(false, $txtOffer->getOfferFieldsValues($currentDir . "form_incorrect.txt", $currentDir . "something.xsd"));

        // EMPTY TXT OFFER WITH 'RIGHT' XML SCHEMA FILE TEST
        echo "testGetOfferFieldsValues(3)\n";
        $this->assertEquals(false, $txtOffer->getOfferFieldsValues($currentDir . "form_empty.txt", $currentDir . "schema.xsd"));

        // 'RIGHT' TXT FILE CONTENT WITH ALL THE ELEMENTS FILLED AND 'RIGHT' XML SCHEMA FILE TEST
        echo "testGetOfferFieldsValues(4)\n";
        $this->assertInstanceOf('Offer', $txtOffer->getOfferFieldsValues($currentDir . "form_correct.txt", $currentDir . "schema.xsd"));

        // ALL ATTRIBUTES OF OFFER OBJECT ARE NOT NULL TEST
        echo "testGetOfferFieldsValues(5)\n";
        $CTests->assertAllAttributesFilled($txtOffer->getOfferFieldsValues($currentDir . "form_correct.txt", $currentDir . "schema.xsd"));

        echo "testGetOfferFieldsValues(OK!)\n\n";
    }

}

?>
