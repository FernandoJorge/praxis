<?php

/**
 * GLOBAL CONFIGURATIONS FOR THE APPLICATION
 */

define('ROOT', $_SERVER['DOCUMENT_ROOT']);
define('DATABASE', ROOT . '/DAL/DATABASES');
define('MODELS', ROOT . '/MODELS');
define('VIEWS', ROOT . '/VIEWS');
define('TWIG_LOADER', ROOT . '/CONFIGS/twig_loader.php');
define('UTILS', ROOT . '/UTILS');
define('PROVIDERS', ROOT . '/FILES/PICTURES/PROVIDERS');

?>