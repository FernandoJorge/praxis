<?php

require_once '/../../../DAL/DATABASES/FactoryDataBaseAccess.php';
require_once '/../../../MODELS/Offer_Data_Validation/Offer_Data_Validation.php';
require_once '/../../../MODELS/Offer/Offer.php';

class Offer_Data_ValidationTest extends PHPUnit_Framework_TestCase {

    function testOfferDataValidation() {

        // WARNING //
        // At 'MODELS/Offer_Data_Validation/Offer_Data_Validation.php'
        // comment all the instructions that write log errors to a file.
        // Search for equal comments in this test case within the method 'offerDataValidation(...)'
        // Example - 'Logs::writeLog(...)'

        $xmlPst = FactoryDataBaseAccess::getSGDBConnection("mysql", "localhost:3306", "praxis", "root", "");
        $offer = new Offer();

        // ABOUT THE INSTITUTION
        echo "testOfferDataValidation(1)\n";
        $offer->getInstitution()->setInstitutionName(null);
        $offer->getInstitution()->setInstitutionType(null);
        $offer->getInstitution()->setInstitutionPhone(null);
        $this->assertEquals('incomplete_institution', Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['result']);

        echo "testOfferDataValidation(2)\n";
        $this->assertCount(3, Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['fields']);

        echo "testOfferDataValidation(3)\n";
        $offer->getInstitution()->setInstitutionName("name");
        $offer->getInstitution()->setInstitutionType(null);
        $offer->getInstitution()->setInstitutionPhone(null);
        $this->assertEquals('incomplete_institution', Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['result']);

        echo "testOfferDataValidation(4)\n";
        $this->assertCount(2, Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['fields']);

        echo "testOfferDataValidation(5)\n";
        $offer->getInstitution()->setInstitutionName("name");
        $offer->getInstitution()->setInstitutionType("University");
        $offer->getInstitution()->setInstitutionPhone(null);
        $this->assertEquals('incomplete_institution', Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['result']);

        echo "testOfferDataValidation(6)\n";
        $this->assertCount(1, Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['fields']);

        // ABOUT THE HOST
        echo "testOfferDataValidation(7)\n";
        $offer->getInstitution()->setInstitutionPhone("939393939");
        $offer->getHost()->setInstitutionName(null);
        $offer->getHost()->setInstitutionType(null);
        $offer->getHost()->setInstitutionPhone(null);
        $this->assertEquals('incomplete_host', Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['result']);

        echo "testOfferDataValidation(8)\n";
        $this->assertCount(3, Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['fields']);

        echo "testOfferDataValidation(9)\n";
        $offer->getHost()->setInstitutionName("name");
        $offer->getHost()->setInstitutionType(null);
        $offer->getHost()->setInstitutionPhone(null);
        $this->assertEquals('incomplete_host', Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['result']);

        echo "testOfferDataValidation(10)\n";
        $this->assertCount(2, Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['fields']);

        echo "testOfferDataValidation(11)\n";
        $offer->getHost()->setInstitutionName("name");
        $offer->getHost()->setInstitutionType("Company");
        $offer->getHost()->setInstitutionPhone(null);
        $this->assertEquals('incomplete_host', Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['result']);

        echo "testOfferDataValidation(12)\n";
        $this->assertCount(1, Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['fields']);

        // VALIDATES IF PROPOSAL´S COUNTRY AND CITY ARE VALID AND IF THE CITY BELONGS TO THE COUNTRY
        echo "testOfferDataValidation(13)\n";
        $offer->getHost()->setInstitutionPhone("9896969696");
        $offer->getProposal()->setProposalCountry("Port");
        $offer->getProposal()->setProposalCity(null);
        $this->assertEquals('invalid_country_proposal', Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['result']);

        echo "testOfferDataValidation(14)\n";
        $offer->getProposal()->setProposalCountry(null);
        $offer->getProposal()->setProposalCity("cit");
        $this->assertEquals('invalid_city_proposal', Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['result']);

        echo "testOfferDataValidation(15)\n";
        $offer->getProposal()->setProposalCountry("PORTUGAL");
        $offer->getProposal()->setProposalCity("Ghent");
        $this->assertEquals('no_match_country_city_proposal', Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['result']);

        // VALIDATES PROPOSAL´S START DATE
        echo "testOfferDataValidation(16)\n";
        $offer->getProposal()->setProposalCountry("PORTUGAL");
        $offer->getProposal()->setProposalCity("Porto");
        $offer->getProposal()->setProposalStartDate("20145-09-10");
        $this->assertEquals('start_date_wrong_date_format', Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['result']);

        echo "testOfferDataValidation(17)\n";
        $offer->getProposal()->setProposalStartDate("2014-02-30");
        $this->assertEquals('start_date_invalid_date', Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['result']);

        // VALIDATE PROPOSAL´S START DATE AGAINST CURRENT SUBMISSION DATE(TODAY)
        echo "testOfferDataValidation(18)\n";
        $offer->getProposal()->setProposalStartDate(date('Y-m-d', strtotime("-6 days")));
        $this->assertEquals('invalid_start_submission_date', Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['result']);

        // VALIDATES PROPOSAL´S END DATE
        echo "testOfferDataValidation(19)\n";
        $offer->getProposal()->setProposalStartDate(date('Y-m-d', strtotime("60 days")));
        $offer->getProposal()->setProposalEndDate('20145-09-10');
        $this->assertEquals('end_date_wrong_date_format', Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['result']);

        echo "testOfferDataValidation(20)\n";
        $offer->getProposal()->setProposalEndDate('2014-02-30');
        $this->assertEquals('end_date_invalid_date', Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['result']);

        // VALIDATES PROPOSAL´S RANGE OF THE DATE
        echo "testOfferDataValidation(21)\n";
        $offer->getProposal()->setProposalStartDate(date('Y-m-d', strtotime("60 days")));
        $offer->getProposal()->setProposalEndDate(date('Y-m-d', strtotime("-60 days")));
        $this->assertEquals('invalid_date_period', Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['result']);

        // VALIDATES NUMBER OF MONTHS
        echo "testOfferDataValidation(22)\n";
        $offer->getProposal()->setProposalStartDate('2019-12-16');
        $offer->getProposal()->setProposalEndDate('2021-12-16');
        $offer->getProposal()->setProposalDuration("23");
        $this->assertEquals('invalid_duration_proposal', Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['result']);

        echo "testOfferDataValidation(23)\n";
        $offer->getProposal()->setProposalDuration("25");
        $this->assertEquals('invalid_duration_proposal', Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['result']);

        // VALIDATES PROPOSAL´S EXPIRATION DATE FORMAT
        echo "testOfferDataValidation(24)\n";
        $offer->getProposal()->setProposalDuration("24");
        $offer->getProposal()->setProposalValidUntil('20145-09-10');
        $this->assertEquals('expiration_date_wrong_date_format', Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['result']);

        echo "testOfferDataValidation(25)\n";
        $offer->getProposal()->setProposalValidUntil('2014-02-30');
        $this->assertEquals('expiration_date_invalid_date', Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['result']);

        // VALIDATES PROPOSAL´S EXPIRATION DATE AGAINST CURRENT SUBMISSION DATE(TODAY)
        echo "testOfferDataValidation(26)\n";
        $offer->getProposal()->setProposalValidUntil(date('Y-m-d', strtotime("-1 days")));
        $this->assertEquals('invalid_expiration_submission_date', Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['result']);

        echo "testOfferDataValidation(27)\n";
        $offer->getProposal()->setProposalValidUntil(date('Y-m-d', strtotime("-30 days")));
        $this->assertEquals('invalid_expiration_submission_date', Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['result']);

        // VALIDATES IF PROPOSAL´S EXPIRATION DATE IS OLDER THAN PROPOSAL'S START DATE
        echo "testOfferDataValidation(28)\n";
        $offer->getProposal()->setProposalStartDate(date('Y-m-d', strtotime("10 days")));
        $offer->getProposal()->setProposalEndDate(date('Y-m-d', strtotime("730 days")));
        $offer->getProposal()->setProposalValidUntil(date('Y-m-d', strtotime("20 days")));
        $this->assertEquals('invalid_expiration_start_date', Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['result']);

        echo "testOfferDataValidation(29)\n";
        $offer->getProposal()->setProposalValidUntil(date('Y-m-d', strtotime("900 days")));
        $this->assertEquals('invalid_expiration_start_date', Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['result']);

        // VALIDATES IF INSTITUTION´S COUNTRY AND CITY ARE VALID AND IF THE CITY BELONGS TO THE COUNTRY
        echo "testOfferDataValidation(30)\n";
        $offer->getProposal()->setProposalValidUntil(date('Y-m-d', strtotime("10 days")));
        $offer->getInstitution()->setInstitutionCountry("Port");
        $offer->getInstitution()->setInstitutionCity(null);
        $this->assertEquals('invalid_country_institution', Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['result']);

        echo "testOfferDataValidation(31)\n";
        $offer->getInstitution()->setInstitutionCountry(null);
        $offer->getInstitution()->setInstitutionCity("cit");
        $this->assertEquals('invalid_city_institution', Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['result']);

        echo "testOfferDataValidation(32)\n";
        $offer->getInstitution()->setInstitutionCountry("PORTUGAL");
        $offer->getInstitution()->setInstitutionCity("Ghent");
        $this->assertEquals('no_match_country_city_institution', Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['result']);

        // VALIDATES IF HOST´S COUNTRY AND CITY ARE VALID AND IF THE CITY BELONGS TO THE COUNTRY
        echo "testOfferDataValidation(33)\n";
        $offer->getInstitution()->setInstitutionCountry("PORTUGAL");
        $offer->getInstitution()->setInstitutionCity("Porto");
        $offer->getHost()->setInstitutionCountry("Port");
        $offer->getHost()->setInstitutionCity(null);
        $this->assertEquals('invalid_country_host', Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['result']);

        echo "testOfferDataValidation(34)\n";
        $offer->getHost()->setInstitutionCountry(null);
        $offer->getHost()->setInstitutionCity("cit");
        $this->assertEquals('invalid_city_host', Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['result']);

        echo "testOfferDataValidation(35)\n";
        $offer->getHost()->setInstitutionCountry("PORTUGAL");
        $offer->getHost()->setInstitutionCity("Ghent");
        $this->assertEquals('no_match_country_city_host', Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['result']);

        // VALIDATES IF INSTITUTTION´S TYPE NAME EXISTS
        echo "testOfferDataValidation(36)\n";
        $offer->getHost()->setInstitutionCountry("PORTUGAL");
        $offer->getHost()->setInstitutionCity("Porto");
        $offer->getInstitution()->setInstitutionType("qq");
        $this->assertEquals('invalid_institution_type', Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['result']);

        // VALIDATES IF HOST´S TYPE NAME EXISTS
        echo "testOfferDataValidation(37)\n";
        $offer->getInstitution()->setInstitutionType("University");
        $offer->getHost()->setInstitutionType("qq");
        $this->assertEquals('invalid_host_type', Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['result']);

        // VALIDATES LANGUAGES
        echo "testOfferDataValidation(38)\n";
        $offer->getHost()->setInstitutionType("University");
        $offer->getProposal()->setProposalLanguages(array('llklklk', 'djfjf', 'wewewe'));
        $this->assertEquals('invalid_languages_proposal', Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['result']);

        echo "testOfferDataValidation(39)\n";
        $this->assertCount(3, Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['languages']);

        echo "testOfferDataValidation(40)\n";
        $offer->getProposal()->setProposalLanguages(array('llklklk'));
        $this->assertEquals('invalid_languages_proposal', Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['result']);

        echo "testOfferDataValidation(41)\n";
        $this->assertCount(1, Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['languages']);

        // VALIDATES STUDY DEGREES
        echo "testOfferDataValidation(42)\n";
        $offer->getProposal()->setProposalLanguages(array('Bulgarian'));
        $offer->getProposal()->setProposalStudyDegrees(array('dfdfdf', 'dfdf', 'tyotiy'));
        $this->assertEquals('invalid_study_degrees_proposal', Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['result']);

        echo "testOfferDataValidation(43)\n";
        $this->assertCount(3, Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['degrees']);

        echo "testOfferDataValidation(44)\n";
        $offer->getProposal()->setProposalStudyDegrees(array('dfdfdf'));
        $this->assertEquals('invalid_study_degrees_proposal', Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['result']);

        echo "testOfferDataValidation(45)\n";
        $this->assertCount(1, Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['degrees']);

        // VALIDATES STUDY AREAS
        echo "testOfferDataValidation(46)\n";
        $offer->getProposal()->setProposalStudyDegrees(array('Bachelor'));
        $offer->getProposal()->setProposalStudyAreas(array('dddd', 'dfdfdf', 'ererer'));
        $this->assertEquals('invalid_study_areas_proposal', Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['result']);

        echo "testOfferDataValidation(47)\n";
        $this->assertCount(3, Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['areas']);

        echo "testOfferDataValidation(48)\n";
        $offer->getProposal()->setProposalStudyAreas(array('dfdfdf'));
        $this->assertEquals('invalid_study_areas_proposal', Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['result']);

        echo "testOfferDataValidation(49)\n";
        $this->assertCount(1, Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['areas']);

        // VALIDATES TARGETS
        echo "testOfferDataValidation(50)\n";
        $offer->getProposal()->setProposalStudyAreas(array('Design'));
        $offer->getProposal()->setProposalTargets(array('dddd', 'dfdfdf', 'ererer'));
        $this->assertEquals('invalid_targets_proposal', Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['result']);

        echo "testOfferDataValidation(51)\n";
        $this->assertCount(3, Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['targets']);

        echo "testOfferDataValidation(52)\n";
        $offer->getProposal()->setProposalTargets(array('dfdfdf'));
        $this->assertEquals('invalid_targets_proposal', Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['result']);

        echo "testOfferDataValidation(53)\n";
        $this->assertCount(1, Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['targets']);

        // VALIDATES PROMOTED SKILLS
        echo "testOfferDataValidation(54)\n";
        $offer->getProposal()->setProposalTargets(array('Project'));
        $offer->getProposal()->setProposalPromotedSkills(array('dddd', 'dfdfdf', 'ererer'));
        $this->assertEquals('invalid_promoted_skills_proposal', Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['result']);

        echo "testOfferDataValidation(55)\n";
        $this->assertCount(3, Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['skills']);

        echo "testOfferDataValidation(56)\n";
        $offer->getProposal()->setProposalPromotedSkills(array('dfdfdf'));
        $this->assertEquals('invalid_promoted_skills_proposal', Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['result']);

        echo "testOfferDataValidation(57)\n";
        $this->assertCount(1, Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['skills']);

        // VALIDATES REQUIRED SKILLS
        echo "testOfferDataValidation(58\n";
        $offer->getProposal()->setProposalPromotedSkills(array('Behaviours - Flexibility and willingness to cope with change'));
        $offer->getProposal()->setProposalRequiredSkills(array('dddd', 'dfdfdf', 'ererer'));
        $this->assertEquals('invalid_required_skills_proposal', Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['result']);

        echo "testOfferDataValidation(59)\n";
        $this->assertCount(3, Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['skills']);

        echo "testOfferDataValidation(60)\n";
        $offer->getProposal()->setProposalRequiredSkills(array('dfdfdf'));
        $this->assertEquals('invalid_required_skills_proposal', Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['result']);

        echo "testOfferDataValidation(61)\n";
        $this->assertCount(1, Offer_Data_Validation::offerDataValidation($offer, $xmlPst)['skills']);

        echo "testOfferDataValidation(OK!)\n\n";
    }

}

?>
