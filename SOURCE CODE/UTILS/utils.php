<?php

/**
 * FILE WITH UTILITIES FOR THE APPLICATION
 */

/**
 * 
 * SANITIZES A STRING
 * 
 * @param String $string - String to sanitize
 * @return String - Sanitized string
 */
function sanitizeString($string) {

    $string = strip_tags($string);
    $string = str_replace(array('\\', '/'), '', $string);

    return $string;
}

/**
 * 
 * HANDLES A FILE DOWNLOAD
 * 
 * @param String $filePath - Absolute path to the file to download
 * @param String $fileName - Name of the file
 */
function downloadFile($filePath, $fileName) {

    $mimeTypes = array(
        'XML' => 'application/xml',
        'PDF' => 'application/pdf',
        'DOC' => 'application/msword',
        'DOCX' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        'RTF' => 'application/rtf',
        'XLS' => 'application/vnd.ms-excel',
        'XLSX' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        'PPT' => 'application/vnd.ms-powerpoint',
        'PPTX' => 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
        'ODT' => 'application/vnd.oasis.opendocument.text',
        'ODS' => 'application/vnd.oasis.opendocument.spreadsheet',
        'JPE' => 'image/jpeg',
        'JPEG' => 'image/jpeg',
        'JPG' => 'image/jpeg',
        'GIF' => 'image/gif',
        'BMP' => 'image/bmp',
        'ICO' => 'image/x-icon',
        'PNG' => 'image/png',
        'TIF' => 'image/tiff',
        'TXT' => 'text/plain',
        'PDF' => 'application/pdf',
        'DOC' => 'application/msword',
        'DOCX' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        'RTF' => 'application/rtf',
        'XLS' => 'application/vnd.ms-excel',
        'XLSX' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        'PPT' => 'application/vnd.ms-powerpoint',
        'PPTX' => 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
        'ODT' => 'application/vnd.oasis.opendocument.text',
        'ODS' => 'application/vnd.oasis.opendocument.spreadsheet',
        'JPE' => 'image/jpeg',
        'JPEG' => 'image/jpeg',
        'JPG' => 'image/jpeg',
        'GIF' => 'image/gif',
        'BMP' => 'image/bmp',
        'ICO' => 'image/x-icon',
        'PNG' => 'image/png',
        'TIF' => 'image/tiff',
        'TXT' => 'text/plain',
        'ZIP' => 'application/zip');

    // GETS EXTENSION FILE
    $split = explode(".", $filePath);
    $ext = end($split);

    $mime;

    // GETS MIME CONTENT TYPE
    foreach ($mimeTypes as $key => $value) {

        if (strcasecmp($ext, $key) == 0) {

            $mime = $value;
            break;
        }
    }

    header("Content-disposition: attachment; filename=" . $fileName);
    header("Content-type: " . $mime);

    readfile($filePath);
}
?>

