<?php

require_once '../../../DAL/DATABASES/MySQL_DB.php';

class MySQL_DBTest extends PHPUnit_Framework_TestCase {

    public function testSelect() {

        // WARNING //
        // Comment the 'Logs::writeLog(...)' instruction
        // of the method 'select(...)' at '...DAL/DATABASES/MySQL_DB.php' file
        // to perform the test.

        $dbObject = new MySQL_DB("localhost:3306", "praxis", "root", "");

        echo "testSelect(1)\n";
        $this->assertFalse($dbObject->select("SELECT * FROM stuyar"));
        echo "testSelect(2)\n";
        $this->assertInternalType('array', $dbObject->select("SELECT * FROM proposal"));
        echo "testSelect(3)\n";
        $this->assertInternalType('array', $dbObject->select("SELECT * FROM studyarea"));
        echo "testSelect(4)\n";
        $this->assertInternalType('array', $dbObject->select("SELECT * FROM studyarea WHERE id = 500"));

        echo "testSelect(OK!)\n\n";

        $dbObject->closeDBConnection();
    }

    public function testInsert() {

        // WARNING //
        // Comment the 'Logs::writeLog(...)' instruction
        // of the method 'insert(...)' at '...DAL/DATABASES/MySQL_DB.php' file
        // to perform the test.

        $dbObject = new MySQL_DB("localhost:3306", "praxis", "root", "");

        echo "testInsert(1)\n";
        $this->assertFalse($dbObject->insert("INSERT INTO stuyar ( name , topic ) VALUES( 'other' , 'topic' )"));
        echo "testInsert(2)\n";
        $this->assertInternalType('int', $dbObject->insert("INSERT INTO studyarea ( name , topic ) VALUES( 'other' , 'topic' )"));
        echo "testInsert(3)\n";
        $this->assertFalse($dbObject->insert("INSERT INTO stuyar ( name , topic ) VALUES( 'other' , 'topic' )", false));
        echo "testInsert(4)\n";
        $this->assertTrue($dbObject->insert("INSERT INTO studyarea ( name , topic ) VALUES( 'other' , 'topic' )", false));

        echo "testInsert(OK!)\n\n";
    }

    public function testDelete() {

        // WARNING //
        // Comment the 'Logs::writeLog(...)' instruction
        // of the method 'delete(...)' at '...DAL/DATABASES/MySQL_DB.php' file
        // to perform the test.

        $dbObject = new MySQL_DB("localhost:3306", "praxis", "root", "");

        echo "testDelete(1)\n";
        $this->assertFalse($dbObject->delete("DELETE FROM stuyar WHERE id = 1"));
        echo "testDelete(2)\n";
        $this->assertTrue($dbObject->delete("DELETE FROM studyarea WHERE id = 1"));

        echo "testDelete(OK!)\n\n";
    }

    public function testUpdate() {

        // WARNING //
        // Comment the 'Logs::writeLog(...)' instruction
        // of the method 'update(...)' at '...DAL/DATABASES/MySQL_DB.php' file
        // to perform the test.

        $dbObject = new MySQL_DB("localhost:3306", "praxis", "root", "");

        echo "testUpdate(1)\n";
        $this->assertFalse($dbObject->update("UPDATE stuyar SET name = 'Study Area' WHERE id = 1"));
        echo "testUpdate(2)\n";
        $this->assertTrue($dbObject->update("UPDATE studyarea SET name = 'Study Area' WHERE id = 1"));

        echo "testUpdate(OK!)\n\n";
    }

}
