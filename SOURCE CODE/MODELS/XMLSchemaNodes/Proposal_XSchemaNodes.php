<?php

/**
 * CLASS OF SERVICES THAT ALLOWS TO GET THE NODE´S DECLARATION OF THE PROPOSAL´S DETAILS
 */
class Proposal_XSchemaNodes {
    /** STATIC METHODS * */

    /**
     * 
     * GETS ELEMENT NODE´S DECLARATION OF A PROPOSAL´S DETAIL
     * 
     * @param int $nodeID
     * @return String - Element node´s declaration
     */
    static function getDeclarationNode($nodeID) {

        switch ($nodeID) {
            case 0:

                return self::getTitleNode();
                break;

            case 1:

                return self::getDescriptionNode();
                break;

            case 2:

                return self::getStudyAreaNode();
            default:
                break;

            case 3:

                return self::getStudyDegreeNode();
                break;

            case 4:

                return self::getTargetNode();
                break;

            case 5:

                return self::getLanguageNode();
                break;

            case 6:

                return self::getCountryNode();
                break;

            case 7:

                return self::getCityNode();
                break;

            case 8:

                return self::getContactNode();
                break;

            case 10:

                return self::getSuperVisorFSTNameNode();
                break;

            case 11:

                return self::getSuperVisorLSTNameNode();
                break;

            case 12:

                return self::getSuperVisorEmailNode();
                break;

            case 13:

                return self::getDurationNode();
                break;

            case 14:

                return self::getStartDateNode();
                break;

            case 15:

                return self::getEndDateNode();
                break;

            case 16:

                return self::getVacanciesNode();
                break;

            case 17:

                return self::getPaymentNode();
                break;

            case 18:

                return self::getGlobalSkillNode();
                break;

            case 19:

                return self::getJobSkillNode();
                break;

            case 20:

                return self::getRequiredSkillNode();
                break;

            case 21:

                return self::getPromotedSkillNode();
                break;

            case 22:

                return self::getBenefitNode();
                break;

            case 23:
                return self::getValidUntilNode();
                break;
        }
    }

    /**
     * 
     * GETS PROPOSAL TITLE ELEMENT DECLARATION
     * 
     * @return String - Node element declaration that represents the title of a Proposal
     */
    static function getTitleNode() {

        $titleNode = "<xs:element name='PROPOSAL_TITLE' type='xs:string' maxOccurs='1' />";

        return $titleNode;
    }

    /**
     * 
     * GETS PROPOSAL DESCRIPTION ELEMENT DECLARATION
     * 
     * @return String - Node element declaration that represents the description of the Proposal
     */
    static function getDescriptionNode() {

        $descriptionNode = "<xs:element name='PROPOSAL_DESCRIPTION' type='xs:string' maxOccurs='1' />";

        return $descriptionNode;
    }

    /**
     * 
     * GETS PROPOSAL STUDY AREA ELEMENT DECLARATION
     * 
     * @return String - Node element declaration that represents the area of study of the Proposal
     */
    static function getStudyAreaNode() {

        $studyArea = "<xs:element name='PROPOSAL_STUDY_AREA' type='xs:string' maxOccurs='unbounded' />";

        return $studyArea;
    }

    /**
     * 
     * GETS PROPOSAL STUDY DEGREE ELEMENT DECLARATION
     * 
     * @return String - Node element declaration that represents the required study degree of the candidate for the Proposal
     */
    static function getStudyDegreeNode() {

        $studyDegree = "<xs:element name='PROPOSAL_STUDY_DEGREE' type='xs:string' maxOccurs='unbounded' />";

        return $studyDegree;
    }

    /**
     * 
     * GETS PROPOSAL TARGET ELEMENT DECLARATION
     * 
     * @return String - Node element declaration that represents a target to achieve while performing the Proposal
     */
    static function getTargetNode() {

        $target = "<xs:element name='PROPOSAL_TARGET' type='xs:string' maxOccurs='unbounded' />";

        return $target;
    }

    /**
     * 
     * GETS PROPOSAL LANGUAGE ELEMENT DECLARATION
     * 
     * @return String - Node element declaration that represents a required language to perform the Proposal
     */
    static function getLanguageNode() {

        $language = "<xs:element name='PROPOSAL_LANGUAGE' type='xs:string' maxOccurs='unbounded' />";

        return $language;
    }

    /**
     * 
     * GETS PROPOSAL COUNTRY ELEMENT DECLARATION
     * 
     * @return String - Node element declaration that represents the country that made the Proposal
     */
    static function getCountryNode() {

        $country = "<xs:element name='PROPOSAL_COUNTRY' type='xs:string' maxOccurs='1' />";

        return $country;
    }

    /**
     * 
     * GETS CITY ELEMENT DECLARATION
     * 
     * @return String - Node element declaration that represents the city that made the Proposal
     */
    static function getCityNode() {

        $city = "<xs:element name='PROPOSAL_CITY' type='xs:string' maxOccurs='1'/>";

        return $city;
    }

    /**
     * 
     * GETS PROPOSAL CONTACT ELEMENT DECLARATION
     * 
     * @return String - Node element declaration that represents the contact for the Proposal
     */
    static function getContactNode() {

        $contact = "<xs:element name='PROPOSAL_CONTACT' type='xs:string' maxOccurs='1' />";

        return $contact;
    }

    /**
     * 
     * GETS PROPOSAL SUPERVISOR FIRST NAME ELEMENT DECLARATION
     * 
     * @return String - Node element declaration that represent the first name of the Proposal´s supervisor
     */
    static function getSuperVisorFSTNameNode() {

        $superVisorFSTName = "<xs:element name='PROPOSAL_SUPERVISORFSTNAME' type='xs:string' maxOccurs='1' />";

        return $superVisorFSTName;
    }

    /**
     * 
     * GETS PROPOSAL SUPERVISOR LAST NAME ELEMENT DECLARATION
     * 
     * @return String - Node element declaration that represent the first name of the Proposal´s supervisor
     */
    static function getSuperVisorLSTNameNode() {

        $superVisorLSTName = "<xs:element name='PROPOSAL_SUPERVISORLSTNAME' type='xs:string' maxOccurs='1' />";

        return $superVisorLSTName;
    }

    /**
     * 
     * GETS PROPOSAL SUPERVISOR EMAIL CONTACT ELEMENT DECLARATION
     * 
     * @return String - Node element declaration that represents the email contact of the Proposal´s supervisor
     */
    static function getSuperVisorEmailNode() {

        $superVisorEmail = "<xs:element name='PROPOSAL_SUPERVISOREMAIL' type='xs:string' maxOccurs='1' />";

        return $superVisorEmail;
    }

    /**
     * 
     * GETS PROPOSAL DURATION ELEMENT DECLARATION
     * 
     * @return String - Node element declaration that represents the duration of the project
     */
    static function getDurationNode() {

        $duration = "<xs:element name='PROPOSAL_DURATION' type='xs:string' maxOccurs='1' />";

        return $duration;
    }

    /**
     * 
     * GETS PROPOSAL START DATE ELEMENT DECLARATION
     * 
     * @return String - Node element declaration that represents the startind date for the project
     */
    static function getStartDateNode() {

        $startDate = "<xs:element name='PROPOSAL_STARTDATE' type='xs:string' maxOccurs='1' />";

        return $startDate;
    }

    /**
     * 
     * GETS PROPOSAL END DATE ELEMENT DECLARATION
     * 
     * @return String - Node element declaration that represents the ending date for the project
     */
    static function getEndDateNode() {

        $endDate = "<xs:element name='PROPOSAL_ENDDATE' type='xs:string' maxOccurs='1' />";

        return $endDate;
    }

    /**
     * 
     * GETS PROPOSAL VACANCIES ELEMENT DECLARATION
     * 
     * @return String - Node that represents the information about vacancies/holidays during the project
     */
    static function getVacanciesNode() {

        $vacancies = "<xs:element name='PROPOSAL_VACANCIES' type='xs:string' maxOccurs='1' />";

        return $vacancies;
    }

    /**
     * 
     * GETS PROPOSAL PAYMENT ELEMENT DECLARATION
     * 
     * @return String - Node element declaration that represents the information about some form of payment to the candidate´s project
     */
    static function getPaymentNode() {

        $payment = "<xs:element name='PROPOSAL_PAYMENT' type='xs:string' maxOccurs='1' />";

        return $payment;
    }

    /**
     * 
     * GETS PROPOSAL GLOBAL SKILL ELEMENT DECLARATION
     * 
     * @return String - Node element declaration that represents a required global skill from the candidate´s Proposal
     */
    static function getGlobalSkillNode() {

        $skill = "<xs:element name='PROPOSAL_GLOBALSKILL' type='xs:string' maxOccurs='1' />";

        return $skill;
    }

    /**
     * 
     * GETS PROPOSAL JOB SKILL ELEMENT DECLARATION
     * 
     * @return String - Node element declaration that represents a required job skill from the candidate´s Proposal
     */
    static function getJobSkillNode() {

        $skill = "<xs:element name='PROPOSAL_JOBSKILL' type='xs:string' maxOccurs='1' />";

        return $skill;
    }

    /**
     * 
     * GETS PROPOSAL REQUIRED SKILL ELEMENT DECLARATION
     * 
     * @return String - Node element declaration that represents a required skill from the candidate´s Proposal
     */
    static function getRequiredSkillNode() {

        $skill = "<xs:element name='PROPOSAL_REQUIREDSKILL' type='xs:string' maxOccurs='unbounded' />";

        return $skill;
    }

    /**
     * 
     * GETS PROPOSAL PROMOTED SKILL ELEMENT DECLARATION
     * 
     * @return String - Node element declaration that represents a promoted skill from the candidate´s Proposal
     */
    static function getPromotedSkillNode() {

        $skill = "<xs:element name='PROPOSAL_PROMOTEDSKILL' type='xs:string' maxOccurs='unbounded' />";

        return $skill;
    }

    /**
     * 
     * GETS PROPOSAL BENEFIT ELEMENT DECLARATION
     * 
     * @return String - Node element declaration that represents a benefit of the candidate´s Proposal
     */
    static function getBenefitNode() {

        $benefit = "<xs:element name='PROPOSAL_BENEFIT' type='xs:string' maxOccurs='1' />";

        return $benefit;
    }
    
    /**
     * 
     * GETS PROPOSAL VALID UNTIL DATE ELEMENT DECLARATION
     * 
     * @return String - Node element declaration that represents a valid until´s Proposal
     */
    static function getValidUntilNode() {

        $validUntil = "<xs:element name='PROPOSAL_VALIDUNTIL' type='xs:string' maxOccurs='unbounded' />";

        return $validUntil;
    }

}

?>
