/**
 * SCRIPT TO HANDLE EVENTS OF THE 'xml_schema_editor.html' page
 */

// GETS PROPOSAL ELEMENT NODE´S DECLARATION
function getProposalElement(nodeID) {

    $.ajax({
        url: 'editor?proposal_node=' + nodeID,
        type: 'GET',
        datatype: 'json',
        success: function (msg) {

            insertTextAreaAtCursor("schema_document", msg['node']);
        },
        error: function (xhr, status, error) {

            console.log(" xhr.responseText: " + xhr.responseText + " \n\nSTATUS:\n " + status + " \n\nERROR:\n " + error);

            $("#modal-message-header").html("Request error&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
            $("#modal-message-content").html("<p>It wasn´t possible to make the request...</p>");

            $("#message").modal({
                zIndex: 9999
            });
            $("#message").modal('show');
        }
    });
}

// GETS INSTITUTION/HOST ELEMENT NODE'S DECLARATION
function getInstitutionElement(nodeID, institutionType) {

    $.ajax({
        url: 'editor?institution_node=' + nodeID + "&institution_type=" + institutionType,
        type: 'GET',
        datatype: 'json',
        success: function (msg) {

            insertTextAreaAtCursor("schema_document", msg['node']);
        },
        error: function (xhr, status, error) {

            console.log(" xhr.responseText: " + xhr.responseText + " \n\nSTATUS:\n " + status + " \n\nERROR:\n " + error);

            $("#modal-message-header").html("Request error&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
            $("#modal-message-content").html("<p>It wasn´t possible to make the request...</p>");

            $("#message").modal({
                zIndex: 9999
            });
            $("#message").modal('show');
        }
    });
}

// GETS XML SCHEMA DOCUMENT NODE'S DECLARATION
function getDocElement(nodeID) {

    $.ajax({
        url: 'editor?document_node=' + nodeID,
        type: 'GET',
        datatype: 'json',
        success: function (msg) {

            insertTextAreaAtCursor("schema_document", msg['node']);
        },
        error: function (xhr, status, error) {

            console.log(" xhr.responseText: " + xhr.responseText + " \n\nSTATUS:\n " + status + " \n\nERROR:\n " + error);

            $("#modal-message-header").html("Request error&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
            $("#modal-message-content").html("<p>It wasn´t possible to make the request...</p>");

            $("#message").modal({
                zIndex: 9999
            });
            $("#message").modal('show');
        }
    });
}

// VALIDATES XML SCHEMA
function validatesXmlSchema() {

    var xsdDocument = $("#schema_document").val();

    if (xsdDocument == '') {

        $("#modal-message-header").html("Syntax validation&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
        $("#modal-message-content").html("<p>Blank document...Edit the document.</p>");

        $("#message").modal({
            zIndex: 9999
        });
        $("#message").modal('show');

        return;
    }

    $("#validateSave").val("validate");

    $("#editorForm").ajaxSubmit({
        url: 'editor',
        datatype: 'json',
        success: function (msg) {

            if (msg['result'] == 'validSyntax') {

                $("#modal-message-header").html("Syntax validation...");
                $("#modal-message-content").html("<p>Document´s syntax is valid&nbsp;<span class='glyphicon glyphicon-thumbs-up'></span></p>");

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'invalidSyntax') {

                $("#modal-message-header").html("Syntax validation...");
                $("#modal-message-content").html("<p>Document´s syntax is not valid&nbsp;<span class='glyphicon glyphicon-thumbs-down'></span></p><br><h5><u>Errors:</u></h5>");

                for (var i = 0; i < msg['errors'].length; i++) {

                    $("#modal-message-content").append("<br>" + msg['errors'][i]);
                }

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'fileCreationError' || msg['result'] == 'fileWriteError') {

                $("#modal-message-header").html("Syntax validation...");
                $("#modal-message-content").html("<p>It was not possible to validate the document syntax&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span></p>");

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }
        },
        error: function (xhr, status, error) {

            console.log(" xhr.responseText: " + xhr.responseText + " \n\nSTATUS:\n " + status + " \n\nERROR:\n " + error);

            $("#modal-message-header").html("Request error&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
            $("#modal-message-content").html("<p>It was not possible to make the request...</p>");

            $("#message").modal({
                zIndex: 9999
            });
            $("#message").modal('show');
        }
    });
}

// SAVES PROVIDER´S XML SCHEMA
function saveXmlSchema() {

    // VALIDATES PROVIDER´S XML SCHEMA
    var xsdDocument = $("#schema_document").val();

    if (xsdDocument == '') {

        $("#modal-message-header").html("Syntax validation...");
        $("#modal-message-content").html("<p>Blank document...Edit the document.&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span></p>");

        $("#message").modal({
            zIndex: 9999
        });
        $("#message").modal('show');

        return;
    }

    $("#validateSave").val("validate");

    $("#editorForm").ajaxSubmit({
        url: 'editor',
        datatype: 'json',
        success: function (msg) {

            if (msg['result'] == 'validSyntax') {

                $("#validateSave").val("save");

                // SAVES PROVIDER´S XML SCHEMA
                $("#editorForm").ajaxSubmit({
                    url: 'editor',
                    datatype: 'json',
                    success: function (saveMsg) {

                        if (saveMsg['result'] == 'xmlSchemaSaved') {

                            $("#modal-message-header").html("Xml Schema status...");
                            $("#modal-message-content").html("<p>Provider´s Xml Schema was successfully saved/updated.&nbsp;<span class='glyphicon glyphicon-thumbs-up'></span></p>");

                            $("#message").modal({
                                zIndex: 9999
                            });
                            $("#message").modal('show');
                        }

                        if (saveMsg['result'] == 'saveModificationTypeUpdateError' ||
                                saveMsg['result'] == 'saveModificationUpdateError' ||
                                saveMsg['result'] == 'saveModificationTypeCreationError' ||
                                saveMsg['result'] == 'saveInsertNewSchemaError' ||
                                saveMsg['result'] == 'saveInsertModificationCreationError' ||
                                saveMsg['result'] == 'saveFileCreationOpenError' ||
                                saveMsg['result'] == 'saveFileWriteError' ||
                                saveMsg['result'] == 'xmlSchemaAccessError') {

                            $("#modal-message-header").html("Xml Schema status...");
                            $("#modal-message-content").html("<p>It was not possible to save the document...&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span></p>");
                            $("#message").modal({
                                zIndex: 9999
                            });
                            $("#message").modal('show');
                        }
                    },
                    error: function (xhr, status, error) {

                        console.log(" xhr.responseText: " + xhr.responseText + " \n\nSTATUS:\n " + status + " \n\nERROR:\n " + error);

                        $("#modal-message-header").html("Request error&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                        $("#modal-message-content").html("<p>It was not possible to make the request to save the document...</p>");

                        $("#message").modal({
                            zIndex: 9999
                        });
                        $("#message").modal('show');
                    }
                });
            }

            if (msg['result'] == 'invalidSyntax') {

                $("#modal-message-header").html("Syntax validation...");
                $("#modal-message-content").html("<p>Document´s syntax is not valid&nbsp;<span class='glyphicon glyphicon-thumbs-down'></span></p><br><h5><u>Errors:</u></h5>");

                for (var i = 0; i < msg['errors'].length; i++) {

                    $("#modal-message-content").append("<br>" + msg['errors'][i]);
                }

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'fileCreationError' || msg['result'] == 'fileWriteError') {

                $("#modal-message-header").html("Syntax validation...");
                $("#modal-message-content").html("<p>It was not possible to validate the document syntax&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span></p>");

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }
        },
        error: function (xhr, status, error) {

            console.log(" xhr.responseText: " + xhr.responseText + " \n\nSTATUS:\n " + status + " \n\nERROR:\n " + error);

            $("#modal-message-header").html("Request error&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
            $("#modal-message-content").html("<p>It was not possible to make the request...</p>");

            $("#message").modal({
                zIndex: 9999
            });
            $("#message").modal('show');
        }
    });
}

// INSERT TEXT IN THE TEXTAREA CURSOR
function insertTextAreaAtCursor(areaId, text) {

    var txtarea = document.getElementById(areaId);
    var scrollPos = txtarea.scrollTop;
    var strPos = 0;
    var br = ((txtarea.selectionStart || txtarea.selectionStart == '0') ?
            "ff" : (document.selection ? "ie" : false));
    if (br == "ie") {
        txtarea.focus();
        var range = document.selection.createRange();
        range.moveStart('character', -txtarea.value.length);
        strPos = range.text.length;
    }
    else if (br == "ff")
        strPos = txtarea.selectionStart;

    var front = (txtarea.value).substring(0, strPos);
    var back = (txtarea.value).substring(strPos, txtarea.value.length);
    txtarea.value = front + text + back;
    strPos = strPos + text.length;
    if (br == "ie") {
        txtarea.focus();
        var range = document.selection.createRange();
        range.moveStart('character', -txtarea.value.length);
        range.moveStart('character', strPos);
        range.moveEnd('character', 0);
        range.select();
    }
    else if (br == "ff") {
        txtarea.selectionStart = strPos;
        txtarea.selectionEnd = strPos;
        txtarea.focus();
    }
    txtarea.scrollTop = scrollPos;
}

function sizeEditor() {

    var editorSize = (parseInt($("#footer").position().top) - parseInt($("#schema_document").position().top)) - parseInt($("#footer").height());

    //var screenHeight = screen.height;
    //var editorSize = 0.35 * screenHeight;

    $("#schema_document").height(editorSize);
}

$(document).ready(function () {

    sizeEditor();

    $("#schema_document").linedtextarea({
        selectedLine: 1
    });
});