<?php

/**
 * INTERFACE TO HANDLE DATABASES ACCESS
 */
interface DataBaseAccess {
    
    function select($SQL);
    
    function insert($SQL, $idReturn);
    
    function delete($SQL);
    
    function update($SQL);
    
    function commitDBChanges();
    
    function rollbackDBChanges();
}
