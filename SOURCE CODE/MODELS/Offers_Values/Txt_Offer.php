<?php

/**
 * IMPORTS
 */
require_once '/../../MODELS/XMLSchemaNodes/SchemaNodes.php';
require_once '/../../MODELS/Offer_Templates/Offer_Templates.php';
require_once 'OfferValues.php';
require_once '/../Offer/Offer.php';

/**
 * CLASS OF SERVICE TO MANAGE TXT DOCUMENT´S FIELDS
 */
class Txt_Offer implements OfferValues {

    /**
     * 
     * GETS ALL VALUES OF THE OFFER´S FIELDS
     * 
     * @param String $txtPath - Absolte path of txt offer
     * @param String $schemaDocPath - Abosulte path to the Xml Schema
     * @return Offer - Offer object with all values of the offer
     */
    function getOfferFieldsValues($txtPath, $schemaDocPath) {

        $offer = new Offer();

        // GETS ALL OFFER'S NODES
        $offerNodes = SchemaNodes::getOfferNodes();

        // GENERATES XML TEMPLATE
        $xmlTemplateContent = Offer_Templates::generateXML($schemaDocPath);

        if (!$xmlTemplateContent)
            return false;

        file_put_contents($_SERVER['DOCUMENT_ROOT'] . "/PRAXIS/TEMP/xml_template.xml", $xmlTemplateContent);

        $xml = simplexml_load_file($_SERVER['DOCUMENT_ROOT'] . "/PRAXIS/TEMP/xml_template.xml");

        // GETS THE CORRECT FIELDS NAMES TO MATCH AGAINST THE SUBMITED OFFER
        $fieldElements = Offer_Templates::getXmlFieldNames($xml, $offerNodes);
        unlink($_SERVER['DOCUMENT_ROOT'] . "/PRAXIS/TEMP/xml_template.xml");

        $fieldsNames = array();

        foreach ($fieldElements as $field) {

            array_push($fieldsNames, $field['fieldName']);
        }

        // GET RID OF THE BLANK LINES
        $content = utf8_encode(file_get_contents($txtPath));
        $newContent = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $content);

        $file = tmpfile();

        if (!fwrite($file, $newContent))
            return false;

        fseek($file, 0);

        if ($file) {

            foreach ($fieldElements as $element) {

                while (!feof($file)) {

                    $line = trim(fgets($file));

                    if ($line == $element['fieldName']) {

                        if ($element['info']['type'] == "text") {

                            $line = trim(fgets($file));
                            $split = explode("(text)", $line);

                            if ($element['info']['element'] == "PROPOSAL_TITLE") {

                                $offer->getProposal()->setProposalTitle(trim($split[1]));
                            }

                            if ($element['info']['element'] == "PROPOSAL_CONTACT") {

                                $offer->getProposal()->setProposalContact(trim($split[1]));
                            }

                            if ($element['info']['element'] == "PROPOSAL_SUPERVISORFSTNAME") {

                                $offer->getProposal()->setProposalSupervisorFstName(trim($split[1]));
                            }

                            if ($element['info']['element'] == "PROPOSAL_SUPERVISORLSTNAME") {

                                $offer->getProposal()->setProposalSupervisorLstName(trim($split[1]));
                            }

                            if ($element['info']['element'] == "PROPOSAL_SUPERVISOREMAIL") {

                                $offer->getProposal()->setProposalSupervisorEmail(trim($split[1]));
                            }

                            if ($element['info']['element'] == "PROPOSAL_STARTDATE") {

                                $offer->getProposal()->setProposalStartDate(trim($split[1]));
                            }

                            if ($element['info']['element'] == "PROPOSAL_ENDDATE") {

                                $offer->getProposal()->setProposalEndDate(trim($split[1]));
                            }

                            if ($element['info']['element'] == "PROPOSAL_VACANCIES") {

                                $offer->getProposal()->setProposalVacancies(trim($split[1]));
                            }

                            if ($element['info']['element'] == "PROPOSAL_PAYMENT") {

                                $offer->getProposal()->setProposalPayment(trim($split[1]));
                            }

                            if ($element['info']['element'] == "PROPOSAL_GLOBALSKILL") {

                                $offer->getProposal()->setProposalGlobalSkills(trim($split[1]));
                            }

                            if ($element['info']['element'] == "PROPOSAL_JOBSKILL") {

                                $offer->getProposal()->setProposalJobSkills(trim($split[1]));
                            }

                            if ($element['info']['element'] == "PROPOSAL_BENEFIT") {

                                $offer->getProposal()->setProposalBenefits(trim($split[1]));
                            }

                            if ($element['info']['element'] == "PROPOSAL_VALIDUNTIL") {

                                $offer->getProposal()->setProposalValidUntil(trim($split[1]));
                            }

                            if ($element['info']['element'] == "INSTITUTION_NAME") {

                                $offer->getInstitution()->setInstitutionName(trim($split[1]));
                            }

                            if ($element['info']['element'] == "INSTITUTION_CONTACTFSTNAME") {

                                $offer->getInstitution()->setInstitutionContactFstName(trim($split[1]));
                            }

                            if ($element['info']['element'] == "INSTITUTION_CONTACTLSTNAME") {

                                $offer->getInstitution()->setInstitutionContactLstName(trim($split[1]));
                            }

                            if ($element['info']['element'] == "INSTITUTION_PHONE") {

                                $offer->getInstitution()->setInstitutionPhone(trim($split[1]));
                            }

                            if ($element['info']['element'] == "INSTITUTION_EMAIL") {

                                $offer->getInstitution()->setInstitutionEmail(trim($split[1]));
                            }

                            if ($element['info']['element'] == "INSTITUTION_WEBSITE") {

                                $offer->getInstitution()->setInstitutionWebsite(trim($split[1]));
                            }

                            if ($element['info']['element'] == "HOST_NAME") {

                                $offer->getHost()->setInstitutionName(trim($split[1]));
                            }

                            if ($element['info']['element'] == "HOST_CONTACTFSTNAME") {

                                $offer->getHost()->setInstitutionContactFstName(trim($split[1]));
                            }

                            if ($element['info']['element'] == "HOST_CONTACTLSTNAME") {

                                $offer->getHost()->setInstitutionContactLstName(trim($split[1]));
                            }

                            if ($element['info']['element'] == "HOST_PHONE") {

                                $offer->getHost()->setInstitutionPhone(trim($split[1]));
                            }

                            if ($element['info']['element'] == "HOST_EMAIL") {

                                $offer->getHost()->setInstitutionEmail(trim($split[1]));
                            }

                            if ($element['info']['element'] == "HOST_WEBSITE") {

                                $offer->getHost()->setInstitutionWebsite(trim($split[1]));
                            }
                            break;
                        }

                        if ($element['info']['type'] == "text_area") {

                            $value = "";

                            $line = trim(fgets($file));
                            $split = explode("(text)", $line);
                            $value = trim($split[1]) . "\n";

                            do {

                                $line = trim(fgets($file));

                                if (!in_array($line, $fieldsNames))
                                    $value .= $line . "\n";
                            } while (!in_array($line, $fieldsNames));

                            if ($element['info']['element'] == "PROPOSAL_DESCRIPTION") {

                                $offer->getProposal()->setProposalDescription($value);
                            }

                            if ($element['info']['element'] == "INSTITUTION_DESCRIPTION") {

                                $offer->getInstitution()->setInstitutionDescription($value);
                            }

                            if ($element['info']['element'] == "HOST_DESCRIPTION") {

                                $offer->getHost()->setInstitutionDescription($value);
                            }
                            break;
                        }

                        if ($element['info']['type'] == "combo_box_sql") {

                            $comboBoxValues = array();

                            do {

                                $line = trim(fgets($file));

                                if (!in_array($line, $fieldsNames)) {

                                    $split = explode("(combobox)", $line);

                                    if (trim($split[1]) == "< CHOOSE ITEM >")
                                        continue;

                                    array_push($comboBoxValues, trim($split[1]));
                                }
                            } while (!in_array($line, $fieldsNames));

                            if ($element['info']['element'] == "PROPOSAL_STUDY_AREA") {

                                $offer->getProposal()->setProposalStudyAreas(array_unique($comboBoxValues));
                            }

                            if ($element['info']['element'] == "PROPOSAL_STUDY_DEGREE") {

                                $offer->getProposal()->setProposalStudyDegrees(array_unique($comboBoxValues));
                            }

                            if ($element['info']['element'] == "PROPOSAL_TARGET") {

                                $offer->getProposal()->setProposalTargets(array_unique($comboBoxValues));
                            }

                            if ($element['info']['element'] == "PROPOSAL_LANGUAGE") {

                                $offer->getProposal()->setProposalLanguages(array_unique($comboBoxValues));
                            }

                            if ($element['info']['element'] == "PROPOSAL_COUNTRY") {

                                $unique = array_unique($comboBoxValues);
                                $offer->getProposal()->setProposalCountry($unique[0]);
                            }

                            if ($element['info']['element'] == "PROPOSAL_CITY") {

                                $unique = array_unique($comboBoxValues);
                                $offer->getProposal()->setProposalCity($unique[0]);
                            }

                            if ($element['info']['element'] == "PROPOSAL_REQUIREDSKILL") {

                                $offer->getProposal()->setProposalRequiredSkills(array_unique($comboBoxValues));
                            }

                            if ($element['info']['element'] == "PROPOSAL_PROMOTEDSKILL") {

                                $offer->getProposal()->setProposalPromotedSkills(array_unique($comboBoxValues));
                            }

                            if ($element['info']['element'] == "INSTITUTION_TYPE") {

                                $unique = array_unique($comboBoxValues);
                                $offer->getInstitution()->setInstitutionType($unique[0]);
                            }

                            if ($element['info']['element'] == "INSTITUTION_COUNTRY") {

                                $unique = array_unique($comboBoxValues);
                                $offer->getInstitution()->setInstitutionCountry($unique[0]);
                            }

                            if ($element['info']['element'] == "INSTITUTION_CITY") {

                                $unique = array_unique($comboBoxValues);
                                $offer->getInstitution()->setInstitutionCity($unique[0]);
                            }

                            if ($element['info']['element'] == "HOST_TYPE") {

                                $unique = array_unique($comboBoxValues);
                                $offer->getHost()->setInstitutionType($unique[0]);
                            }

                            if ($element['info']['element'] == "HOST_COUNTRY") {

                                $unique = array_unique($comboBoxValues);
                                $offer->getHost()->setInstitutionCountry($unique[0]);
                            }

                            if ($element['info']['element'] == "HOST_CITY") {

                                $unique = array_unique($comboBoxValues);
                                $offer->getHost()->setInstitutionCity($unique[0]);
                            }
                            break;
                        }

                        if ($element['info']['type'] == "combo_box_numbers") {

                            $line = trim(fgets($file));
                            $split = explode(")", $line);

                            if (trim($split[1]) == "< CHOOSE NUMBER >")
                                break;

                            if ($element['info']['element'] == "PROPOSAL_DURATION") {

                                $offer->getProposal()->setProposalDuration(trim($split[1]));
                            }
                            break;
                        }
                    }
                }

                fseek($file, 0);
            }
        }

        fclose($file);

        return $offer;
    }

}
