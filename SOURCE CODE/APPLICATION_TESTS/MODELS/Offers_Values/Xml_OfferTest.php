<?php

require_once '../../../MODELS/Offers_Values/Xml_Offer.php';
require_once '../../../MODELS/Offer/Offer.php';
require_once '/../../CUSTOM_TESTS/CustomTests.php';

class Xml_OfferTest extends PHPUnit_Framework_TestCase {

    public function testGetOfferFieldsValues() {

        $CTests = new CustomTests();
        $xmlOffer = new Xml_Offer();

        $currentDir = getcwd() . "\\";

        // 'WRONG' XML FILE(With no proper information) AND A USELESS XML SCHEMA FILE TEST
        echo "testGetOfferFieldsValues(1)\n";
        $this->assertInstanceOf('Offer', $xmlOffer->getOfferFieldsValues($currentDir . "provider_wrong.xml", $currentDir . "whatever.xsd"));

        // ALL ATTRIBUTES OF OFFER OBJECT ARE NULL TEST
        echo "testGetOfferFieldsValues(2)\n";
        $CTests->assertAllAttributesNull($xmlOffer->getOfferFieldsValues($currentDir . "provider_wrong.xml", $currentDir . "whatever.xsd"));

        // 'WRONG' XML FILE(With no proper information) TEST
        echo "testGetOfferFieldsValues(3)\n";
        $this->assertInstanceOf('Offer', $xmlOffer->getOfferFieldsValues($currentDir . "provider_wrong.xml"));

        // ALL ATTRIBUTES OF OFFER OBJECT ARE NULL TEST
        echo "testGetOfferFieldsValues(4)\n";
        $CTests->assertAllAttributesNull($xmlOffer->getOfferFieldsValues($currentDir . "provider_wrong.xml"));

        // 'RIGHT' XML FILE WITH ALL ELEMENTS FILLED AND A USELESS XML SCHEMA FILE TEST
        echo "testGetOfferFieldsValues(5)\n";
        $this->assertInstanceOf('Offer', $xmlOffer->getOfferFieldsValues($currentDir . "provider.xml", $currentDir . "whatever.xsd"));

        // ALL ATTRIBUTES OF OFFER OBJECT ARE NOT NULL TEST
        echo "testGetOfferFieldsValues(6)\n";
        $CTests->assertAllAttributesFilled($xmlOffer->getOfferFieldsValues($currentDir . "provider.xml", $currentDir . "whatever.xsd"));

        // 'RIGHT' XML FILE WITH ALL ELEMENTS FILLED TEST
        echo "testGetOfferFieldsValues(7)\n";
        $this->assertInstanceOf('Offer', $xmlOffer->getOfferFieldsValues($currentDir . "provider.xml"));

        // ALL ATTRIBUTES OF OFFER OBJECT ARE NOT NULL TEST
        echo "testGetOfferFieldsValues(8)\n";
        $CTests->assertAllAttributesFilled($xmlOffer->getOfferFieldsValues($currentDir . "provider.xml"));

        echo "testGetOfferFieldsValues(OK!)\n\n";
    }

}

?>
