<?php

/**
 * CLASS THAT MODELS AN INSTITUTION OR HOST OF AN OFFER
 */
class Institution {
    
    // INSTITUTION ATTRIBUTES
    private $institution_name = null;
    private $institution_description = null;
    private $institution_type = null;
    private $institution_contact_fst_name = null;
    private $institution_contact_lst_name = null;
    private $institution_country = null;
    private $institution_city = null;
    private $institution_phone = null;
    private $institution_email = null;
    private $institution_website = null;
    
    // INSTITUTION METHODS
    function setInstitutionName($name) {

        $this->institution_name = $name;
    }

    function setInstitutionDescription($description) {

        $this->institution_description = $description;
    }

    function setInstitutionType($type) {

        $this->institution_type = $type;
    }

    function setInstitutionContactFstName($fstName) {

        $this->institution_contact_fst_name = $fstName;
    }

    function setInstitutionContactLstName($lstName) {

        $this->institution_contact_lst_name = $lstName;
    }

    function setInstitutionCountry($country) {

        $this->institution_country = $country;
    }

    function setInstitutionCity($city) {

        $this->institution_city = $city;
    }

    function setInstitutionPhone($phone) {

        $this->institution_phone = $phone;
    }

    function setInstitutionEmail($email) {

        $this->institution_email = $email;
    }

    function setInstitutionWebsite($website) {

        $this->institution_website = $website;
    }

    function getInstitutionName() {

        return $this->institution_name;
    }

    function getInstitutionDescription() {

        return $this->institution_description;
    }

    function getInstitutionType() {

        return $this->institution_type;
    }

    function getInstitutionContactFstName() {

        return $this->institution_contact_fst_name;
    }

    function getInstitutionContactLstName() {

        return $this->institution_contact_lst_name;
    }

    function getInstitutionCountry() {

        return $this->institution_country;
    }

    function getInstitutionCity() {

        return $this->institution_city;
    }

    function getInstitutionPhone() {

        return $this->institution_phone;
    }

    function getInstitutionEmail() {

        return $this->institution_email;
    }

    function getInstitutionWebsite() {

        return $this->institution_website;
    }
}
