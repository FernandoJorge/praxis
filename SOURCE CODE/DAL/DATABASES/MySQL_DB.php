<?php

/**
 * IMPORTS
 */
require_once 'DataBaseAccess.php';
require_once '/../../LOGS/Logs.php';

/**
 * CLASS TO HANDLE MYSQL TRANSACTIONS
 */
class MySQL_DB implements DataBaseAccess {

    private $db_connection = null;
    private $db_database = null;

    /**
     * 
     * CONSTRUCTOR TO OPEN A CONNECTION TO MYSQL DATABASE
     * 
     * @param String $db_server - SGDB server
     * @param String $db_database - Database´s name
     * @param String $db_username - Database´s username
     * @param String $db_password - Database´s password
     */
    function __construct($db_server, $db_database, $db_username, $db_password) {

        // OPEN CONNECTION TO DATABASE
        $this->openDBConnection($db_server, $db_database, $db_username, $db_password);
    }

    /**
     * 
     * ESTABLISHES A CONNECTION TO THE MYSQL DATABASE
     * 
     * @return boolean - true - success/ false - unsuccess
     */
    private function openDBConnection($db_server, $db_database, $db_username, $db_password) {

        /**
         * OPEN CONNECTION TO DATABASE
         */
        $this->db_connection = mysqli_connect($db_server, $db_username, $db_password, $db_database);

        if (!$this->db_connection) {

            Logs::writeLog("Couldn´t connect to " . $db_database . " database: " . mysqli_error($this->db_connection), true);
            return false;
        }

        $this->db_database = $db_database;

        // SETTINGS TO ALLOW SPECIAL CHARACTERS
        mysqli_query($this->db_connection, "SET character_set_results=utf8");
        mb_language('uni');
        mb_internal_encoding('UTF-8');
        mysqli_select_db($this->db_connection, $db_database);
        mysqli_query($this->db_connection, "set names 'utf8'");
        mysqli_query($this->db_connection, "SET character_set_client=utf8");
        mysqli_query($this->db_connection, "SET character_set_connection=utf8");
        mysqli_query($this->db_connection, "SET character_set_results=utf8");

        /**
         * TURNS OFF AUTO-COMMIT 
         */
        mysqli_autocommit($this->db_connection, false);

        return true;
    }

    /**
     * 
     * CLOSES CONNECTION TO THE MYSQL DATABASE
     * 
     * @return boolean - true - success/ false - unsuccess
     */
    function closeDBConnection() {

        $result = mysqli_close($this->db_connection);

        if (!$result) {

            Logs::writeLog("Couldn´t close connect to " . $this->db_database . " database: " . mysqli_error($this->db_connection), true);
            return false;
        }

        return true;
    }

    /**
     * 
     * PERFORMS A SELECT TO THE MYSQL DATABASE
     * 
     * @param String $SQL - Select statement
     * @return Array - Array with all the tuples/ false - unsuccess
     */
    function select($SQL) {

        $resultSet = mysqli_query($this->db_connection, $SQL);

        if (!$resultSet) {

            Logs::writeLog("Couldn´t perform select query to the " . $this->db_database . " database: " . mysqli_error($this->db_connection), true);
            return false;
        }

        $rows = mysqli_fetch_all($resultSet, MYSQLI_ASSOC);
        mysqli_free_result($resultSet);

        return $rows;
    }

    /**
     * 
     * PERFORMS A INSERT TO THE MYSQL DATABASE
     * 
     * @param String $SQL - Insert statement
     * @param boolean $idReturn - Set true if new record´s id is required / Set false if new record´s id is not required 
     * @return boolean - Id of new record if success and id required/ True if success id no id required / False if failed
     */
    function insert($SQL, $idReturn = true) {

        $result = mysqli_query($this->db_connection, $SQL);

        if (!$result) {

            Logs::writeLog("Couldn´t perform insert to the " . $this->db_database . " database: " . mysqli_error($this->db_connection), true);
            return false;
        } else {

            if ($idReturn) {
                return mysqli_insert_id($this->db_connection);
            } else {

                return true;
            }
        }
    }

    /**
     * 
     * PERFORMS A DELETE TO THE MYSQL DATABASE
     * 
     * @param String $SQL - Delete statement
     * @return boolean - true - success/ false - unsuccess
     */
    function delete($SQL) {

        $result = mysqli_query($this->db_connection, $SQL);

        if (!$result) {

            Logs::writeLog("Couldn´t perform delete to the " . $this->db_database . " database: " . mysqli_error($this->db_connection), true);
            return false;
        }

        return true;
    }

    /**
     * 
     * @param String $SQL - Update statement
     * @return boolean - true - success/ false - unsuccess
     */
    function update($SQL) {

        $result = mysqli_query($this->db_connection, $SQL);

        if (!$result) {

            Logs::writeLog("Couldn´t perform update to the " . $this->db_database . " database: " . mysqli_error($this->db_connection), true);
            return false;
        }

        return true;
    }

    /**
     * 
     * COMMIT CHANGES MADE TO DATABASE
     * 
     * @return boolean - true - success/ false - unsuccess
     */
    function commitDBChanges() {

        $result = mysqli_commit($this->db_connection);

        if (!$result) {

            Logs::writeLog("Couldn´t commit changes to " . $this->db_database . " database: " . mysqli_error($this->db_connection), true);
            return false;
        }

        return true;
    }

    /**
     * 
     * ROLLBACK CHANGES MADE TO DATABASE
     * 
     * @return boolean - true - success/ false - unsuccess
     */
    function rollbackDBChanges() {

        $result = mysqli_rollback($this->db_connection);

        if (!$result) {

            Logs::writeLog("Couldn´t rollback changes made to " . $this->db_database . " database: " . mysqli_error($this->db_connection), true);
            return false;
        }

        return true;
    }

}
