<?php

require_once 'Proposal.php';
require_once 'Institution.php';

/**
 * CLASS THAT MODELS AN OFFER
 */
class Offer {

    // ATTRIBUTES
    private $proposal = null;
    private $institution = null;
    private $host = null;

    /**
     * CONSTRUCTOR
     */
    function __construct() {

        $this->proposal = new Proposal();
        $this->institution = new Institution();
        $this->host = new Institution();
    }

    // METHODS
    function getProposal(){
        
        return $this->proposal;
    }
    
    function getInstitution(){
        
        return $this->institution;
    }
    
    function getHost(){
        
        return $this->host;
    }
}

?>