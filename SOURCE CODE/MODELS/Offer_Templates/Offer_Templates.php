<?php

/**
 * IMPORTS
 */
require_once '/../../LIBRARIES/TCPDF/tcpdf.php';
require_once '/../../MODELS/XMLSchemaNodes/SchemaNodes.php';

/**
 * CLASS OF SERVICE TO GENERATE OFFER TEMPLATES
 */
class Offer_Templates {

    static private $dataTypes = array(
        'string',
        'normalizedString',
        'token',
        'ENTITIES',
        'ENTITY',
        'ID',
        'IDREF',
        'IDREFS',
        'language',
        'Name',
        'NCName',
        'NMTOKEN',
        'NMTOKENS',
        'QName',
        'byte',
        'unsignedByte',
        'base64Binary',
        'hexBinary',
        'integer',
        'positiveInteger',
        'negativeInteger',
        'nonNegativeInteger',
        'nonPositiveInteger',
        'int',
        'unsignedInt',
        'long',
        'unsignedLong',
        'short',
        'unsignedShort',
        'decimal',
        'date',
        'dateTime',
        'duration',
        'gDay',
        'gMonth',
        'gMonthDay',
        'gYear',
        'gYearMonth',
        'time',
        'anyURI',
        'boolean',
        'double',
        'float',
        'NOTATION'
    );

    /**
     * 
     * FRONT-END TO GENERATE A PDF FILE TEMPLATE USING A XML SCHEMA FILE
     * 
     * @param String $schemaDocPath - Path to the Xml schema file
     * @param Reference $mySQLConnection - Database connection
     * @return String | boolean - PDF content | False if it fails
     */
    static function generatePDF($schemaDocPath, $mySQLConnection) {

        // GETS ALL OFFER'S NODES
        $offerNodes = SchemaNodes::getOfferNodes();

        // GENERATES XML TEMPLATE
        $xmlTemplateContent = self::generateXML($schemaDocPath);

        if (!$xmlTemplateContent)
            return false;

        file_put_contents($_SERVER['DOCUMENT_ROOT'] . "/PRAXIS/TEMP/xml_template.xml", $xmlTemplateContent);

        $xml = simplexml_load_file($_SERVER['DOCUMENT_ROOT'] . "/PRAXIS/TEMP/xml_template.xml");
        $fieldElements = self::getXmlFieldNames($xml, $offerNodes);
        unlink($_SERVER['DOCUMENT_ROOT'] . "/PRAXIS/TEMP/xml_template.xml");

        // GENERATES THE PDF TEMPLATE
        $pdf = new TCPDF('P', 'cm', 'A4');
        $pdf->Open();
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        $pdf->AddPage();
        $pdf->SetMargins(2.5, 2.5, 2.5);
        $pdf->SetFont('times', 'B', 20, true);
        $pdf->Cell(0, 1, 'PRAXIS PROJECT / INTERNSHIP FORM', 0, 1, 'C');
        $pdf->Ln();

        $pdf->setFontSubsetting(false);
        $pdf->setFormDefaultProp(array('lineWidth' => 1, 'borderStyle' => 'solid', 'fillColor' => array(255, 255, 200), 'strokeColor' => array(255, 128, 128)));

        foreach ($fieldElements as $record) {

            $pageHeight = $pdf->getPageHeight();
            $topMarginPage = $pdf->getMargins()['top'];
            $bottomMarginPage = $pdf->getMargins()['bottom'];
            $yCursorPos = $pdf->GetY();

            echo "POS = " . $yCursorPos . "; HEIGHT = " . $pageHeight . "<br>";

            if ($record['info']['type'] == 'text') {


                if ($yCursorPos + 2 >= $pageHeight) {

                    $pdf->AddPage('P', 'A4', true);
                    $pdf->SetFont('times', 'B', 20, true);
                }

                $pdf->SetFont('times', 'B', 10, true);
                $pdf->Cell(35, 1, $record['fieldName']);
                $pdf->Ln();
                $pdf->SetFont('times', 'I', 8, true);
                $pdf->Cell(0, 1, $pdf->TextField($record['info']['element'], 16, 0.5, array('multiline' => false, 'lineWidth' => 1, 'borderStyle' => 'solid')));
                $pdf->ln(1);
            }

            if ($record['info']['type'] == 'text_area') {

                if ($yCursorPos + 2 >= $pageHeight) {

                    $pdf->AddPage('P', 'A4', true);
                    $pdf->SetFont('times', 'B', 20, true);
                }

                $pdf->SetFont('times', 'B', 10, true);
                $pdf->Cell(35, 1, $record['fieldName']);
                $pdf->Ln();
                $pdf->SetFont('times', 'I', 8, true);
                $pdf->Cell(0, 1, $pdf->TextField($record['info']['element'], 16, 1, array('multiline' => true, 'lineWidth' => 1, 'borderStyle' => 'solid')));
                $pdf->ln(1);
            }

            if ($record['info']['type'] == 'combo_box_numbers') {

                if ($yCursorPos + 2 >= $pageHeight) {

                    $pdf->AddPage('P', 'A4', true);
                    $pdf->SetFont('times', 'B', 20, true);
                }

                $pdf->SetFont('times', 'B', 10, true);
                $pdf->Cell(35, 1, $record['fieldName']);
                $pdf->Ln();
                $values = array('< CHOOSE NUMBER >');

                for ($i = 1; $i < 37; $i++)
                    array_push($values, $i);

                $pdf->SetFont('times', 'B', 8, true);
                $pdf->Cell(0, 1, $pdf->ComboBox($record['info']['element'], 6, 0.4, $values));
                $pdf->ln(1);
            }

            if ($record['info']['type'] == 'combo_box_sql') {

                if ($yCursorPos + 2 + $record['info']['show_items'] >= $pageHeight) {

                    $pdf->AddPage('P', 'A4', true);
                    $pdf->SetFont('times', 'B', 20, true);
                }

                $info = $mySQLConnection->select($record['info']['sql']);
                $pdf->SetFont('times', 'B', 10, true);
                $pdf->Cell(35, 1, $record['fieldName']);
                $pdf->Ln();
                $values = array('< CHOOSE ITEM >');

                if (isset($info) && count($info) > 0) {

                    foreach ($info as $value) {

                        array_push($values, trim($value[$record['info']['field']]));
                    }
                }

                $stop;

                $pdf->SetFont('times', 'B', 8, true);

                if (isset($record['info']['show_items'])) {

                    if ($record['info']['show_items'] <= (count($values) - 1)) {

                        $stop = $record['info']['show_items'];
                    } else {
                        $stop = (count($values) - 1);
                    }

                    for ($i = 0; $i < $stop; $i++) {

                        $pdf->Cell(0, 1, $pdf->ComboBox($record['info']['element'] . "_" . (string) $i, 16, 0.4, $values));
                        $pdf->Ln(0.5);
                    }

                    $pdf->Ln(0.5);
                } else {

                    $pdf->Cell(0, 1, $pdf->ComboBox($record['info']['element'], 16, 0.4, $values));
                    $pdf->ln(1);
                }
            }
        }

        return $pdf->Output("offer_template", 'S');
    }

    /**
     *
     * FRONT-END TO GENERATE A XML FILE TEMPLATE CONTENT USING A XML SCHEMA FILE
     *  
     * @param String $schemaDocPath - Path to the Xml schema file
     * @return String | boolean - Xml content | False if it fails
     */
    static function generateXML($schemaDocPath) {

        // FORMATS XSD
        $schema = self::formatXSD($schemaDocPath);

        if ($schema == "fileReadError" || $schema == "fileWriteError")
            return false;

        // GENERATE OFFER XML TEMPLATE'S NODES
        $elements = self::getXML($schema);

        if (!$elements || empty($elements))
            return false;

        $offerTemplate = "<?xml version='1.0' encoding='UTF-8'?>\n" . $elements;

        return $offerTemplate;
    }

    /**
     * 
     * FORMATS A XML SCHEMA CONTENT TO FACILITATE THE GENERATION OF A XML TEMPLATE
     * 
     * @param String $schemaDocPath - Path to the Xml schema file
     * @return String - Xml schema content formated | 
     * 'fileReadError' if it can´t read content from XML Schema file path | 
     * 'fileWriteError' if it can´t write content of XML Schema file
     */
    private static function formatXSD($schemaDocPath) {

        $complexTypeLocalTags = array();
        $complexTypeGlobalTags = array();
        $allLines = array();

        $file = @file_get_contents($schemaDocPath);

        if (!$file || empty($file)) {
            return 'fileReadError';
        }

        $file = str_replace("elementFormDefault", "", $file);
        $file = str_replace("><", ">\n<", $file);
        $file = str_replace("\"", "'", $file);

        $temp = tmpfile();

        if (!fwrite($temp, $file))
            return 'fileWriteError';

        fseek($temp, 0);

        $save = true;

        while (!feof($temp)) {

            $line = trim(fgets($temp));

            // START COMPLEXTYPE LOCAL TAGS
            if (stristr($line, "<") && stristr($line, "element") && stristr($line, "name=") && !stristr($line, "type=") && !stristr($line, "/>")) {

                $namePos = stripos($line, "name=");
                $newLine = substr($line, $namePos + 6);
                $name = explode("'", $newLine)[0];

                array_push($complexTypeLocalTags, $name);
                array_push($allLines, $line);
                $save = false;
            }

            // END - COMPLEXTYPE LOCAL TAGS
            if (stristr($line, "</") && stristr($line, "element")) {

                $lastTag = array_pop($complexTypeLocalTags);
                array_push($allLines, "<" . $lastTag . ">");
                $save = false;
            }

            // START COMPLEXTYPE GLOBAL TAGS
            if (stristr($line, "complextype") && stristr($line, "name=")) {

                $namePos = stripos($line, "name=");
                $newLine = substr($line, $namePos + 6);
                $name = explode("'", $newLine)[0];

                array_push($complexTypeGlobalTags, $name);
                array_push($allLines, $line);
                $save = false;
            }

            // END COMPLEXTYPE GLOBAL TAGS
            if (stristr($line, "</") && stristr($line, "complextype")) {

                if (count($complexTypeGlobalTags) > count($complexTypeLocalTags)) {

                    $lastTag = array_pop($complexTypeGlobalTags);
                    array_push($allLines, "<" . $lastTag . ">");
                    $save = false;
                }
            }

            if ($save) {

                array_push($allLines, $line);
            } else {
                $save = true;
            }
        }

        fclose($temp);

        // DELETES ALL LINES WITH ATTRIBUTE DECLARATIONS AND COMMENTS
        for ($i = 0; $i < count($allLines); $i++) {

            if (stristr($allLines[$i], "<") && stristr($allLines[$i], "attribute ") && stristr($allLines[$i], "/>") || stristr($allLines[$i], "<!--") && stristr($allLines[$i], "-->")) {

                unset($allLines[$i]);
            }
        }

        $file = "";

        foreach ($allLines as $line) {

            $file .= $line . "\n";
        }

        return $file;
    }

    /**
     * 
     * GENERATES A XML FILE TEMPLATE CONTENT USING A XML SCHEMA FILE
     * 
     * @param String $schemaContent - Content of the Xml schema
     * @return String - XML TEMPLATE CONTENT
     */
    private static function getXML($schemaContent) {

        $tags = array('simpletype', 'attributeGroup');
        $allElements = "";

        $schemaActualPointer = tmpfile();
        if (!fwrite($schemaActualPointer, $schemaContent))
            return false;

        fseek($schemaActualPointer, 0);

        while (!feof($schemaActualPointer)) {

            $declaration = fgets($schemaActualPointer);
            $declaration = trim($declaration);

            // IF IT´S A 'SIMPLTYPE' OR 'ATTRIBUTEGROUP' ELEMENT MOVES POINTER FORWARD
            foreach ($tags as $tag) {

                if (stristr($declaration, $tag) && stristr($declaration, "name=")) {

                    $stopTag = null;

                    // GETS STOP TAG
                    foreach ($tags as $stop) {

                        if (stristr($declaration, $stop)) {

                            $stopTag = $stop;
                            break;
                        }
                    }

                    while (!feof($schemaActualPointer)) {

                        $declaration = trim(fgets($schemaActualPointer));

                        if (stristr($declaration, "</") && stristr($declaration, $stopTag)) {
                            break;
                        }
                    }

                    // MOVE FILE´S POINTER TO THE NEXT LINE
                    $declaration = trim(fgets($schemaActualPointer));
                }
            }

            // IF IT´S A GLOBAL TYPE DECLARATION MOVE THE FILE´S POINTER FORWARD
            if (stristr($declaration, "complextype") && stristr($declaration, "name=")) {

                $elementName = self::getName($declaration);

                while (!feof($schemaActualPointer)) {

                    $declaration = trim(fgets($schemaActualPointer));

                    if (stristr($declaration, "<" . $elementName . ">")) {
                        break;
                    }
                }

                // MOVE FILE´S POINTER TO THE NEXT LINE
                $declaration = trim(fgets($schemaActualPointer));
            }

            // SIMPLE ELEMENT DECLARATION WITH DEFINITION TYPE
            if (stripos($declaration, "<") == 0 && stristr($declaration, "/>") && stristr($declaration, "element") && stristr($declaration, "type=") && !stristr($declaration, "ref=")) {

                $simple = self::getSimpleElement($declaration, $schemaContent);

                if (!$simple) {

                    fclose($schemaActualPointer);
                    return false;
                }

                $allElements .= $simple;
            }

            // SIMPLE ELEMENT DECLARATION WITHOUT DEFINITION TYPE
            if (stripos($declaration, "<") == 0 && stristr($declaration, "/>") && stristr($declaration, "element") && !stristr($declaration, "type=") && !stristr($declaration, "ref=")) {

                $simple = self::getSimpleElement($declaration, $schemaContent);

                if (!$simple) {

                    fclose($schemaActualPointer);
                    return false;
                }

                $allElements .= $simple;
            }

            // SIMPLE ELEMENT REFERENCE DECLARATION 
            if (stripos($declaration, "<") == 0 && stristr($declaration, "/>") && stristr($declaration, "element") && stristr($declaration, "ref=")) {
                $allElements .= self::getReferenceElement($declaration, $schemaContent);
            }

            // COMPLEX LOCAL ELEMENT DECLARATION
            if (stripos($declaration, "<") == 0 && !stristr($declaration, "/>") && !stristr($declaration, "type=") && stristr($declaration, "element") && !stristr($declaration, "ref=")) {

                $elementName = self::getName($declaration);
                $allElements .= self::getComplexLocalElement($declaration, $schemaContent);


                // MOVES FILE´S POINTER TO THE END OF THE COMPLEX
                while (!feof($schemaActualPointer)) {

                    $declaration = trim(fgets($schemaActualPointer));

                    if (stristr($declaration, "<" . $elementName . ">")) {
                        break;
                    }
                }
            }
        }

        fclose($schemaActualPointer);
        $allElements = str_replace("<>", "", $allElements);
        return $allElements;
    }

    /**
     * 
     * GETS XML´S DECLARATION FOR A SIMPLE OR LOCAL TYPE ELEMENT
     * 
     * @param String $declaration - Element declaration
     * @param String $schemaContent - Xml Schema content
     * @return String - Xml´s declaration of a simple or local element
     */
    private static function getSimpleElement($declaration, $schemaContent) {

        $elementName = self::getName($declaration);
        $othersElements = self::getElementType($declaration, $schemaContent);

        // IF IS A DEFAULT TYPE DECLARATION ELEMENT
        if ($othersElements == "default_type") {

            return "\n<" . $elementName . ">\n\n</" . $elementName . ">\n";
        } else {

            return "\n<" . $elementName . ">\n" . $othersElements . "\n</" . $elementName . ">\n";
        }
    }

    /**
     * 
     * GETS XML'S DECLARATION FOR A GLOBAL TYPE ELEMENT
     * 
     * @param String $declaration - Element declaration
     * @param String $schemaContent - Xml Schema content
     * @return String - xml´s declaration of a global element
     */
    private static function getComplexLocalElement($declaration, $schemaContent) {
        $elementName = self::getName($declaration);
        return "<" . $elementName . ">" . self::getComplexLocalTypeElements($declaration, $schemaContent) . "</" . $elementName . ">";
    }

    /**
     * 
     * GETS XML´S DECLARATION FOR A REFERENCED TYPE ELEMENT
     * 
     * @param String $declaration - Element declaration
     * @param String $schemaContent - Xml Schema content
     * @return String - Xml´s declaration for a referenced type element
     */
    private static function getReferenceElement($declaration, $schemaContent) {

        $reference;
        $refPos = stripos($declaration, "ref=");
        $declaration = substr($declaration, $refPos + 5);

        // VERIFIES IF HAS NAMESPACE
        $refTest = explode(":", $declaration);

        if (count($refTest) > 1) {

            $ref1 = explode("'", $refTest[1]);

            if (count($ref1) > 1) {

                $reference = $ref1[0];
            }
        } else {

            $type = explode("'", $declaration);

            if (count($type) > 1) {

                $reference = $type[0];
            }
        }

        $schema = tmpfile();
        if (!fwrite($schema, $schemaContent))
            return false;

        fseek($schema, 0);

        while (!feof($schema)) {

            $line = trim(fgets($schema));

            if (stristr($line, "name='" . $reference . "'")) {

                // SIMPLE ELEMENT DECLARATION WITH DEFINITION TYPE
                if (stripos($line, "<") == 0 && stristr($line, "/>") && stristr($line, "element") && stristr($line, "type=") && !stristr($line, "ref=")) {

                    $simple = self::getSimpleElement($line, $schemaContent);

                    if (!$simple) {

                        fclose($schema);
                        return false;
                    }

                    fclose($schema);
                    return $simple;
                }

                // SIMPLE ELEMENT DECLARATION WITHOUT DEFINITON TYPE
                if (stripos($line, "<") == 0 && stristr($line, "/>") && stristr($line, "element") && !stristr($line, "type=") && !stristr($line, "ref=")) {

                    $simple = self::getSimpleElement($line, $schemaContent);

                    if (!$simple) {

                        fclose($schema);
                        return false;
                    }

                    fclose($schema);
                    return $simple;
                }

                // COMPLEX LOCAL ELEMENT DECLARATION
                if (stripos($line, "<") == 0 && !stristr($line, "/>") && !stristr($line, "type=") && stristr($line, "element ") && !stristr($line, "ref=")) {
                    fclose($schema);
                    return self::getComplexLocalElement($line, $schemaContent);
                }
            }
        }

        fclose($schema);
    }

    /**
     * 
     * GETS THE NAME OF AN ELEMENT
     * 
     * @param String $declaration - Element declaration
     * @return String - Name of the element
     */
    private static function getName($declaration) {

        $namePos = stripos($declaration, "name=");
        $declaration = substr($declaration, $namePos + 6);
        $name = explode("'", $declaration);

        if (count($name) > 1) {

            return $name[0];
        }
    }

    /**
     * 
     * GETS THE TYPE OF AN ELEMENT
     * 
     * @param String $declaration - Element declaration
     * @return String - Name of the type´s element
     */
    private static function getType($declaration) {

        $typePos = stripos($declaration, "type=");

        if (!$typePos) {
            return false;
        }

        $declaration = substr($declaration, $typePos + 6);

        // VERIFIES IF HAS NAMESPACE
        if (strstr($declaration, ":")) {

            $split1 = explode(":", $declaration);
            $split2 = explode("'", $split1[1]);
            $type = $split2[0];
        } else {

            $split = explode("'", $declaration);
            $type = $split[0];
        }

        return $type;
    }

    /**
     * 
     * TESTS IF IS A SIMPLE OR LOCAL ELEMENT AND RETURNS ALL THE XML ELEMENTS' DECLARATIONS IF IS A GLOBAL ELEMENT
     * 
     * @param String $declaration - Element declaration
     * @param String $schemaContent - Xml Schema content
     * @return string - 'default_type' if is a simple element; The rest of the xml elements' declarations if is a global element
     */
    private static function getElementType($declaration, $schemaContent) {

        $type = self::getType($declaration);

        if (!$type) {
            return "default_type";
        }

        // TESTS IF IT'S A DEFAULT TYPE
        if (in_array($type, self::$dataTypes)) {
            return "default_type";
        } else {

            $elements = "";
            $read = false;

            $schema = tmpfile();

            if (!fwrite($schema, $schemaContent))
                return false;

            fseek($schema, 0);

            while (!feof($schema)) {

                $declaration = trim(fgets($schema));

                if (strstr($declaration, "name='" . $type . "'") || $read) {

                    // MOVES FILE´S POINTER TO THE NEXT LINE
                    if (strstr($declaration, "name='" . $type . "'")) {
                        $declaration = trim(fgets($schema));
                    }

                    $read = true;

                    // IF IT`S A SIMPLETYPE MOVE FILE'S POINTER FORWARD
                    if (!stristr($declaration, "</") && stristr($declaration, "simpletype")) {

                        while (!feof($schema)) {

                            $declaration = trim(fgets($schema));

                            if (stristr($declaration, "</") && stristr($declaration, "simpletype")) {
                                break;
                            }
                        }
                    }

                    // STOPS WHEN MATCHS THE END ELEMENT TAG
                    if (strstr($declaration, "<" . $type . ">")) {
                        fclose($schema);
                        return $elements;
                    }

                    // SIMPLE ELEMENT DECLARATION WITH DEFINITION TYPE
                    if (stripos($declaration, "<") == 0 && stristr($declaration, "/>") && stristr($declaration, "element") && stristr($declaration, "type=") && !stristr($declaration, "ref=")) {
                        $elements .= self::getSimpleElement($declaration, $schemaContent);
                    }

                    // SIMPLE ELEMENT DECLARATION WITHOUT DEFINITION TYPE
                    if (stripos($declaration, "<") == 0 && stristr($declaration, "/>") && stristr($declaration, "element") && !stristr($declaration, "type=") && !stristr($declaration, "ref=")) {

                        $elements .= self::getSimpleElement($declaration, $schemaContent);
                    }

                    // SIMPLE ELEMENT REFERENCE DECLARATION 
                    if (stripos($declaration, "<") == 0 && stristr($declaration, "/>") && stristr($declaration, "element") && stristr($declaration, "ref=")) {
                        $elements .= self::getReferenceElement($declaration, $schemaContent);
                    }

                    // COMPLEX LOCAL ELEMENT DECLARATION
                    if (stripos($declaration, "<") == 0 && !stristr($declaration, "/>") && !stristr($declaration, "type=") && stristr($declaration, "element") && !stristr($declaration, "ref=")) {
                        $elementName = self::getName($declaration);
                        $elements .= self::getComplexLocalElement($declaration, $schemaContent);

                        // MOVES FILE´S POINTER TO THE END OF THE COMPLEX
                        while (!feof($schema)) {

                            $declaration = trim(fgets($schema));

                            if (stristr($declaration, "<" . $elementName . ">")) {
                                break;
                            }
                        }
                    }
                }
            }

            fclose($schema);
            return $elements;
        }
    }

    /**
     * 
     * GETS ALL THE XML ELEMENTS' DECLARATION OF A GLOBAL TYPE ELEMENT
     * 
     * @param String $declaration - Element declaration
     * @param String $schemaContent - Xml schema content
     * @return String - Xml elements' declarations of the global element
     */
    private static function getComplexLocalTypeElements($declaration, $schemaContent) {

        $complexName = self::getName($declaration);
        $elements = "";
        $read = false;

        $schema = tmpfile();

        if (!fwrite($schema, $schemaContent))
            return false;

        fseek($schema, 0);

        while (!feof($schema)) {

            $line = trim(fgets($schema));

            if ($line == $declaration || $read) {

                $read = true;

                // MOVE FILE POINTER TO THE NEXT LINE
                if ($line == $declaration) {
                    $line = trim(fgets($schema));
                }

                // IF IT'S A SIMPLETYPE MOVE FILE'S POINTER FORWARD
                if (!stristr($line, "</") && stristr($line, "simpletype")) {

                    while (!feof($schema)) {

                        $line = trim(fgets($schema));

                        if (stristr($line, "</") && stristr($line, "simpletype")) {
                            break;
                        }
                    }
                }

                // STOPS WHEN MATCHS THE END ELEMENT TAG
                if (strstr($line, "<" . $complexName . ">")) {
                    fclose($schema);
                    return $elements;
                }

                // SIMPLE ELEMENT DECLARATION WITH DEFINITION TYPE
                if (stripos($line, "<") == 0 && stristr($line, "/>") && stristr($line, "element") && stristr($line, "type=") && !stristr($line, "ref=")) {
                    $elements .= self::getSimpleElement($line, $schemaContent);
                }

                // SIMPLE ELEMENT DECLARATION WITHOUT DEFINITON TYPE
                if (stripos($line, "<") == 0 && stristr($line, "/>") && stristr($line, "element ") && !stristr($line, "type=") && !stristr($line, "ref=") && !stristr($line, "ref=")) {

                    $elements .= self::getSimpleElement($line, $schemaContent);
                }

                // SIMPLE ELEMENT REFERENCE DECLARATION 
                if (stripos($line, "<") == 0 && stristr($line, "/>") && stristr($line, "element") && stristr($line, "ref=")) {
                    $elements .= self::getReferenceElement($line, $schemaContent);
                }

                // COMPLEX LOCAL ELEMENT DECLARATION
                if (stripos($line, "<") == 0 && !stristr($line, "/>") && !stristr($line, "type=") && stristr($line, "element") && !stristr($line, "ref=")) {
                    $elementName = self::getName($line);
                    $elements .= self::getComplexLocalElement($line, $schemaContent);

                    // MOVES FILE´S POINTER TO THE END OF THE COMPLEX
                    while (!feof($schema)) {

                        $line = trim(fgets($schema));

                        if (stristr($line, "<" . $elementName . ">")) {
                            break;
                        }
                    }
                }
            }
        }

        fclose($schema);
        return $elements;
    }

    /**
     * 
     * GETS ALL THE NAMES OF THE FIELDS TO SHOW AT PDF TEMPLATE
     * 
     * @param Reference $xmlRoot - Reference to a xml node
     * @param Array $checkNames - Array with information about xsd mapping fields names <=> xsd elements
     * @return Array - Names of the fields names to show on pdf template
     */
    static function getXmlFieldNames($xmlRoot, $checkNames) {

        $xmlFieldsNames = array();

        $tagName = trim($xmlRoot->getName());

        // TEST IF IS A VALID OFFER TAG NAME
        foreach ($checkNames as $fieldName => $value) {

            if ($tagName == $value['element']) {

                array_push($xmlFieldsNames, array('fieldName' => $fieldName, 'info' => $value));
                break;
            }
        }

        // GETS THE OTHERS CHILDS AND TEST
        $children = $xmlRoot->children();

        foreach ($children as $child) {

            $fieldsNames = self::getXmlFieldNames($child, $checkNames);
            $xmlFieldsNames = array_merge($xmlFieldsNames, $fieldsNames);
        }

        return $xmlFieldsNames;
    }

}

?>
