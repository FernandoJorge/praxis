<?php

/**
 * CLASS OF SERVICES THAT ALLOWS TO GET THE NODE´S DECLARATION OF THE INSTITUTION´S DETAILS
 */
class Institution_XSchemaNodes {
    /** STATIC METHODS * */

    /**
     * 
     * GETS ELEMENT NODE´S DECLARATION OF A INSTITUTION´S DETAIL
     * 
     * @param int $nodeID
     * @param int $type Intitution type - Institution or Host
     * @return String - Element node´s declaration
     */
    static function getDeclarationNode($nodeID, $type) {

        switch ($nodeID) {
            case 0:

                return self::getNameNode($type);
                break;

            case 1:

                return self::getDescriptionNode($type);
                break;

            case 2:

                return self::getTypeNode($type);
                break;

            case 3:

                return self::getContactFSTNameNode($type);
                break;

            case 4:

                return self::getContactLSTNameNode($type);
                break;

            case 5:

                return self::getCountryNode($type);
                break;

            case 6:

                return self::getCityNode($type);
                break;

            case 7:

                return self::getPhoneNode($type);
                break;

            case 8:

                return self::getEmailNode($type);
                break;

            case 9:

                return self::getWebSiteNode($type);
                break;
        }
    }

    /**
     * 
     * GETS INSTITUTTION NAME ELEMENT DECLARATION
     * 
     * @return String - Node element declaration that represents the name of the institution
     */
    static function getNameNode($type) {

        switch ($type) {

            case 0:
                return "<xs:element name='INSTITUTION_NAME' type='xs:string' maxOccurs='1' />";
                break;

            case 1:
                return "<xs:element name='HOST_NAME' type='xs:string' maxOccurs='1' />";
                break;
        }
    }

    /**
     * 
     * GETS INSTITUTTION DESCRIPTION ELEMENT DECLARATION
     * 
     * @return String - Node element declaration that represents the description of the institution
     */
    static function getDescriptionNode($type) {

        switch ($type) {

            case 0:
                return "<xs:element name='INSTITUTION_DESCRIPTION' type='xs:string' maxOccurs='1' />";
                break;

            case 1:
                return "<xs:element name='HOST_DESCRIPTION' type='xs:string' maxOccurs='1' />";
                break;
        }
    }

    /**
     * 
     * GETS INSTITUTTION TYPE ELEMENT DECLARATION
     * 
     * @return String - Node element declaration that represents the type of institution
     */
    static function getTypeNode($type) {

        switch ($type) {

            case 0:
                return "<xs:element name='INSTITUTION_TYPE' type='xs:string' maxOccurs='1' />";
                break;

            case 1;
                return "<xs:element name='HOST_TYPE' type='xs:string' maxOccurs='1' />";
                break;
        }
    }

    /**
     * 
     * GETS INSTITUTTION CONTACT FIRST NAME ELEMENT DECLARATION
     * 
     * @return String - Node element declaration that represents contact´s first name of the institution
     */
    static function getContactFSTNameNode($type) {

        switch ($type) {

            case 0:
                return "<xs:element name='INSTITUTION_CONTACTFSTNAME' type='xs:string' maxOccurs='1' />";
                break;

            case 1:
                return "<xs:element name='HOST_CONTACTFSTNAME' type='xs:string' maxOccurs='1' />";
                break;
        }
    }

    /**
     * 
     * GETS INSTITUTTION CONTACT LAST NAME ELEMENT DECLARATION
     * 
     * @return String - Node element declaration that represents contact´s last name of the institution
     */
    static function getContactLSTNameNode($type) {

        switch ($type) {

            case 0:
                return "<xs:element name='INSTITUTION_CONTACTLSTNAME' type='xs:string' maxOccurs='1' />";
                break;

            case 1:
                return "<xs:element name='HOST_CONTACTLSTNAME' type='xs:string' maxOccurs='1' />";
                break;
        }
    }

    /**
     * 
     * GETS INSTITUTTION COUNTRY ELEMENT DECLARATION
     * 
     * @return String - Node element declaration that represents the country of the institution
     */
    static function getCountryNode($type) {

        switch ($type) {

            case 0:
                return "<xs:element name='INSTITUTION_COUNTRY' type='xs:string' maxOccurs='1' />";
                break;

            case 1:
                return "<xs:element name='HOST_COUNTRY' type='xs:string' maxOccurs='1' />";
                break;
        }
    }

    /**
     * 
     * GETS INSTITUTTION CITY ELEMENT DECLARATION
     * 
     * @return String - Node element declaration that represents the city of the institution
     */
    static function getCityNode($type) {

        switch ($type) {

            case 0:
                return "<xs:element name='INSTITUTION_CITY' type='xs:string' maxOccurs='1' />";
                break;

            case 1:
                return "<xs:element name='HOST_CITY' type='xs:string' maxOccurs='1' />";
                break;
        }
    }

    /**
     * 
     * GETS INSTITUTTION PHONE CONTACT ELEMENT DECLARATION
     * 
     * @return String - Node element declaration that represents the phone contact of the institution
     */
    static function getPhoneNode($type) {

        switch ($type) {

            case 0:
                return "<xs:element name='INSTITUTION_PHONE' type='xs:string' maxOccurs='1' />";
                break;

            case 1:
                return "<xs:element name='HOST_PHONE' type='xs:string' maxOccurs='1' />";
                break;
        }
    }

    /**
     * 
     * GETS INSTITUTTION EMAIL CONTACT ELEMENT DECLARATION
     * 
     * @return String - Node element declaration that represents email of the institution
     */
    static function getEmailNode($type) {

        switch ($type) {

            case 0:
                return "<xs:element name='INSTITUTION_EMAIL' type='xs:string' maxOccurs='1' />";
                break;

            case 1:
                return "<xs:element name='HOST_EMAIL' type='xs:string' maxOccurs='1' />";
                break;
        }
    }

    /**
     * 
     * GETS INSTITUTTION WEBSITE ELEMENT DECLARATION
     * 
     * @return String - Node element declaration that represents the website url of the institution
     */
    static function getWebSiteNode($type) {

        switch ($type) {

            case 0:
                return "<xs:element name='INSTITUTION_WEBSITE' type='xs:anyURI' maxOccurs='1' />";
                break;

            case 1:
                return "<xs:element name='HOST_WEBSITE' type='xs:anyURI' maxOccurs='1' />";
                break;
        }
    }

}
