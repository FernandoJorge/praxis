<?php

/**
 * CLASS OF SERVICES TO REPORT EVENTS/ERRORS
 */
class Logs {

    /**
     * 
     * SAVES DESCRIPTION OF AN EVENT ON LOG´S FILE
     * 
     * @param String $log - Description of the event/error
     * @param boolean $error - Set true if is a log error / Set false if is a log event
     * @return boolean - True if success / False if fails 
     */
    static function writeLog($log, $error = false) {

        // OPEN LOGS´ FILE
        $logsFile;

        if (!$error) {

            $logsFile = fopen($_SERVER['DOCUMENT_ROOT'] . "/PRAXIS/FILES/LOGS/logs_events_praxis.txt", "a");
        } else {

            $logsFile = fopen($_SERVER['DOCUMENT_ROOT'] . "/PRAXIS/FILES/LOGS/logs_errors_praxis.txt", "a");
        }

        if (!$logsFile)
            return false;

        // SETS LOG EVENT/ERROR
        $eventLog;
        $timezone = 0;

        if (!$error) {

            $eventLog = "[ " . gmdate("Y-m-d H:i:s", time() + 3600 * ($timezone + date("I"))) . " ] [ MESSAGE EVENT: " . $log . " ];\n";
        } else {

            $eventLog = "[ " . gmdate("Y-m-d H:i:s", time() + 3600 * ($timezone + date("I"))) . " ] [ MESSAGE ERROR: " . $log . " ];\n";
        }

        // WRITE LOG TO LOGS´ FILE
        if (!fwrite($logsFile, $eventLog))
            return false;

        // CLOSE LOGS´ FILE
        fclose($logsFile);

        return true;
    }

}
