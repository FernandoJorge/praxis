<?php

/**
 * CLASS OF SERVICES TO VALIDATE XML/XSD DOCUMENTS
 */
class XML_XSDValidation {

    /**
     * 
     * VALIDATES THE SYNTAX OF XML/XSD DOCUMENT
     * 
     * @param String $docPath - Absoulte path to the file to validate
     * @return Array - Errors catched while parsing the document
     */
    static function checkDocSyntax($docPath) {

        $errors = array();

        $xml_parser = xml_parser_create();

        $file = fopen($docPath, 'r');

        while ($data = fread($file, 4096)) {

            if (!xml_parse($xml_parser, $data, feof($file))) {

                array_push($errors, "[ LINE => " . xml_get_current_line_number($xml_parser) . " ; COLUMN => " . xml_get_current_column_number($xml_parser) . ""
                        . " <br>MESSAGE => " . xml_error_string(xml_get_error_code($xml_parser)) . " ]<br><br>");
            }
        }

        xml_parser_free($xml_parser);

        fclose($file);

        return array_unique($errors);
    }

    /**
     * 
     * VALIDATES A XML DOCUMENT AGAINST A XML SCHEMA DOCUMENT
     * 
     * @param String $xmlDocPath - Absolute path to the xml document
     * @param String $xmlSchemaDocPath - Absolute path to the xml schema document
     * @return array - Errors catched while validating the document
     */
    static function checkXMLSchemaValidation($xmlDocPath, $xmlSchemaDocPath) {

        $errors = array();

        // ENABLE ERROR HANDLING
        libxml_use_internal_errors(true);

        $xml = new DOMDocument();
        $xml->load($xmlDocPath);

        if (!$xml->schemaValidate($xmlSchemaDocPath)) {

            $allErrors = libxml_get_errors();

            foreach ($allErrors as $error) {

                array_push($errors, "[ LINE => " . $error->line . " ; COLUMN => " . $error->column . " <br>MESSAGE => '" . $error->message . "' ]<br><br>");
            }

            libxml_clear_errors();
        }

        libxml_use_internal_errors(true);

        return array_unique($errors);
    }

}
