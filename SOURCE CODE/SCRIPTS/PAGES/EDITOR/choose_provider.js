/**
 * SCRIPT TO HANDLE EVENTS OF THE 'choose_provider.html' page
 */

var provider_id;

// DELETES PROVIDER´S XML SCHEMA REQUEST
function deleteSchema() {

    $("#delete-schema-dialog").modal('hide');

    $.ajax({
        url: 'editor?provider_scheme_delete=' + provider_id,
        type: 'GET',
        datatype: 'json',
        async: false,
        success: function (msg) {

            if (msg['result'] == "success") {

                $("#action" + provider_id).html("<A class='btn btn-success' href='editor?provider_scheme_edit=" + provider_id + "' role='button'><I>CREATE </I></A>");

                $("#modal-message-header").html("<span class='glyphicon glyphicon-info-sign'></span>&nbsp;Delete confirmation...");
                $("#modal-message-content").html("<p>The provider´s XML Schema was successfully deleted</p>");
                $("#errorMessage").modal({
                    zIndex: 9999
                });
                $("#errorMessage").modal('show');
            }

            if (msg['result'] == "failed" || msg['result'] == "deleteAccessError") {

                $("#modal-message-header").html("Delete confirmation&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>Occurred a problem while deleting provider´s XML Schema...</p>");
                $("#errorMessage").modal({
                    zIndex: 9999
                });
                $("#errorMessage").modal('show');
            }
        },
        error: function (xhr, status, error) {

            console.log(" xhr.responseText: " + xhr.responseText + " \n\nSTATUS:\n " + status + " \n\nERROR:\n " + error);

            $("#modal-message-header").html("Error request&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
            $("#modal-message-content").html("<p>It was not possible to perform the request to delete the XML Schema...</p>");
            $("#errorMessage").modal({
                zIndex: 9999
            });
            $("#errorMessage").modal('show');
        }
    });
}

// ASKS USER TO CONFIRM PROVIDER´S XML SCHEMA DELETION
function confirmDeleteSchema(providerID) {

    provider_id = providerID;

    $("#delete-schema-dialog").modal({
        zIndex: 9999
    });
}

function sizeTable() {

    var tableSize = (parseInt($("#footer").position().top) - parseInt($("#providers").position().top)) - 4 * parseInt($("#footer").height());

    $("#providers").DataTable({
        "scrollY": tableSize + "px",
        "scrollCollapse": true,
        "processing": true
    });


}

$(document).ready(function () {

    sizeTable();

});
