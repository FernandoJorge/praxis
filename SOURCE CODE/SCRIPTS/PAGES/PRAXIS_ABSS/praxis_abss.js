function authorize() {

    $("#projectAuthentication").ajaxSubmit({
        url: 'project',
        method: 'POST',
        datatype: 'json',
        success: function (msg) {

            if (msg['result'] == 'autheticationAccessError') {

                $("#username").val("");
                $("#password").val("");

                $("#modal-message-header").html("Authentication&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>Occurred an error while perform authorization...</p>");

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'failed') {

                $("#username").val("");
                $("#password").val("");

                $("#modal-message-header").html("Authentication&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>Authentication failed...</p>");

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'enabled') {

                $("#username").val("");
                $("#password").val("");

                $("#modal-message-header").html("Authentication&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>Your account is currently disabled. Contact Praxis network for <a href='http://www.praxisnetwork.eu/contact/'>Help</a></b></p>");
                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'locked') {

                $("#username").val("");
                $("#password").val("");

                $("#modal-message-header").html("Authentication&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>Your account is currently locked. Contact Praxis network for <a href='http://www.praxisnetwork.eu/contact/'>Help</a></b></p>");
                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'expired') {

                $("#username").val("");
                $("#password").val("");

                $("#modal-message-header").html("Authentication&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>Your account record has expired. <a href='http://www.praxisnetwork.eu/register/'>Register</a></b> or contact Praxis network for <a href='http://www.praxisnetwork.eu/contact/'>Help</a></b></p>");
                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'authorized') {

                document.location.href = "http://" + location.host + "/PRAXIS/SUBMIT_OFFER";
            }
        },
        error: function (xhr, status, error) {

            console.log(" xhr.responseText: " + xhr.responseText + " \n\nSTATUS:\n " + status + " \n\nERROR:\n " + error);

            $("#modal-message-header").html("Request error&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
            $("#modal-message-content").html("<p>It was not possible to make the request...</p>");

            $("#message").modal({
                zIndex: 9999
            });
            $("#message").modal('show');
        }
    });
}