<?php

/**
 * IMPORTS
 */
require_once 'MySQL_DB.php';

/**
 * FACTORY TO CREATE A DATABASE CONNECTION OBJECT TO HANDLE DATABASE ACCESS(MYSQL, SQL SEVER, ORACLE, etc)
 */
class FactoryDataBaseAccess {

    /**
     * 
     * OBJECT OF DATABASE CONNECTION
     * 
     * @param String $SGDB - SGDB server
     * @param String $db_database - Database´s name
     * @param String $db_username - Database´s username
     * @param String $db_password - Database´s password
     * @return \MySQL_DB - Database connection object
     */
    static function getSGDBConnection($SGDB, $db_server, $db_database, $db_username, $db_password) {

        if ($SGDB == "mysql") {

            $sgdb = new MySQL_DB($db_server, $db_database, $db_username, $db_password);
            return $sgdb;
        }
    }

}
