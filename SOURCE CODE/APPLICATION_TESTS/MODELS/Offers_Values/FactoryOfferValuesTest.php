<?php

require_once '../../../MODELS/Offers_Values/FactoryOfferValues.php';
require_once '../../../MODELS/Offers_Values/OfferValues.php';

class FactoryOfferValuesTest extends PHPUnit_Framework_TestCase {

    public function testOfferValues() {

        echo "testOfferValues(1)\n";
        $this->assertInstanceOf('OfferValues', FactoryOfferValues::offerValues("xml"));
        echo "testOfferValues(2)\n";
        $this->assertInstanceOf('OfferValues', FactoryOfferValues::offerValues("txt"));
        echo "testOfferValues(3)\n";
        $this->assertNotInstanceOf('OfferValues', FactoryOfferValues::offerValues("pdf"));
        echo "testOfferValues(4)\n";
        $this->assertNotInstanceOf('OfferValues', FactoryOfferValues::offerValues("doc"));
        echo "testOfferValues(5)\n";
        $this->assertNotInstanceOf('OfferValues', FactoryOfferValues::offerValues("jpg"));
        echo "testOfferValues(6)\n";
        $this->assertNotInstanceOf('OfferValues', FactoryOfferValues::offerValues("ppt"));
        echo "testOfferValues(7)\n";
        $this->assertNotInstanceOf('OfferValues', FactoryOfferValues::offerValues("XML"));
        echo "testOfferValues(8)\n";
        $this->assertNotInstanceOf('OfferValues', FactoryOfferValues::offerValues("TXT"));

        echo "testOfferValues(OK!)\n\n";
    }
}

?>
