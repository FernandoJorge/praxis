<?php

/**
 * IMPORTS
 */
require_once '../LOGS/Logs.php';
require_once '../DAL/DATABASES/FactoryDataBaseAccess.php';
require_once '../CONFIGS/twig_loader.php';

// REQUEST TO SAVE EMAIL´S SUBSCRIBER
if (isset($_REQUEST['emailNewsletter'])) {

    $email = $_REQUEST['emailNewsletter'];
    $email = trim($email);
    $email = filter_var($email, FILTER_SANITIZE_EMAIL);

    $valid = filter_var($email, FILTER_VALIDATE_EMAIL);

    if (!$valid) {

        ob_get_clean();
        ob_start();
        header("Content-Type: application/json");

        echo json_encode(array("result" => 'emailNewsletterNotValid'));
        exit;
    }

    // INSTANTIATES MYSQL ACCESS HANDLER
    $mySQL = FactoryDataBaseAccess::getSGDBConnection("mysql", "localhost:3306", "praxis", "root", "");

    // CHECK IF THE EMAIL'S SUBSCRIBER ALREADY EXISTS
    $SQL = "SELECT TRIM(email) FROM feed_community WHERE email = '" . $email . "'";

    $rows = $mySQL->select($SQL);

    if (!$rows && !is_array($rows)) {

        $mySQL->closeDBConnection();
        Logs::writeLog("ERROR GEETTING SUBSCRIBERS FROM 'FEED_COMMUNITY' TABLE DATABASE", true);

        ob_get_clean();
        ob_start();
        header("Content-Type: application/json");

        echo json_encode(array("result" => 'emailNewsletterSaveError'));
        exit;
    }

    if (count($rows) > 0) {

        $mySQL->closeDBConnection();
        Logs::writeLog("ATTEMPT TO SAVE AN EXISTING EMAIL'S SUBSCRIBER INTO 'FEED_COMMUNITY' TABLE DATABASE", true);

        ob_get_clean();
        ob_start();
        header("Content-Type: application/json");

        echo json_encode(array("result" => 'emailNewsletterExists'));
        exit;
    }

    $SQL = "INSERT INTO feed_community (email) "
            . "VALUES ('" . $email . "')";

    $result = $mySQL->insert($SQL, false);

    if (!$result) {

        $mySQL->closeDBConnection();
        Logs::writeLog("ERROR SAVING NEW EMAIL'S SUBSCRIBER INTO 'FEED_COMMUNITY' TABLE DATABASE", true);

        ob_get_clean();
        ob_start();
        header("Content-Type: application/json");

        echo json_encode(array("result" => 'emailNewsletterSaveError'));
        exit;
    }

    // SUCCESS
    $mySQL->commitDBChanges();
    $mySQL->closeDBConnection();

    Logs::writeLog("NEW EMAIL'S SUBSCRIBER SAVED INTO 'FEED_COMMUNITY' TABLE DATABASE");

    ob_get_clean();
    ob_start();
    header("Content-Type: application/json");

    echo json_encode(array("result" => 'emailNewsletterSaved'));
    exit;
}
?>
