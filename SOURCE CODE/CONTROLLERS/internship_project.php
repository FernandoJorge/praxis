<?php

/**
 * IMPORTS
 */
require_once '../CONFIGS/twig_loader.php';
require_once '../UTILS/utils.php';
require_once '../DAL/DATABASES/FactoryDataBaseAccess.php';
require_once '../MODELS/Offers_Persistence/Databases/FactoryDataBasePersistence.php';
require_once '../MODELS/Offers_Values/FactoryOfferValues.php';
require_once '../MODELS/Offers_Validation/Files_Validation.php';
require_once '../MODELS/Offer_Data_Validation/Offer_Data_Validation.php';
require_once '../MODELS/Offer_Templates/Offer_Templates.php';

/**
 * PROVIDER´S REQUEST FOR AUTHENTICATION
 */
if (isset($_REQUEST['username']) && isset($_REQUEST['password'])) {

    $usernameOrEmail = trim(sanitizeString($_REQUEST['username']));
    $password = trim(sanitizeString($_REQUEST['password']));

    // INSTANCIATES A MTSQL ACCESS HANDLER
    $mySQL = FactoryDataBaseAccess::getSGDBConnection("mysql", "localhost:3306", "praxis", "root", "");

    $SQL = "SELECT * FROM fos_user "
            . "WHERE username = '" . $usernameOrEmail . "' OR email_canonical = '" . $usernameOrEmail . "'";

    $infoProvider = $mySQL->select($SQL);

    if (!$infoProvider && !is_array($infoProvider)) {

        $mySQL->closeDBConnection();
        Logs::writeLog("ERROR GETTING PROVIDERS FROM 'FOS_USER' TABLE DATABASE", true);

        ob_get_clean();
        ob_start();
        header("Content-Type: application/json");

        echo json_encode(array("result" => "autheticationAccessError"));
        exit;
    }

    $userExists = false;
    $userID = "";
    $userName = "";
    $userFirstName = "";
    $userLastName = "";
    $userEmail = "";
    $userEnabled = "";
    $userLocked = "";
    $userExpired = "";

    foreach ($infoProvider as $provider) {

        if (($usernameOrEmail == $provider['username'] || $usernameOrEmail == $provider['email_canonical']) && password_verify($password, $provider['password'])) {

            $userExists = true;
            $userID = trim($provider['id']);
            $userName = trim($provider['username']);
            $userFirstName = trim($provider['firstName']);
            $userLastName = trim($provider['lastName']);
            $userEmail = trim($provider['email_canonical']);
            $userEnabled = trim($provider['enabled']);
            $userLocked = trim($provider['locked']);
            $userExpired = trim($provider['expired']);
            break;
        }
    }

    // USER EXISTS IN THE DATABASE
    if ($userExists) {

        // THE USER IS NOT DISABLED, LOCKED AND HIS REGISTRATION DIDN´T EXPIRED
        if ((int) $userEnabled == 1 && (int) $userLocked == 0 && (int) $userExpired == 0) {

            // STARTS SESSION TO THE PROVIDER USER
            session_start();

            // SAVES LOGIN´S DATETIME
            $SQL = "UPDATE fos_user "
                    . "SET last_login = NOW() "
                    . "WHERE id = " . $userID;

            $mySQL->update($SQL);
            $mySQL->commitDBChanges();
            $mySQL->closeDBConnection();

            $_SESSION['last_activity'] = time();
            $_SESSION['providerID'] = (int) $userID;
            $_SESSION['userFirstName'] = $userFirstName;
            $_SESSION['userLastName'] = $userLastName;
            $_SESSION['userEmail'] = $userEmail;

            // USER´S NAME TO SHOW ON THE PAGE
            $showName = $userFirstName . " " . $userLastName;
            $_SESSION['showName'] = $showName;

            if ($showName == " ") {

                $showName = $userName;
                $_SESSION['showName'] = $showName;
            }

            // DISPLAY PAGE TO SUBMIT PROVIDER´S PROJECT XML/PDF FORMAT
            ob_get_clean();
            ob_start();
            header("Content-Type: application/json");

            echo json_encode(array("result" => "authorized"));
            exit;
        } else {

            $mySQL->closeDBConnection();

            // INFORM USER THAT HE IS UNABLED, BLOCKED OR REGISTRATION IS EXPIRED
            $warning = " ";

            if ((int) $userEnabled == 0) {

                $warning = "enabled";
            } else {

                if ((int) $userLocked == 1) {

                    $warning = "locked";
                } else {

                    if ((int) $userExpired == 1) {

                        $warning = "expired";
                    }
                }
            }

            ob_get_clean();
            ob_start();
            header("Content-Type: application/json");

            echo json_encode(array("result" => $warning));
            exit;
        }
    } else {

        $mySQL->closeDBConnection();

        // USER´S AUTHENTICATION FAILED
        ob_get_clean();
        ob_start();
        header("Content-Type: application/json");

        echo json_encode(array("result" => "failed"));
        exit;
    }
}

/**
 *  REQUEST TO SHOW OFFERS SUBMISSION PAGE
 */
if (isset($_REQUEST['submission_page']) && $_REQUEST['submission_page'] == '1') {

    session_start();

    if (isset($_SESSION['providerID'])) {

        // CREATE ACTIVITY TIME POINT
        $_SESSION['last_activity'] = time();

        echo $twig->render('PROJECT/submit_project.html', array('username' => $_SESSION['showName']));
        exit;
    }

    session_destroy();
    echo $twig->render('PROJECT/authentic.html');
}

/**
 * PROVIDER´S REQUEST TO SAVE INTERNSHIP/PROJECT
 */
if (isset($_FILES['projectFile']['tmp_name'])) {

    session_start();

    if (!isset($_SESSION['providerID'])) {

        echo $twig->render('PROJECT/authentic.html');
        exit;
    }

    // TESTS IF OCCURRED SESSION TIMEOUT
    if (isset($_SESSION['last_activity']) && (time() - $_SESSION['last_activity']) > 600) {

        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]
            );
        }

        session_unset();
        session_destroy();

        echo $twig->render('PROJECT/authentic.html');
        exit;
    }

    // NEW ACTIVITY TIME POINT
    $_SESSION['last_activity'] = time();

    $exts = array('xml', 'txt');

    // SPLIT THE NAME´S FILE TO GET THE EXTENSION FORMAT
    $extension = explode(".", $_FILES['projectFile']['name']);
    $extension = end($extension);

    // TEST FILE´S EXTENSION FORMAT
    if (!in_array($extension, $exts)) {

        ob_get_clean();
        ob_start();
        header("Content-Type: application/json");

        echo json_encode(array("result" => 'InvalidFormatSubmission'));
        exit;
    }

    // GETS XML SCHEMA PROVIDER IF EXISTS
    // INSTANTIATES MYSQL ACCESS HANDLER
    $mySQL = FactoryDataBaseAccess::getSGDBConnection("mysql", "localhost:3306", "praxis", "root", "");

    $SQL = "SELECT provider_id , xml_schema_path FROM xml_schema "
            . "WHERE provider_id = " . $_SESSION['providerID'];

    $infoSchema = $mySQL->select($SQL);

    if (!$infoSchema && !is_array($infoSchema)) {

        $mySQL->closeDBConnection();

        ob_get_clean();
        ob_start();
        header("Content-Type: application/json");

        echo json_encode(array("result" => 'providerSchemaAccessError'));
        exit;
    }

    //TESTS IF THERE IS XML SCHEMA SET TO THE PROVIDER TO VALIDATE DE FILE
    if (count($infoSchema) == 0) {

        $mySQL->closeDBConnection();

        ob_get_clean();
        ob_start();
        header("Content-Type: application/json");

        echo json_encode(array("result" => 'noProviderSchemaError'));
        exit;
    }

    $providerXmlSchemaPath = $infoSchema[0]['xml_schema_path'];

    // TESTS IF THE FILE WAS MANUALLY DELETED
    if (!file_exists($providerXmlSchemaPath)) {

        // DELETES RECORD OF SCHEMA RELATIVE TO THE DELETED XML SCHEMA FILE
        $SQL = "DELETE FROM xml_schema "
                . "WHERE id = " . $infoSchema[0]['provider_id'];

        $mySQL->delete($SQL);
        $mySQL->commitDBChanges();
        $mySQL->closeDBConnection();

        ob_get_clean();
        ob_start();
        header("Content-Type: application/json");

        echo json_encode(array("result" => 'noSchemaFile'));
        exit;
    }

    // COPY FILE TO A TEMPORARY FOLDER 'TEMP'
    $sourceName = $_FILES['projectFile']['name'];
    $source = $_FILES['projectFile']['tmp_name'];
    $dest = $_SERVER['DOCUMENT_ROOT'] . "/PRAXIS/TEMP/" . $sourceName;

    $copyResult = copy($source, $dest);

    if (!$copyResult) {

        $mySQL->closeDBConnection();
        ob_get_clean();
        ob_start();
        header("Content-Type: application/json");

        echo json_encode(array("result" => 'fileCopyError'));
        exit;
    }

    // VALIDATES THE OFFER FILE
    $errors = Files_Validation::validateFileFormat($dest, $providerXmlSchemaPath);

    // TESTS IF THERE ARE ERRORS IN THE FILE
    if ($errors['result'] != 'valid') {

        $mySQL->closeDBConnection();
        unlink($dest);

        ob_get_clean();
        ob_start();
        header("Content-Type: application/json");

        echo json_encode($errors);
        exit;
    }

    // GETS OBJECT FOR MANAGE TO GET VALUES OF OFFER
    $offer = FactoryOfferValues::offerValues($extension);

    // GET VALUES OF THE OFFER
    $offerValues = $offer->getOfferFieldsValues($dest, $providerXmlSchemaPath);

    // VALIDATES DATA OF THE OFFER
    $result1 = Offer_Data_Validation::offerDataValidation($offerValues, $mySQL);

    // TESTS IF THERE ARE ANY INVALID DATA IN THE OFFER
    if ($result1['result'] != 'valid') {

        $mySQL->closeDBConnection();
        unlink($dest);

        ob_get_clean();
        ob_start();
        header("Content-Type: application/json");

        echo json_encode($result1);
        exit;
    }

    $mySQL->closeDBConnection();

    // PERSISTS ALL THE INFORMATION OF THE OFFER FILE TO DATABASE
    $offerPers = FactoryDataBasePersistence::persistData("mysql", "localhost:3306", "praxis", "root", "");
    $result2 = $offerPers->saveOffer($offerValues);

    unlink($dest);

    // RESPONSE TO USER - PERSISTENCE ERRORS/SUCCESS
    ob_get_clean();
    ob_start();
    header("Content-Type: application/json");

    echo json_encode($result2);
    exit;
}

/**
 * PROVIDER´S REQUEST TO GENERATE OFFER'S TEMPLATE
 */
if (isset($_REQUEST['template'])) {

    session_start();

    if (!isset($_SESSION['providerID'])) {

        echo $twig->render('PROJECT/authentic.html');
        exit;
    }

    // TESTS IF OCCURRED SESSION TIMEOUT
    if (isset($_SESSION['last_activity']) && (time() - $_SESSION['last_activity']) > 600) {

        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]
            );
        }

        session_unset();
        session_destroy();

        echo $twig->render('PROJECT/authentic.html');
        exit;
    }

    // NEW ACTIVITY TIME POINT
    $_SESSION['last_activity'] = time();

    // GETS XML SCHEMA PROVIDER IF EXISTS
    // INSTANTIATES MYSQL ACCESS HANDLER
    $mySQL = FactoryDataBaseAccess::getSGDBConnection("mysql", "localhost:3306", "praxis", "root", "");

    $SQL = "SELECT provider_id , xml_schema_path FROM xml_schema "
            . "WHERE provider_id = " . $_SESSION['providerID'];

    $infoSchema = $mySQL->select($SQL);

    if (!$infoSchema && !is_array($infoSchema)) {

        $mySQL->closeDBConnection();

        echo '<script language="javascript">'
        . 'alert("Occurred an error while trying to generate offer´s template...")'
        . '</script>';

        echo $twig->render('PROJECT/submit_project.html');
        exit;
    }

    if (count($infoSchema) == 0 || $infoSchema[0]['xml_schema_path'] == NULL) {

        $mySQL->closeDBConnection();

        echo '<script language="javascript">'
        . 'alert("At the moment you can´t generate an offer´s template. Contact Praxis network via \'www.praxisnetwork.eu/contact\' for help")'
        . '</script>';

        echo $twig->render('PROJECT/submit_project.html', array('username' => $_SESSION['showName']));
        exit;
    }

    // GENERATE XML TEMPLATE
    if ($_REQUEST['template'] == "xml") {

        $xmlTemplate = Offer_Templates::generateXML($infoSchema[0]['xml_schema_path']);
        
        if (!$xmlTemplate && gettype($xmlTemplate) != 'string') {

            $mySQL->closeDBConnection();

            echo '<script language="javascript">'
            . 'alert("It is not possible to generate the xml offer template...")'
            . '</script>';

            echo $twig->render('PROJECT/submit_project.html');
            exit;
        } else {

            file_put_contents($_SERVER['DOCUMENT_ROOT'] . "/PRAXIS/TEMP/xml_offer_template.xml", $xmlTemplate);
            downloadFile($_SERVER['DOCUMENT_ROOT'] . "/PRAXIS/TEMP/xml_offer_template.xml", "xml_offer_template.xml");
            unlink($_SERVER['DOCUMENT_ROOT'] . "/PRAXIS/TEMP/xml_offer_template.xml");
        }
    }

    // GENERATE PDF TEMPLATE
    if ($_REQUEST['template'] == "pdf") {

        $pdfTemplate = Offer_Templates::generatePDF($infoSchema[0]['xml_schema_path'], $mySQL);

        if (!$pdfTemplate) {

            $mySQL->closeDBConnection();

            echo '<script language="javascript">'
            . 'alert("It is not possible to generate the pdf offer template...")'
            . '</script>';

            echo $twig->render('PROJECT/submit_project.html');
            exit;
        } else {

            file_put_contents($_SERVER['DOCUMENT_ROOT'] . "/PRAXIS/TEMP/offer_template.pdf", $pdfTemplate);

            // CREATES ZIP WITH READ ME FILE AND TEMPLATE´S OFFER
            $zip = new ZipArchive();

            if (($zip->open($_SERVER['DOCUMENT_ROOT'] . "/PRAXIS/TEMP/offer_template.zip", ZipArchive::CREATE))) {

                $zip->addFile($_SERVER['DOCUMENT_ROOT'] . "/PRAXIS/FILES/OTHERS/READ ME BEFORE EDIT THE OFFER.txt", "READ ME BEFORE EDIT THE OFFER.txt");
                $zip->addFile($_SERVER['DOCUMENT_ROOT'] . "/PRAXIS/TEMP/offer_template.pdf", "offer_template.pdf");
                $zip->close();

                downloadFile($_SERVER['DOCUMENT_ROOT'] . "/PRAXIS/TEMP/offer_template.zip", "offer_template.zip");
                unlink($_SERVER['DOCUMENT_ROOT'] . "/PRAXIS/TEMP/offer_template.pdf");
                unlink($_SERVER['DOCUMENT_ROOT'] . "/PRAXIS/TEMP/offer_template.zip");
            }
        }
    }

    $mySQL->closeDBConnection();
}
?>
