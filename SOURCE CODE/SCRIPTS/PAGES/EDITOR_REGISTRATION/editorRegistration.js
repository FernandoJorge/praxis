function confirmPass() {

    var password = $("#password").val();
    var confPassword = $("#confPassword").val();

    if (password != confPassword) {

        $("#modal-message-header").html("Editor´s registration&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
        $("#modal-message-content").html("<p>Password´s confirmation doesn´t match...</p>");

        $("#message").modal({
            zIndex: 9999
        });
        $("#message").modal('show');

        return false;
    }

    return true;
}

function validateEmail() {

    var email = $("#editorEmail").val();

    var regExpres = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

    if (!regExpres.test(email)) {

        $("#editorEmail").val("");

        $("#modal-message-header").html("Editor´s registration&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
        $("#modal-message-content").html("<p>Invalid email address...</p>");

        $("#message").modal({
            zIndex: 9999
        });

        $("#message").modal('show');

        return false;
    }

    return true;
}

function saveRegistration() {

    $("#editorRegistration").ajaxSubmit({
        url: 'editor_regist',
        datatype: 'json',
        success: function (msg) {

            if (msg['result'] == 'editorRegistSaved') {

                $("#modal-message-header").html("Editor´s registration&nbsp;<span class='glyphicon glyphicon-thumbs-up'></span>");
                $("#modal-message-content").html("<p>Editor registration with success!</p>");

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'editorRegistFailed') {

                $("#modal-message-header").html("Editor´s registration&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>Occurred an error while saving the editor´s registration...</p>");

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            $('#editorRegistration')[0].reset();
        },
        error: function (xhr, status, error) {

            console.log(" xhr.responseText: " + xhr.responseText + " \n\nSTATUS:\n " + status + " \n\nERROR:\n " + error);

            $("#modal-message-header").html("Request error&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
            $("#modal-message-content").html("<p>It was not possible to make the request...</p>");

            $("#message").modal({
                zIndex: 9999
            });
            $("#message").modal('show');
        }
    });
}

$(document).ready(function () {

    // HANDLES FIRST REQUEST FOR SAVING A NEW EDITOR´S REGISTRATION
    $("#registerBt").on("click", function () {

        var fields = new Array();

        fields['Name'] = $("#editorName").val();
        fields['Email'] = $("#editorEmail").val();
        fields['Username'] = $("#username").val();
        fields['Password'] = $("#password").val();
        fields['Repeat password'] = $("#confPassword").val();

        var header = "The following field(s) is/are required:<br><br>";
        var body = "";

        // VALIDATIONS
        for (key in fields) {

            if (fields[key] == '') {

                body += "'" + key + "';<br>";
            }
        }

        if (body != '') {

            var message = header + body;

            $("#modal-message-header").html("Editor´s registration&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
            $("#modal-message-content").html("<p>" + message + "</p>");

            $("#message").modal({
                zIndex: 9999
            });
            $("#message").modal('show');

            return;
        }

        if (!validateEmail()) {
            return;
        }

        if (!confirmPass()) {
            return;
        }

        $("#confirmation").modal('show');

    });

    // HANDLES REQUEST FOR CONFIRMATION OF A NEW EDITOR´S REGISTRATION
    $("#confirmRegistrationBt").on("click", function () {

        $("#confirmation").modal('hide');
        saveRegistration();
    });

    // TESTS IF EDITOR ALREADY EXISTS
    $("#username").on("change", function () {

        var username = $("#username").val();

        $.ajax({
            url: 'editor_regist?editor_username=' + username,
            type: 'GET',
            datatype: 'json',
            success: function (msg) {

                if (msg['result'] == 'editorUsernameExists') {

                    $("#username").val("");

                    $("#modal-message-header").html("Editor´s registration&nbsp;<span class='glyphicon glyphicon-exlamation-sign'></span>");
                    $("#modal-message-content").html("<p>Username already exists...<br>Choose a different one.</p>");

                    $("#message").modal({
                        zIndex: 9999
                    });
                    $("#message").modal('show');
                }

                if (msg['result'] == 'editorUsernameExistsError') {

                    $("#username").val("");
                    $("#password").val("");

                    $("#modal-message-header").html("Editor´s registration&nbsp;<span class='glyphicon glyphicon-exlamation-sign'></span>");
                    $("#modal-message-content").html("<p>Occurred an error testing the availability of the username...</p>");

                    $("#message").modal({
                        zIndex: 9999
                    });
                    $("#message").modal('show');

                    $('#editorRegistration')[0].reset();
                }
            },
            error: function (xhr, status, error) {

                console.log(" xhr.responseText: " + xhr.responseText + " \n\nSTATUS:\n " + status + " \n\nERROR:\n " + error);

                $("#modal-message-header").html("Request error&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>It was not possible to make the request...</p>");

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }
        });
    });

    // TESTS IF EDITOR'S EMAIL ALREADY EXISTS
    $("#editorEmail").on("change", function () {

        var email = $("#editorEmail").val();

        $.ajax({
            url: 'editor_regist?editor_email=' + email,
            type: 'GET',
            datatype: 'json',
            success: function (msg) {

                if (msg['result'] == 'editorEmailExists') {

                    $("#editorEmail").val("");

                    $("#modal-message-header").html("Editor´s registration&nbsp;<span class='glyphicon glyphicon-exlamation-sign'></span>");
                    $("#modal-message-content").html("<p>Email already exists...<br>Choose a different one.</p>");

                    $("#message").modal({
                        zIndex: 9999
                    });
                    $("#message").modal('show');
                }

                if (msg['result'] == 'editorEmailExistsError') {

                    $("#username").val("");
                    $("#password").val("");

                    $("#modal-message-header").html("Editor´s registration&nbsp;<span class='glyphicon glyphicon-exlamation-sign'></span>");
                    $("#modal-message-content").html("<p>Occurred an error testing the availability of the username...</p>");

                    $("#message").modal({
                        zIndex: 9999
                    });
                    $("#message").modal('show');

                    $('#editorRegistration')[0].reset();
                }
            },
            error: function (xhr, status, error) {

                console.log(" xhr.responseText: " + xhr.responseText + " \n\nSTATUS:\n " + status + " \n\nERROR:\n " + error);

                $("#modal-message-header").html("Request error&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>It was not possible to make the request...</p>");

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }
        });
    });

    // TESTS IF THE PASSWORD AND CONFIRMATION PASSWORS MATCH(1)
    $("#confPassword").on("change", function () {

        var password = $("#password").val();
        var confPassword = $("#confPassword").val();

        if (password != '' && confPassword != '') {

            if (password != confPassword) {

                $("#modal-message-header").html("Editor´s registration&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>Password´s confirmation doesn´t match...</p>");

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }
        }
    });

    // TESTS IF THE PASSWORD AND CONFIRMATION PASSWORS MATCH(2)
    $("#password").on("change", function () {

        var password = $("#password").val();
        var confPassword = $("#confPassword").val();

        if (password != '' && confPassword != '') {

            if (password != confPassword) {

                $("#modal-message-header").html("Editor´s registration&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>Password´s confirmation doesn´t match...</p>");

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }
        }
    });

    // VALIDATES EMAIL
    $("#editorEmail").on("change", function () {

        var email = $("#editorEmail").val();

        var regExpres = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

        if (!regExpres.test(email)) {

            $("#editorEmail").val("");

            $("#modal-message-header").html("Editor´s registration&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
            $("#modal-message-content").html("<p>Invalid email address...</p>");

            $("#message").modal({
                zIndex: 9999
            });

            $("#message").modal('show');
        }
    });

});