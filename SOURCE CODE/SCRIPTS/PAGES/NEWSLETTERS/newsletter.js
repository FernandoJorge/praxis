/**
 * SCRIPT TO HANDLE EVENTS OF THE 'newsletter.html' page
 */

function saveEmail() {

    // VALIDATES SUBSCRIBER'S EMAIL
    var email = $("#emailNewsletter").val();

    if (email == '') {

        $("#modal-message-header").html("Newsletter subscription&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
        $("#modal-message-content").html("<p>Insert a email address...</p>");

        $("#message").modal({
            zIndex: 9999
        });

        $("#message").modal('show');

        return;
    }

    var regExpres = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

    if (!regExpres.test(email)) {

        $("#modal-message-header").html("Newsletter subscription&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
        $("#modal-message-content").html("<p>Invalid email address...</p>");

        $("#message").modal({
            zIndex: 9999
        });

        $("#message").modal('show');

        return;
    }

    $("#newsletterForm").ajaxSubmit({
        url: 'newsletters',
        datatype: 'json',
        success: function (msg) {

            if (msg['result'] == 'emailNewsletterSaved') {

                $("#emailNewsletter").val("");

                $("#modal-message-header").html("Newsletter subscription...");
                $("#modal-message-content").html("<p>Your email address was successfully saved.&nbsp;<span class='glyphicon glyphicon-thumbs-up'></span></p>");

                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'emailNewsletterExists') {

                debugger;

                $("#modal-message-header").html("NewsLetter subscription&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>The email address already exists in our database...</p>");
                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'emailNewsletterSaveError') {

                $("#modal-message-header").html("NewsLetter subscription&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>Occurred an error while saving the email address...</p>");
                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }

            if (msg['result'] == 'emailNewsletterNotValid') {

                $("#modal-message-header").html("NewsLetter subscription&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
                $("#modal-message-content").html("<p>Invalid email address...</p>");
                $("#message").modal({
                    zIndex: 9999
                });
                $("#message").modal('show');
            }
        },
        error: function (xhr, status, error) {

            console.log(" xhr.responseText: " + xhr.responseText + " \n\nSTATUS:\n " + status + " \n\nERROR:\n " + error);

            $("#modal-message-header").html("Request error&nbsp;<span class='glyphicon glyphicon-exclamation-sign'></span>");
            $("#modal-message-content").html("<p>It was not possible to make the request...</p>");

            $("#message").modal({
                zIndex: 9999
            });
            $("#message").modal('show');
        }
    });
}

