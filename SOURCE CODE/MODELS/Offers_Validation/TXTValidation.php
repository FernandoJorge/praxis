<?php

/**
 * IMPORTS
 */
require_once '/../../MODELS/XMLSchemaNodes/SchemaNodes.php';
require_once '/../../MODELS/Offer_Templates/Offer_Templates.php';

/**
 * CLASS OF SERVICE TO VALIDATE TXT FORMAT OFFERS
 */
class TXTValidation {

    /**
     * 
     * VALIDATES TXT OFFER FORMAT
     * 
     * @param String $txtPath - Path of the file to validate
     * @param String $schemaDocPath - Path of the Xml Schema document
     * @return String | boolean - 'corruptedFile' if txt file is not well formed | 
     * 'errorTXTValidation' if it not possible to validate the file | 
     * 'valid' if file is valid | 
     * false if it was not possible to generate the XML template.
     */
    static function fieldsNamesValidation($txtPath, $schemaDocPath) {

        $numFields = 0;

        // GETS ALL OFFER'S NODES
        $offerNodes = SchemaNodes::getOfferNodes();

        // GENERATES XML TEMPLATE
        $xmlTemplateContent = Offer_Templates::generateXML($schemaDocPath);
        
        if(!$xmlTemplateContent)
            false;

        file_put_contents($_SERVER['DOCUMENT_ROOT'] . "/PRAXIS/TEMP/xml_template.xml", $xmlTemplateContent);

        $xml = simplexml_load_file($_SERVER['DOCUMENT_ROOT'] . "/PRAXIS/TEMP/xml_template.xml");

        // GETS THE CORRECT FIELDS NAMES TO MATCH AGAINST THE SUBMITED OFFER
        $fieldElements = Offer_Templates::getXmlFieldNames($xml, $offerNodes);
        unlink($_SERVER['DOCUMENT_ROOT'] . "/PRAXIS/TEMP/xml_template.xml");

        $file = fopen($txtPath, 'r');

        if ($file) {

            // TESTS IF ALL FIELDS NAMES EXIST
            foreach ($fieldElements as $element) {

                while (!feof($file)) {

                    $line = trim(fgets($file));

                    if ($line == $element['fieldName'])
                        $numFields++;
                }

                fseek($file, 0);
            }

            if ($numFields != count($fieldElements))
                return "corruptedTXTFile";
        }else {

            fclose($file);
            return 'errorTXTValidation';
        }

        fclose($file);

        return "valid";
    }

}
