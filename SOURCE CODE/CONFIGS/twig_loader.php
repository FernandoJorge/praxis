<?php

/**
 * CONFIGURATION TO LOAD TEMPLATES USING TWIG TEMPLATES
 */

include '../TWIG-ENGINE/lib/Twig/Autoloader.php';
Twig_Autoloader::register();

$loader = new Twig_Loader_Filesystem("../VIEWS");
$twig = new Twig_Environment($loader);

?>
