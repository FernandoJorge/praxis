<?php

/**
 * IMPORTS
 */
require_once '../UTILS/utils.php';
require_once '../DAL/DATABASES/FactoryDataBaseAccess.php';
require_once '../CONFIGS/twig_loader.php';
require_once '../MODELS/XMLSchemaNodes/Document_XSchemaNodes.php';
require_once '../MODELS/XMLSchemaNodes/Institution_XSchemaNodes.php';
require_once '../MODELS/XMLSchemaNodes/Proposal_XSchemaNodes.php';
require_once '../MODELS/Offers_Validation/XML_XSDValidation.php';

/**
 * USER´S REQUEST FOR AUTHENTICATION
 */
if (isset($_REQUEST['username']) && isset($_REQUEST['password'])) {

    $usernameOrEmail = trim(sanitizeString($_REQUEST['username']));
    $password = trim(sanitizeString($_REQUEST['password']));

    // INSTANCIATES A MYSQL DATABASE ACCESS HANDLER
    $mysqlDB = FactoryDataBaseAccess::getSGDBConnection("mysql", "localhost:3306", "praxis", "root", "");

    // QUERY THE DATABASE TO GET THE ADMINSITRATOR ID
    $SQL = "SELECT * "
            . "FROM schema_editor "
            . "WHERE username = '" . $usernameOrEmail . "' OR email = '" . $usernameOrEmail . "'";

    $rows = $mysqlDB->select($SQL);

    if (!$rows && !is_array($rows)) {

        $mysqlDB->closeDBConnection();
        Logs::writeLog("ERROR GETTING XML SCHEMA EDITORS FROM 'SCHEMA_EDITOR' TABLE DATABASE", true);

        ob_get_clean();
        ob_start();
        header("Content-Type: application/json");

        echo json_encode(array("result" => 'authorizationError'));
        exit;
    }

    $name = "";

    foreach ($rows as $provider) {

        if (($usernameOrEmail == $provider['username'] || $usernameOrEmail == $provider['email'] ) && password_verify($password, $provider['password'])) {

            $name = trim($provider['name']);
            break;
        }
    }

    if (!empty($name)) {

        // START SESSION TO THE USER
        session_start();

        $_SESSION['last_activity'] = time();
        $_SESSION['username'] = $name;
        $_SESSION['editorID'] = (int) $rows[0]['id'];

        // SAVES LOGIN´S DATETIME
        $SQL = "UPDATE schema_editor "
                . "SET last_login = NOW() "
                . "WHERE id = " . $_SESSION['editorID'];

        $mysqlDB->update($SQL);
        $mysqlDB->commitDBChanges();
        $mysqlDB->closeDBConnection();

        ob_get_clean();
        ob_start();
        header("Content-Type: application/json");

        echo json_encode(array("result" => "authorized"));
        exit;
    } else {

        $mysqlDB->closeDBConnection();

        ob_get_clean();
        ob_start();
        header("Content-Type: application/json");

        echo json_encode(array("result" => "failed"));
        exit;
    }
}

/**
 * USER´S REQUEST TO DELETE PROVIDER´S XML SCHEMA
 */
if (isset($_REQUEST['provider_scheme_delete'])) {

    session_start();

    if (!isset($_SESSION['editorID'])) {
        echo $twig->render('XML_SCHEMA_EDITOR/authentic.html');
        exit;
    }

    // TESTS IF OCCURRED SESSION TIMEOUT
    if (isset($_SESSION['last_activity']) && (time() - $_SESSION['last_activity']) > 600) {

        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]
            );
        }

        session_unset();
        session_destroy();

        echo $twig->render('XML_SCHEMA_EDITOR/authentic.html');
        exit;
    }

    // NEW ACTIVITY TIME POINT
    $_SESSION['last_activity'] = time();

    // PROVIDER´S ID
    $providerID = (int) $_REQUEST['provider_scheme_delete'];

    // INSTANCIATES MYSQL ACCESS HANDLER
    $mySQL = FactoryDataBaseAccess::getSGDBConnection("mysql", "localhost:3306", "praxis", "root", "");

    $SQL = "SELECT provider_id , xml_schema_path FROM xml_schema "
            . "WHERE provider_id = " . $providerID;

    // GETS XML_SCHEMA ID THAT BELONGS TO THE PROVIDER AND PATH IN FILESYSTEM
    $xml_schema = $mySQL->select($SQL);

    if (!$xml_schema && !is_array($xml_schema)) {

        $mySQL->closeDBConnection();

        ob_get_clean();
        ob_start();
        header("Content-Type: application/json");

        echo json_encode(array("result" => "deleteAccessError"));
        exit;
    }

    // DELETES INFORMATION ABOUT XML SCHEMA
    if ($xml_schema[0]['provider_id'] != NULL && $xml_schema[0]['xml_schema_path'] != NULL) {

        $xml_schema_provider_id = (int) $xml_schema[0]['provider_id'];
        $xml_schema_path = $xml_schema[0]['xml_schema_path'];

        $SQL = "DELETE FROM xml_schema "
                . "WHERE provider_id = " . $xml_schema_provider_id;

        $deleteResult = $mySQL->delete($SQL);

        if ($deleteResult) {

            // DELETES XML SCHEMA FILE FROM THE FILESYSTEM
            $deleteFileResult = unlink($xml_schema_path);

            if ($deleteFileResult) {

                // COMMIT DB CHANGES
                $mySQL->commitDBChanges();

                $mySQL->closeDBConnection();

                ob_get_clean();
                ob_start();
                header("Content-Type: application/json");

                echo json_encode(array("result" => "success"));
                exit;
            } else {

                // ROLLBACK DB CHANGES
                $mySQL->rollbackDBChanges();

                $mySQL->closeDBConnection();

                ob_get_clean();
                ob_start();
                header("Content-Type: application/json");

                echo json_encode(array("result" => "failed"));
                exit;
            }
        } else {

            // ROLLBACK DB CHANGES
            $mySQL->rollbackDBChanges();

            $mySQL->closeDBConnection();

            ob_get_clean();
            ob_start();
            header("Content-Type: application/json");

            echo json_encode(array("result" => "failed"));
            exit;
        }
    }
}

/**
 * USER´S REQUEST FOR SHOW XML SCHEMA EDITOR´S PAGE
 */
if (isset($_REQUEST['provider_scheme_edit'])) {

    session_start();

    if (!isset($_SESSION['editorID'])) {
        echo $twig->render('XML_SCHEMA_EDITOR/authentic.html');
        exit;
    }

    // TESTS IF OCCURRED SESSION TIMEOUT
    if (isset($_SESSION['last_activity']) && (time() - $_SESSION['last_activity']) > 600) {

        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]
            );
        }

        session_unset();
        session_destroy();

        echo $twig->render('XML_SCHEMA_EDITOR/authentic.html');
        exit;
    }

    // NEW ACTIVITY TIME POINT
    $_SESSION['last_activity'] = time();

    // PROVIDER´S ID
    $_SESSION['providerID'] = (int) $_REQUEST['provider_scheme_edit'];
    $providerID = (int) $_REQUEST['provider_scheme_edit'];

    // INSTANCIATES A MYSQL ACCESS HANDLER
    $mySQL = FactoryDataBaseAccess::getSGDBConnection("mysql", "localhost:3306", "praxis", "root", "");

    // CHECK IF EXISTS XML SCHEMA FOR THE CURRENT PROVIDER
    $SQL = "SELECT provider_id , xml_schema_path FROM xml_schema "
            . "WHERE provider_id = " . $providerID;

    $xml_schema = $mySQL->select($SQL);

    $file = "";

    // LOAD PROVIDER´S XML SCHEMA FILE
    if (count($xml_schema) == 1 && $xml_schema[0]['provider_id'] != NULL && $xml_schema[0]['xml_schema_path'] != NULL) {

        // IF THE FILE WASN´T DELETED READS FILE FROM THE FILESYSTEM
        if (file_exists($xml_schema[0]['xml_schema_path'])) {

            $file = file_get_contents($xml_schema[0]['xml_schema_path']);
        } else {

            // DELETES FILE RECORD AND SHOWS A XML SCHEMA INITIAL STRUCTURE
            $SQL = "DELETE FROM xml_schema "
                    . "WHERE provider_id = " . $xml_schema[0]['provider_id'];

            $mySQL->delete($SQL);
            $mySQL->commitDBChanges();

            $file = Document_XSchemaNodes::getDocumentNode();
        }
    } else {

        // IF THERE´S NO XML SCHEMA SAVED FOR THE PROVIDER DISPLAYS A XML SCHEMA INITIAL STRUCTURE
        $file = Document_XSchemaNodes::getDocumentNode();
    }

    // CLOSE DB CONNECTION
    $mySQL->closeDBConnection();

    // PROPOSAL´S DETAILS HTML
    $proposalDetailsHTML = "<ul class='dropdown-menu' role='menu'>
                                <li><a onclick='getProposalElement(0)'><i>Title</i></a></li>
                                <li><a onclick='getProposalElement(1)'><i>Description</i></a></li>
                                <li><a onclick='getProposalElement(2)'><i>Study area</i></a></li>
                                <li><a onclick='getProposalElement(3)'><i>Study degree</i></a></li>
                                <li><a onclick='getProposalElement(4)'><i>Target</i></a></li>
                                <li><a onclick='getProposalElement(5)'><i>Language</i></a></li>
                                <li><a onclick='getProposalElement(6)'><i>Country</i></a></li>
                                <li><a onclick='getProposalElement(7)'><i>City</i></a></li>
                                <li><a onclick='getProposalElement(8)'><i>Contact</i></a></li>
                                <li><a onclick='getProposalElement(10)'><i>Supervisor first name</i></a></li>
                                <li><a onclick='getProposalElement(11)'><i>Supervisor last name</i></a></li>
                                <li><a onclick='getProposalElement(12)'><i>Supervisor email</i></a></li>
                                <li><a onclick='getProposalElement(13)'><i>Duration</i></a></li>
                                <li><a onclick='getProposalElement(14)'><i>Start date</i></a></li>
                                <li><a onclick='getProposalElement(15)'><i>End date</i></a></li>
                                <li><a onclick='getProposalElement(16)'><i>Vacancies</i></a></li>
                                <li><a onclick='getProposalElement(17)'><i>Payment</i></a></li>
                                <li><a onclick='getProposalElement(18)'><i>Global skill</i></a></li>
                                <li><a onclick='getProposalElement(19)'><i>Job skill required</i></a></li>
                                <li><a onclick='getProposalElement(20)'><i>Skill required</i></a></li>
                                <li><a onclick='getProposalElement(21)'><i>Skill to promote</i></a></li>
                                <li><a onclick='getProposalElement(22)'><i>Benefit</i></a></li>
                                <li><a onclick='getProposalElement(23)'><i>Expiration date</i></a></li>
                            </ul>";

    // INSTITUTION´S DETAILS HTML
    $institutionDetailsHTML = "<ul class='dropdown-menu' role='menu'>
                                    <li><a onclick='getInstitutionElement(0,0)'><i>Name</i></a></li>
                                    <li><a onclick='getInstitutionElement(1,0)'><i>Description</i></a></li>
                                    <li><a onclick='getInstitutionElement(2,0)'><i>Institution type</i></a></li>
                                    <li><a onclick='getInstitutionElement(3,0)'><i>Contact first name</i></a></li>
                                    <li><a onclick='getInstitutionElement(4,0)'><i>Contact last name</i></a></li>
                                    <li><a onclick='getInstitutionElement(5,0)'><i>Country</i></a></li>
                                    <li><a onclick='getInstitutionElement(6,0)'><i>City</i></a></li>
                                    <li><a onclick='getInstitutionElement(7,0)'><i>Phone</i></a></li>
                                    <li><a onclick='getInstitutionElement(8,0)'><i>Email</i></a></li>
                                    <li><a onclick='getInstitutionElement(9,0)'><i>Website</i></a></li>
                                </ul>";

    // HOST´S DETAILS HTML
    $hostDetailsHTML = "<ul class='dropdown-menu' role='menu'>
                                    <li><a onclick='getInstitutionElement(0,1)'><i>Name</i></a></li>
                                    <li><a onclick='getInstitutionElement(1,1)'><i>Description</i></a></li>
                                    <li><a onclick='getInstitutionElement(2,1)'><i>Host type</i></a></li>
                                    <li><a onclick='getInstitutionElement(3,1)'><i>Contact first name</i></a></li>
                                    <li><a onclick='getInstitutionElement(4,1)'><i>Contact last name</i></a></li>
                                    <li><a onclick='getInstitutionElement(5,1)'><i>Country</i></a></li>
                                    <li><a onclick='getInstitutionElement(6,1)'><i>City</i></a></li>
                                    <li><a onclick='getInstitutionElement(7,1)'><i>Phone</i></a></li>
                                    <li><a onclick='getInstitutionElement(8,1)'><i>Email</i></a></li>
                                    <li><a onclick='getInstitutionElement(9,1)'><i>Website</i></a></li>
                                </ul>";

    // XML SCHEMA STRUCTURE´S ELEMENTS DECALRATIONS HTML
    $xsdDocHTML = "<ul class='dropdown-menu' role='menu'>
                        <li><a onclick='getDocElement(0)'><i>XML schema document</i></a></li>
                        <li><a onclick='getDocElement(1)'><i>Simple element declaration</i></a></li>
                        <li><a onclick='getDocElement(2)'><i>Local simple element declaration</i></a></li>
                        <li><a onclick='getDocElement(3)'><i>Global simple element declaration</i></a></li>
                        <li><a onclick='getDocElement(4)'><i>Local complex type element</i></a></li>
                        <li><a onclick='getDocElement(5)'><i>Global complex type element</i></a></li>
                        <li><a onclick='getDocElement(6)'><i>Group element</i></a></li>
                        <li><a onclick='getDocElement(7)'><i>Sequence element</i></a></li>
                        <li><a onclick='getDocElement(8)'><i>Choice element</i></a></li>
                        <li><a onclick='getDocElement(9)'><i>Restriction operation element</i></a></li>
                        <li><a onclick='getDocElement(10)'><i>Data type to set an element type</i></a></li>
                    </ul>";

    // RENDERS THE PAGE FOR EDITING THE PROVIDER´S XML SCHEMA
    echo $twig->render('XML_SCHEMA_EDITOR/xml_schema_editor.html', array('file' => $file, 'proposal' => $proposalDetailsHTML, 'institution' => $institutionDetailsHTML, 'host' => $hostDetailsHTML, 'xsdDoc' => $xsdDocHTML, 'username' => $_SESSION['username']));
}

/**
 * USER´S REQUEST TO GO TO THE PROVIDER´S LIST
 */
if (isset($_REQUEST['list_providers']) && $_REQUEST['list_providers'] == '1') {

    session_start();

    if (isset($_SESSION['editorID'])) {

        // NEW ACTIVITY TIME POINT
        $_SESSION['last_activity'] = time();

        // SHOW PRAXIS PROVIDERS LIST
        $providersList = getProvidersList();

        echo $twig->render('XML_SCHEMA_EDITOR/choose_provider.html', array('tbody' => $providersList, 'username' => $_SESSION['username']));
        exit;
    }

    session_destroy();
    echo $twig->render('XML_SCHEMA_EDITOR/authentic.html');
}

/**
 * USER´S REQUEST FOR PROPOSAL ELEMENT NODE´S DECLARATION
 */
if (isset($_REQUEST['proposal_node'])) {

    session_start();

    // NEW ACTIVITY TIME POINT
    $_SESSION['last_activity'] = time();

    // NODE ID OF THE ELEMENT DECLARATION
    $nodeID = $_REQUEST['proposal_node'];

    $element = Proposal_XSchemaNodes::getDeclarationNode($nodeID);

    ob_get_clean();
    ob_start();
    header("Content-Type: application/json");

    echo json_encode(array("node" => $element));
    exit;
}

/**
 * USER´S REQUEST FOR INSTITUTION ELEMENT NODE´S DECLARATION
 */
if (isset($_REQUEST['institution_node']) && isset($_REQUEST['institution_type'])) {

    session_start();

    // NEW ACTIVITY TIME POINT
    $_SESSION['last_activity'] = time();

    // NODE ID/INSTITUTION TYPE OF THE ELEMENT DECLARATION
    $nodeID = $_REQUEST['institution_node'];
    $type = $_REQUEST['institution_type'];

    $element = Institution_XSchemaNodes::getDeclarationNode($nodeID, $type);

    ob_get_clean();
    ob_start();
    header("Content-Type: application/json");

    echo json_encode(array("node" => $element));
    exit;
}

/**
 * USER´S REQUEST FOR XML SCHEMA DOCUMENT ELEMENT NODE´S DECLARATION
 */
if (isset($_REQUEST['document_node'])) {

    session_start();

    // NEW ACTIVITY TIME POINT
    $_SESSION['last_activity'] = time();

    // NODE ID OF THE ELEMENT DECLARATION
    $nodeID = $_REQUEST['document_node'];

    $element = Document_XSchemaNodes::getDeclarationNode($nodeID);

    ob_get_clean();
    ob_start();
    header("Content-Type: application/json");

    echo json_encode(array("node" => $element));
    exit;
}

/**
 * USER´S REQUEST TO VALIDATE XML SCHEMA DOCUMENT
 */
if (isset($_REQUEST['schema_document']) && isset($_REQUEST['validateSave']) && $_REQUEST['validateSave'] == "validate") {

    session_start();

    if (!isset($_SESSION['editorID'])) {
        echo $twig->render('XML_SCHEMA_EDITOR/authentic.html');
        exit;
    }

    // NEW ACTIVITY TIME POINT
    $_SESSION['last_activity'] = time();

    $xsdDocument = $_REQUEST['schema_document'];

    // CREATES TEMPORARY XML SCHEMA
    $file = fopen($_SERVER['DOCUMENT_ROOT'] . "/PRAXIS/TEMP/temp.xsd", "w");

    // IF IT FAILS TO CREATE THE TEMPORARY FILE
    if (!$file) {

        ob_get_clean();
        ob_start();
        header("Content-Type: application/json");

        echo json_encode(array("result" => 'fileCreationError'));
        exit;
    }

    // WRITES TO THE FILE
    $writeResult = fwrite($file, $xsdDocument);

    // IF IT FAILS TO WRITE TO THE TEMPORARY FILE
    if (!$writeResult) {

        ob_get_clean();
        ob_start();
        header("Content-Type: application/json");

        echo json_encode(array("result" => 'fileWriteError'));
        exit;
    }

    // CLOSE FILE
    fclose($file);

    // VALIDATES XML SCHEMA SYNTAX
    $errors = XML_XSDValidation::checkDocSyntax($_SERVER['DOCUMENT_ROOT'] . "/PRAXIS/TEMP/temp.xsd");

    // DELETES TEMPORARY FILE
    unlink($_SERVER['DOCUMENT_ROOT'] . "/PRAXIS/TEMP/temp.xsd");

    // TESTS IF THERE´S ERRORS
    if (count($errors) == 0) {

        ob_get_clean();
        ob_start();
        header("Content-Type: application/json");

        echo json_encode(array("result" => 'validSyntax'));
        exit;
    } else {

        ob_get_clean();
        ob_start();
        header("Content-Type: application/json");

        echo json_encode(array("result" => 'invalidSyntax', 'errors' => $errors));
        exit;
    }
}

/**
 * USER´S REQUEST TO SAVE PROVIDER´S XML _SCHEMA
 */
if (isset($_REQUEST['schema_document']) && isset($_REQUEST['validateSave']) && $_REQUEST['validateSave'] == "save") {

    session_start();

    if (!isset($_SESSION['editorID'])) {
        echo $twig->render('XML_SCHEMA_EDITOR/authentic.html');
        exit;
    }

    // NEW ACTIVITY TIME POINT
    $_SESSION['last_activity'] = time();

    $xsdDocument = $_REQUEST['schema_document'];

    $editorID = $_SESSION['editorID'];
    $providerID = $_SESSION['providerID'];

    // CREATES INSTANCE OF MYSQL ACCESS HANDLER
    $mySQL = FactoryDataBaseAccess::getSGDBConnection("mysql", "localhost:3306", "praxis", "root", "");

    // CHECK IF EXISTS XML SCHEMA FOR THE CURRENT PROVIDER
    $SQL = "SELECT provider_id , xml_schema_path FROM xml_schema "
            . "WHERE provider_id = " . $providerID;

    $xml_schema = $mySQL->select($SQL);

    if (!$xml_schema && !is_array($xml_schema)) {

        $mySQL->closeDBConnection();

        ob_get_clean();
        ob_start();
        header("Content-Type: application/json");

        echo json_encode(array("result" => 'xmlSchemaAccessError'));
        exit;
    }

    // IF ALREADY EXISTS A XML SCHEMA TO THE PROVIDER
    if ($xml_schema[0]['provider_id'] != NULL && $xml_schema[0]['xml_schema_path'] != NULL) {

        // GETS MOFICIATION TYPE ID
        $SQL = "SELECT id FROM xml_modification_type "
                . "WHERE description = 'update'";

        $modificationID = $mySQL->select($SQL);

        if (!$modificationID && !is_array($modificationID)) {

            $mySQL->closeDBConnection();

            ob_get_clean();
            ob_start();
            header("Content-Type: application/json");

            echo json_encode(array("result" => 'xmlSchemaAccessError'));
            exit;
        }

        if ($modificationID[0]['id'] == NULL) {

            $mySQL->closeDBConnection();

            ob_get_clean();
            ob_start();
            header("Content-Type: application/json");

            echo json_encode(array("result" => 'saveModificationTypeUpdateError'));
            exit;
        }

        // INSERT NEW MODIFICATION TO THE PROVIDER´S XML SCHEMA
        $SQL = "INSERT INTO xml_schema_modification ( xml_schema_id , schema_editor_id , mod_type_id , mod_date ) "
                . "VALUES ( " . (int) $xml_schema[0]['provider_id'] . " , " . $editorID . " , " . (int) $modificationID[0]['id'] . " , NOW() ) ";

        $insertResult = $mySQL->insert($SQL);

        if (!$insertResult) {

            $mySQL->closeDBConnection();

            ob_get_clean();
            ob_start();
            header("Content-Type: application/json");

            echo json_encode(array("result" => 'saveModificationUpdateError'));
            exit;
        }

        // CREATE/OPEN PROVIDER´S XML SCHEMA FILE
        $file = fopen($xml_schema[0]['xml_schema_path'], "w");

        // IF IT FAILS TO CREATE/OPEN FILE
        if (!$file) {

            $mySQL->rollbackDBChanges();
            $mySQL->closeDBConnection();

            ob_get_clean();
            ob_start();
            header("Content-Type: application/json");

            echo json_encode(array("result" => 'saveFileCreationOpenError'));
            exit;
        }

        // WRITES TO THE FILE
        $writeResult = fwrite($file, $xsdDocument);

        // IF IT FAILS TO WRITE TO THE FILE
        if (!$writeResult) {

            $mySQL->rollbackDBChanges();
            $mySQL->closeDBConnection();

            ob_get_clean();
            ob_start();
            header("Content-Type: application/json");

            echo json_encode(array("result" => 'saveFileWriteError'));
            exit;
        }

        // CLOSE FILE
        fclose($file);

        // IF THERE IS NO ERROS THEN COMMIT CHANGES MADE TO DATABASE
        $mySQL->commitDBChanges();
        $mySQL->closeDBConnection();

        ob_get_clean();
        ob_start();
        header("Content-Type: application/json");

        echo json_encode(array("result" => 'xmlSchemaSaved'));
        exit;
    } else { // IF DOES NOT EXIST A XML SCHEMA TO THE PROVIDER
        // GETS MOFICIATION TYPE ID
        $SQL = "SELECT id FROM xml_modification_type "
                . "WHERE description = 'creation'";

        $modificationID = $mySQL->select($SQL);

        if (!$modificationID && !is_array($modificationID)) {

            $mySQL->closeDBConnection();

            ob_get_clean();
            ob_start();
            header("Content-Type: application/json");

            echo json_encode(array("result" => 'xmlSchemaAccessError'));
            exit;
        }

        if ($modificationID[0]['id'] == NULL) {

            $mySQL->closeDBConnection();

            ob_get_clean();
            ob_start();
            header("Content-Type: application/json");

            echo json_encode(array("result" => 'saveModificationTypeCreationError'));
            exit;
        }

        // CREATE NEW XML SCHEMA TO THE PROVIDER
        $SQL = "INSERT INTO xml_schema ( provider_id , xml_schema_path , creation_date ) "
                . "VALUES ( " . (int) $providerID . " , '" . $_SERVER['DOCUMENT_ROOT'] . "/PRAXIS/FILES/XML_SCHEMAS/schema_provider_" . $providerID . ".xsd' , NOW() )";

        // RETURNS THE ID OF THE NEW XML SCHEMA, OR FALSE
        $insertResult = $mySQL->insert($SQL, false);

        if (!$insertResult) {

            $mySQL->closeDBConnection();

            ob_get_clean();
            ob_start();
            header("Content-Type: application/json");

            echo json_encode(array("result" => 'saveInsertNewSchemaError'));
            exit;
        }

        // INSERT NEW MODIFICATION TO THE PROVIDER´S XML SCHEMA
        $SQL = "INSERT INTO xml_schema_modification ( xml_schema_id , schema_editor_id , mod_type_id , mod_date ) "
                . "VALUES ( " . (int) $providerID . " , " . $editorID . " , " . (int) $modificationID[0]['id'] . " , NOW() ) ";

        $insertResult = $mySQL->insert($SQL);

        if (!$insertResult) {

            $mySQL->closeDBConnection();

            ob_get_clean();
            ob_start();
            header("Content-Type: application/json");

            echo json_encode(array("result" => 'saveInsertModificationCreationError'));
            exit;
        }

        // CREATE/OPEN PROVIDER´S XML SCHEMA FILE
        $file = fopen($_SERVER['DOCUMENT_ROOT'] . "/PRAXIS/FILES/XML_SCHEMAS/schema_provider_" . $providerID . ".xsd", "w");

        // IF IT FAILS TO CREATE/OPEN FILE
        if (!$file) {

            $mySQL->rollbackDBChanges();
            $mySQL->closeDBConnection();

            ob_get_clean();
            ob_start();
            header("Content-Type: application/json");

            echo json_encode(array("result" => 'saveFileCreationOpenError'));
            exit;
        }

        // WRITES TO THE FILE
        $writeResult = fwrite($file, $xsdDocument);

        // IF IT FAILS TO WRITE TO THE FILE
        if (!$writeResult) {

            $mySQL->rollbackDBChanges();
            $mySQL->closeDBConnection();

            ob_get_clean();
            ob_start();
            header("Content-Type: application/json");

            echo json_encode(array("result" => 'saveFileWriteError'));
            exit;
        }

        // CLOSE FILE
        fclose($file);

        // IF THERE IS NO ERROS THEN COMMIT CHANGES MADE TO DATABASE
        $mySQL->commitDBChanges();
        $mySQL->closeDBConnection();

        ob_get_clean();
        ob_start();
        header("Content-Type: application/json");

        echo json_encode(array("result" => 'xmlSchemaSaved'));
        exit;
    }
}

/**
 * AUXILIAR METHODS
 */

/**
 * 
 * HTML TABLE´S ROWS PROVIDERS´ INFORMATION
 * 
 * @return String - HTML of the table´s rows with providers information
 */
function getProvidersList() {

    // INSTANCIATES A MYSQL DATABASE ACCESS HANDLER
    $mysqlDB = FactoryDataBaseAccess::getSGDBConnection("mysql", "localhost:3306", "praxis", "root", "");

    // QUERY THE DATABASE
    $SQL = "SELECT PR.id , PR.firstName , PR.lastName , PR.enabled , "
            . "CTR.name countryName , "
            . "CTY.name cityName , "
            . "IST.name institutionName , "
            . "XM.provider_id schemaID "
            . "FROM fos_user PR "
            . "LEFT OUTER JOIN country CTR "
            . "ON PR.country_id = CTR.id "
            . "LEFT OUTER JOIN city CTY "
            . "ON PR.city_id = CTY.id "
            . "LEFT OUTER JOIN institution IST "
            . "ON PR.institution_id = IST.id "
            . "LEFT OUTER JOIN xml_schema XM "
            . "ON PR.id = XM.provider_id "
            . "ORDER BY PR.firstName";

    $providersInfo = $mysqlDB->select($SQL);

    $mysqlDB->closeDBConnection();

    /**
     * BUILDS TABLE BODY HTML
     */
    $rowsTableBodyHTML = "";

    foreach ($providersInfo as $provider) {

        $rowsTableBodyHTML .= "<TR style='text-align: left'>"
                . "<TD>" . $provider['firstName'] . "</TD>"
                . "<TD>" . $provider['lastName'] . "</TD>"
                . "<TD>" . $provider['countryName'] . "</TD>"
                . "<TD>" . $provider['cityName'] . "</TD>"
                . "<TD>" . $provider['institutionName'] . "</TD>";

        if ($provider['enabled'] == 1) {

            $rowsTableBodyHTML .= "<TD>Enabled</TD>";
        } else {

            $rowsTableBodyHTML .= "<TD>Disabled</TD>";
        }

        if ($provider['schemaID'] == NULL) {

            $rowsTableBodyHTML .= "<TD><A class='btn btn-success' style='margin: 2px' href='editor?provider_scheme_edit=" . $provider['id'] . "' role='button'><I>CREATE </I></A></TD>";
        } else {

            $rowsTableBodyHTML .= "<TD id='action" . $provider['id'] . "'><BUTTON class='btn btn-danger' style='margin: 2px' type='button' onclick='confirmDeleteSchema(" . $provider['id'] . ")'><I>DELETE </I></BUTTON>"
                    . "<A class='btn btn-primary' style='margin: 2px' href='editor?provider_scheme_edit=" . $provider['id'] . "' role='button'><I>EDIT </I></A></TD>";
        }

        $rowsTableBodyHTML .= "</TR>";
    }

    return $rowsTableBodyHTML;
}
?>

