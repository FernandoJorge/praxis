<?php

/**
 * IMPORTS
 */
require_once '../UTILS/utils.php';
require_once '../LOGS/Logs.php';
require_once '../DAL/DATABASES/FactoryDataBaseAccess.php';
require_once '../CONFIGS/twig_loader.php';

/**
 * REQUEST TO SAVE A NEW EDITOR´S REGISTRATION
 */
if (isset($_REQUEST['editor_name']) && isset($_REQUEST['editor_email']) && isset($_REQUEST['username']) && isset($_REQUEST['password'])) {

    $editorName = trim(sanitizeString($_REQUEST['editor_name']));
    $email = trim(sanitizeString($_REQUEST['editor_email']));
    $username = trim(sanitizeString($_REQUEST['username']));
    $password = trim(sanitizeString($_REQUEST['password']));

    // HASH THE PASSWORD WITH BCRYPT METHOD
    $hashPassword = password_hash($password, PASSWORD_DEFAULT, ['cost' => 12]);

    if (!$hashPassword) {

        Logs::writeLog("ATTEMPT TO HASH PASSWORD TO SAVE NEW EDITOR´S REGISTRATION FAILED", true);

        ob_get_clean();
        ob_start();
        header("Content-Type: application/json");

        echo json_encode(array("result" => 'editorRegistFailed'));
        exit;
    }

    // INSTANTIATES MYSQL ACCESS HANDLER
    $mysqlDB = FactoryDataBaseAccess::getSGDBConnection("mysql", "localhost:3306", "praxis", "root", "");

    // SAVES NEW EDITOR´S RECORD
    $SQL = "INSERT INTO schema_editor ( name , email , username , password , creation_date ) "
            . "VALUES ('" . $editorName . "' , '" . $email . "', '" . $username . "' ,'" . $hashPassword . "' , NOW() )";

    $result = $mysqlDB->insert($SQL, false);

    if (!$result) {

        $mySQL->closeDBConnection();
        Logs::writeLog("ERROR SAVING NEW EDITOR´S REGISTRATION INTO 'SCHEMA_EDITOR' TABLE DATABASE", true);

        ob_get_clean();
        ob_start();
        header("Content-Type: application/json");

        echo json_encode(array("result" => 'editorRegistFailed'));
        exit;
    }

    // SUCCESS
    $mysqlDB->commitDBChanges();
    $mysqlDB->closeDBConnection();

    Logs::writeLog("NEW EDITOR´S REGISTRATION SAVED INTO 'SCHEMA_EDITOR' TABLE DATABASE");

    ob_get_clean();
    ob_start();
    header("Content-Type: application/json");

    echo json_encode(array("result" => 'editorRegistSaved'));
    exit;
}

/**
 * REQUEST FOR TESTING IF EDITOR'S USERNAME ALREADY EXISTS
 */
if (isset($_REQUEST['editor_username'])) {

    $username = trim(sanitizeString($_REQUEST['editor_username']));

    // INSTANTIATES MYSQL ACCESS HANDLER
    $mysqlDB = FactoryDataBaseAccess::getSGDBConnection("mysql", "localhost:3306", "praxis", "root", "");

    // TESTS IF EDITOR ALREADY EXISTS
    $SQL = "SELECT * FROM schema_editor WHERE username = '" . $username . "'";

    $rows = $mysqlDB->select($SQL);

    if (!$rows && !is_array($rows)) {

        $mysqlDB->closeDBConnection();
        Logs::writeLog("ERROR GETTING A SCHEMA_EDITOR´S RECORD FROM 'SCHEMA_EDITOR' TABLE DATABASE TO TEST EDITOR'S USERNAME IN ORDER TO REGISTER NEW XML SCHEMA EDITOR", true);

        ob_get_clean();
        ob_start();
        header("Content-Type: application/json");

        echo json_encode(array("result" => 'editorUsernameExistsError'));
        exit;
    }

    if (count($rows) > 0) {

        $mysqlDB->closeDBConnection();

        ob_get_clean();
        ob_start();
        header("Content-Type: application/json");

        echo json_encode(array("result" => 'editorUsernameExists'));
        exit;
    }

    $mysqlDB->closeDBConnection();
}

/**
 * REQUEST FOR TESTING IF EMAIL'S EDITOR ALREADY EXISTS
 */
if (isset($_REQUEST['editor_email'])) {

    $email = trim(sanitizeString($_REQUEST['editor_email']));

    // INSTANTIATES MYSQL ACCESS HANDLER
    $mysqlDB = FactoryDataBaseAccess::getSGDBConnection("mysql", "localhost:3306", "praxis", "root", "");

    // TESTS IF EDITOR ALREADY EXISTS
    $SQL = "SELECT * FROM schema_editor WHERE email = '" . $email . "'";

    $rows = $mysqlDB->select($SQL);

    if (!$rows && !is_array($rows)) {

        $mysqlDB->closeDBConnection();
        Logs::writeLog("ERROR GETTING A SCHEMA_EDITOR´S RECORD FROM 'SCHEMA_EDITOR' TABLE DATABASE TO TEST EMAIL'S EDITOR IN ORDER TO REGISTER NEW XML SCHEMA EDITOR", true);

        ob_get_clean();
        ob_start();
        header("Content-Type: application/json");

        echo json_encode(array("result" => 'editorEmailExistsError'));
        exit;
    }

    if (count($rows) > 0) {

        $mysqlDB->closeDBConnection();

        ob_get_clean();
        ob_start();
        header("Content-Type: application/json");

        echo json_encode(array("result" => 'editorEmailExists'));
        exit;
    }

    $mysqlDB->closeDBConnection();
}
?>

