<?php

/**
 * 
 * CLASS TO PROVIDE INFORMATION ABOUT SCHEMA NODES
 *
 */
class SchemaNodes {

    static private $offerSchemaNodes = array(
        'Title of the project/internship' => array('element' => 'PROPOSAL_TITLE', 'type' => 'text'),
        'Description of the project/internship' => array('element' => 'PROPOSAL_DESCRIPTION', 'type' => 'text_area'),
        'Study area for the project/internship' => array('element' => 'PROPOSAL_STUDY_AREA', 'type' => 'combo_box_sql', 'sql' => 'SELECT DISTINCT name FROM studyarea ORDER BY name', 'field' => 'name', 'show_items' => 3),
        'Study degree for the project/internship' => array('element' => 'PROPOSAL_STUDY_DEGREE', 'type' => 'combo_box_sql', 'sql' => 'SELECT DISTINCT name FROM studydegree ORDER BY name', 'field' => 'name', 'show_items' => 3),
        'Target of the project/internship' => array('element' => 'PROPOSAL_TARGET', 'type' => 'combo_box_sql', 'sql' => 'SELECT DISTINCT name FROM target ORDER BY name', 'field' => 'name', 'show_items' => 3),
        'Language required for the project/internship' => array('element' => 'PROPOSAL_LANGUAGE', 'type' => 'combo_box_sql', 'sql' => 'SELECT DISTINCT name FROM language ORDER BY name', 'field' => 'name', 'show_items' => 3),
        'Country of the project/internship' => array('element' => 'PROPOSAL_COUNTRY', 'type' => 'combo_box_sql', 'sql' => 'SELECT DISTINCT name FROM country ORDER BY name', 'field' => 'name'),
        'City of the project/internship' => array('element' => 'PROPOSAL_CITY', 'type' => 'combo_box_sql', 'sql' => 'SELECT DISTINCT name FROM city ORDER BY name', 'field' => 'name'),
        'Person contact for the project/internship' => array('element' => 'PROPOSAL_CONTACT', 'type' => 'text'),
        'Supervisor first name of the project/internship' => array('element' => 'PROPOSAL_SUPERVISORFSTNAME', 'type' => 'text'),
        'Supervisor last name of the project/internship' => array('element' => 'PROPOSAL_SUPERVISORLSTNAME', 'type' => 'text'),
        'Supervisor email of the project/internship' => array('element' => 'PROPOSAL_SUPERVISOREMAIL', 'type' => 'text'),
        'Duration of the project/internship(number of months)' => array('element' => 'PROPOSAL_DURATION', 'type' => 'combo_box_numbers'),
        'Starting date of the project/internship(ex: 2014-04-02)' => array('element' => 'PROPOSAL_STARTDATE', 'type' => 'text'),
        'Ending date of the project/internship(ex: 2014-04-02)' => array('element' => 'PROPOSAL_ENDDATE', 'type' => 'text'),
        'Expiration date of the project/internship(ex: 2014-04-02)' => array('element' => 'PROPOSAL_VALIDUNTIL', 'type' => 'text'),
        'Information about candidate vacancies for the project/internship' => array('element' => 'PROPOSAL_VACANCIES', 'type' => 'text'),
        'Information about candidate payment for the project/internship' => array('element' => 'PROPOSAL_PAYMENT', 'type' => 'text'),
        'Candidate global skill required for the project/internship' => array('element' => 'PROPOSAL_GLOBALSKILL', 'type' => 'text'),
        'Candidate job skill required for the project/internship' => array('element' => 'PROPOSAL_JOBSKILL', 'type' => 'text'),
        'Candidate skill required for the project/internship' => array('element' => 'PROPOSAL_REQUIREDSKILL', 'type' => 'combo_box_sql', 'sql' => 'SELECT DISTINCT name FROM skill ORDER BY name', 'field' => 'name', 'show_items' => 10),
        'Candidate skill to promote for the project/internship' => array('element' => 'PROPOSAL_PROMOTEDSKILL', 'type' => 'combo_box_sql', 'sql' => 'SELECT DISTINCT name FROM skill ORDER BY NAME', 'field' => 'name', 'show_items' => 10),
        'Information about candidate benefit for the project/internship' => array('element' => 'PROPOSAL_BENEFIT', 'type' => 'text'),
        'Name of the institution' => array('element' => 'INSTITUTION_NAME', 'type' => 'text'),
        'Description of the institution' => array('element' => 'INSTITUTION_DESCRIPTION', 'type' => 'text_area'),
        'Type of the institution' => array('element' => 'INSTITUTION_TYPE', 'type' => 'combo_box_sql', 'sql' => 'SELECT DISTINCT name FROM institutiontype ORDER BY name', 'field' => 'name'),
        'Person contact first name for the institution' => array('element' => 'INSTITUTION_CONTACTFSTNAME', 'type' => 'text'),
        'Person contact last name for the institution' => array('element' => 'INSTITUTION_CONTACTLSTNAME', 'type' => 'text'),
        'Country of the institution' => array('element' => 'INSTITUTION_COUNTRY', 'type' => 'combo_box_sql', 'sql' => 'SELECT DISTINCT name FROM country ORDER BY name', 'field' => 'name'),
        'City of the instittution' => array('element' => 'INSTITUTION_CITY', 'type' => 'combo_box_sql', 'sql' => 'SELECT DISTINCT name FROM city ORDER BY name', 'field' => 'name'),
        'Phone contact of the institution' => array('element' => 'INSTITUTION_PHONE', 'type' => 'text'),
        'Email contact of the institution' => array('element' => 'INSTITUTION_EMAIL', 'type' => 'text'),
        'Website of the institution' => array('element' => 'INSTITUTION_WEBSITE', 'type' => 'text'),
        'Name of the host' => array('element' => 'HOST_NAME', 'type' => 'text'),
        'Description of the host' => array('element' => 'HOST_DESCRIPTION', 'type' => 'text_area'),
        'Type of the host' => array('element' => 'HOST_TYPE', 'type' => 'combo_box_sql', 'sql' => 'SELECT DISTINCT name FROM institutiontype ORDER BY name', 'field' => 'name'),
        'Person contact first name for the host' => array('element' => 'HOST_CONTACTFSTNAME', 'type' => 'text'),
        'Person contact last name for the host' => array('element' => 'HOST_CONTACTLSTNAME', 'type' => 'text'),
        'Country of the host' => array('element' => 'HOST_COUNTRY', 'type' => 'combo_box_sql', 'sql' => 'SELECT DISTINCT name FROM country ORDER BY name', 'field' => 'name'),
        'City of the host' => array('element' => 'HOST_CITY', 'type' => 'combo_box_sql', 'sql' => 'SELECT DISTINCT name FROM city ORDER BY name', 'field' => 'name'),
        'Phone contact of the host' => array('element' => 'HOST_PHONE', 'type' => 'text'),
        'Email contact of the host' => array('element' => 'HOST_EMAIL', 'type' => 'text'),
        'Website of the host' => array('element' => 'HOST_WEBSITE', 'type' => 'text'),
    );

    /**
     * 
     * GETS ALL OFFER'S NODES
     * 
     * @return Array - Offer´s nodes
     */
    static function getOfferNodes() {

        return self::$offerSchemaNodes;
    }

}
