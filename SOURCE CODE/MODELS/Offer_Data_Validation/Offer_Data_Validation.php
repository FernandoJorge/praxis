<?php

/**
 * IMPORTS
 */
require_once '/../Offer/Offer.php';
require_once '/../../DAL/DATABASES/FactoryDataBaseAccess.php';

/**
 * Description of Offer_Data_Validation
 *
 * @author Janus
 */
class Offer_Data_Validation {

    /**
     * 
     * VALIDATES DATA OF AN OFFER
     * 
     * @param Offer $OfferValues - Offer object with all data´s offer
     * @param Object $mysqlDB - Database´s object reference
     * @return array - If data is valid returns array('result' => 'valid') | Otherwise returns array('result' => <error message>)
     */
    static function offerDataValidation($OfferValues, $mysqlDB) {


        /**
         * VALIDATIONS
         */
        // IMPORTANT INFORMATION THAT MUST EXIST IF A OFFER IS TO BE SAVED
        // ABOUT THE INSTITUTION
        if (empty($OfferValues->getInstitution()->getInstitutionName()) || empty($OfferValues->getInstitution()->getInstitutionType()) || empty($OfferValues->getInstitution()->getInstitutionPhone())) {

            $fields['incomplete_institution'] = array();

            if (empty($OfferValues->getInstitution()->getInstitutionName())) {


                array_push($fields['incomplete_institution'], "Institution´s name");
                Logs::writeLog("ATTEMPT TO SAVE NEW OFFER FAILED - INSTITUTION´S NAME EMPTY VALUE", true);
            }


            if (empty($OfferValues->getInstitution()->getInstitutionType())) {


                array_push($fields['incomplete_institution'], "Institution´s type");
                Logs::writeLog("ATTEMPT TO SAVE NEW OFFER FAILED - INSTITUTION´S TYPE EMPTY VALUE", true);
            }


            if (empty($OfferValues->getInstitution()->getInstitutionPhone())) {


                array_push($fields['incomplete_institution'], "Institution´s phone contact");
                Logs::writeLog("ATTEMPT TO SAVE NEW OFFER FAILED - INSTITUTION´S PHONE EMPTY VALUE", true);
            }

            return array("result" => 'incomplete_institution', 'fields' => $fields['incomplete_institution']);
        }

        // ABOUT THE HOST
        if (empty($OfferValues->getHost()->getInstitutionName()) || empty($OfferValues->getHost()->getInstitutionType()) || empty($OfferValues->getHost()->getInstitutionPhone())) {

            $fields['incomplete_host'] = array();

            if (empty($OfferValues->getHost()->getInstitutionName())) {


                array_push($fields['incomplete_host'], "Host´s name");
                Logs::writeLog("ATTEMPT TO SAVE NEW OFFER FAILED - HOST´S NAME EMPTY VALUE", true);
            }


            if (empty($OfferValues->getHost()->getInstitutionType())) {


                array_push($fields['incomplete_host'], "Host´s type");
                Logs::writeLog("ATTEMPT TO SAVE NEW OFFER FAILED - HOST´S TYPE EMPTY VALUE", true);
            }


            if (empty($OfferValues->getHost()->getInstitutionPhone())) {


                array_push($fields['incomplete_host'], "Host´s phone contact");
                Logs::writeLog("ATTEMPT TO SAVE NEW OFFER FAILED - HOST´S PHONE EMPTY VALUE", true);
            }

            return array("result" => 'incomplete_host', 'fields' => $fields['incomplete_host']);
        }

        // VALIDATES IF PROPOSAL´S COUNTRY AND CITY ARE VALID AND IF THE CITY BELONGS TO THE COUNTRY
        $result = self::validateCountryCity($OfferValues->getProposal()->getProposalCountry(), $OfferValues->getProposal()->getProposalCity(), $mysqlDB);

        if ($result == "no_country") {


            Logs::writeLog("ATTEMPT TO SAVE NEW OFFER FAILED - PROPOSAL´S COUNTRY OF THE OFFER IS INVALID", true);
            return array("result" => 'invalid_country_proposal');
        } else {

            if ($result == "no_city") {


                Logs::writeLog("ATTEMPT TO SAVE NEW OFFER FAILED - PROPOSAL´S CITY OF THE OFFER IS INVALID", true);
                return array("result" => 'invalid_city_proposal');
            } else {

                if ($result == "no_match") {


                    Logs::writeLog("ATTEMPT TO SAVE NEW OFFER FAILED - PROPOSAL´S COUNTRY AND CITY OF THE OFFER DON´T MATCH", true);
                    return array("result" => 'no_match_country_city_proposal');
                }
            }
        }

        // VALIDATES PROPOSAL´S START DATE
        $result = self::validateDateFomat($OfferValues->getProposal()->getProposalStartDate());

        if ($result == "wrong_date_format") {


            Logs::writeLog("ATTEMPT TO SAVE NEW OFFER FAILED - OFFER PROPOSAL´S START DATE FORMAT IS INVALID", true);
            return array("result" => 'start_date_wrong_date_format');
        }

        if ($result == "invalid_date") {


            Logs::writeLog("ATTEMPT TO SAVE NEW OFFER FAILED - OFFER PROPOSAL´S START DATE VALUE(S) IS(ARE) INVALID", true);
            return array("result" => 'start_date_invalid_date');
        }

        // VALIDATE PROPOSAL´S START DATE AGAINST CURRENT SUBMISSION DATE(TODAY)
        $result = self::validateSubmissionDate($OfferValues->getProposal()->getProposalStartDate());

        if ($result == "invalid_submission_date") {


            Logs::writeLog("ATTEMPT TO SAVE NEW OFFER FAILED - OFFER APPLICATION´S START DATE VALUE IS PRIOR TO CURRENT SUBMISSION´S DATE", true);
            return array("result" => 'invalid_start_submission_date');
        }

        // VALIDATES PROPOSAL´S END DATE
        $result = self::validateDateFomat($OfferValues->getProposal()->getProposalEndDate());

        if ($result == "wrong_date_format") {


            Logs::writeLog("ATTEMPT TO SAVE NEW OFFER FAILED - OFFER PROPOSAL´S END DATE FORMAT IS INVALID", true);
            return array("result" => 'end_date_wrong_date_format');
        }

        if ($result == "invalid_date") {


            Logs::writeLog("ATTEMPT TO SAVE NEW OFFER FAILED - OFFER PROPOSAL´S END DATE VALUE(S) IS(ARE) INVALID", true);
            return array("result" => 'end_date_invalid_date');
        }

        // VALIDATES PROPOSAL´S RANGE OF THE DATE
        $result = self::validateStartEndDatePeriod($OfferValues->getProposal()->getProposalStartDate(), $OfferValues->getProposal()->getProposalEndDate());

        if ($result == "invalid_date_period") {


            Logs::writeLog("ATTEMPT TO SAVE NEW OFFER FAILED - OFFER PROPOSAL´S START DATE AND END DATE SETS AN INVALID PERIOD", true);
            return array("result" => 'invalid_date_period');
        }

        // VALIDATES NUMBER OF MONTHS
        if (self::validateMonthNumbers($OfferValues->getProposal()->getProposalStartDate(), $OfferValues->getProposal()->getProposalEndDate(), $OfferValues->getProposal()->getProposalDuration()) == "invalid_duration_proposal") {


            Logs::writeLog("ATTEMPT TO SAVE NEW OFFER FAILED - PROPOSAL´S NUMBER OF MONTHS DURATION FOR THE OFFER DOES NOT MATCH THE START AND FINAL DATE", true);
            return array("result" => 'invalid_duration_proposal');
        }

        // VALIDATES PROPOSAL´S EXPIRATION DATE FORMAT
        $result = self::validateDateFomat($OfferValues->getProposal()->getProposalValidUntil());

        if ($result == "wrong_date_format") {


            Logs::writeLog("ATTEMPT TO SAVE NEW OFFER FAILED - OFFER PROPOSAL´S EXPIRATION DATE FORMAT IS INVALID", true);
            return array("result" => 'expiration_date_wrong_date_format');
        }

        if ($result == "invalid_date") {


            Logs::writeLog("ATTEMPT TO SAVE NEW OFFER FAILED - OFFER PORPOSAL´S EXPIRATION DATE VALUE(S) IS(ARE) INVALID", true);
            return array("result" => 'expiration_date_invalid_date');
        }

        // VALIDATES PROPOSAL´S EXPIRATION DATE AGAINST CURRENT SUBMISSION DATE(TODAY)
        $result = self::validateSubmissionDate($OfferValues->getProposal()->getProposalValidUntil());

        if ($result == "invalid_submission_date") {


            Logs::writeLog("ATTEMPT TO SAVE NEW OFFER FAILED - OFFER PROPOSAL´S EXPIRATION DATE VALUE IS PRIOR TO CURRENT SUBMISSION´S DATE", true);
            return array("result" => 'invalid_expiration_submission_date');
        }

        // VALIDATES IF PROPOSAL´S EXPIRATION DATE IS OLDER THAN PROPOSAL'S START DATE
        $result = self::validatePriorExpirationDateToStartDate($OfferValues->getProposal()->getProposalValidUntil(), $OfferValues->getProposal()->getProposalStartDate());

        if ($result == "invalid_expiration_start_date") {


            Logs::writeLog("ATTEMPT TO SAVE NEW OFFER FAILED - OFFER PROPOSAL´S EXPIRATION DATE IS LESS OLDER THAN PROPOSAL`S START DATE", true);
            return array("result" => 'invalid_expiration_start_date');
        }

        // VALIDATES IF INSTITUTION´S COUNTRY AND CITY ARE VALID AND IF THE CITY BELONGS TO THE COUNTRY
        $result = self::validateCountryCity($OfferValues->getInstitution()->getInstitutionCountry(), $OfferValues->getInstitution()->getInstitutionCity(), $mysqlDB);

        if ($result == "no_country") {


            Logs::writeLog("ATTEMPT TO SAVE NEW OFFER FAILED - INSTITUTION´S COUNTRY OF THE OFFER IS INVALID", true);
            return array("result" => 'invalid_country_institution');
        } else {

            if ($result == "no_city") {


                Logs::writeLog("ATTEMPT TO SAVE NEW OFFER FAILED - INSTITUTION´S CITY OF THE OFFER IS INVALID", true);
                return array("result" => 'invalid_city_institution');
            } else {

                if ($result == "no_match") {


                    Logs::writeLog("ATTEMPT TO SAVE NEW OFFER FAILED - INSTITUTION´S COUNTRY AND CITY OF THE OFFER DON´T MATCH", true);
                    return array("result" => 'no_match_country_city_institution');
                }
            }
        }

        // VALIDATES IF HOST´S COUNTRY AND CITY ARE VALID AND IF THE CITY BELONGS TO THE COUNTRY
        $result = self::validateCountryCity($OfferValues->getHost()->getInstitutionCountry(), $OfferValues->getHost()->getInstitutionCity(), $mysqlDB);

        if ($result == "no_country") {


            Logs::writeLog("ATTEMPT TO SAVE NEW OFFER FAILED - HOST´S COUNTRY OF THE OFFER IS INVALID", true);
            return array("result" => 'invalid_country_host');
        } else {

            if ($result == "no_city") {


                Logs::writeLog("ATTEMPT TO SAVE NEW OFFER FAILED - HOST´S CITY OF THE OFFER IS INVALID", true);
                return array("result" => 'invalid_city_host');
            } else {

                if ($result == "no_match") {


                    Logs::writeLog("ATTEMPT TO SAVE NEW OFFER FAILED - HOST´S COUNTRY AND CITY OF THE OFFER DON´T MATCH", true);
                    return array("result" => 'no_match_country_city_host');
                }
            }
        }

        // VALIDATES IF INSTITUTTION´S TYPE NAME EXISTS
        $result = self::validateInstitutionType($OfferValues->getInstitution()->getInstitutionType(), $mysqlDB);

        if ($result == "invalid") {


            Logs::writeLog("ATTEMPT TO SAVE NEW OFFER FAILED - INSTITUTION´S TYPE OF THE OFFER IS INVALID", true);
            return array("result" => 'invalid_institution_type');
        }

        // VALIDATES IF HOST´S TYPE NAME EXISTS
        $result = self::validateInstitutionType($OfferValues->getHost()->getInstitutionType(), $mysqlDB);

        if ($result == "invalid") {


            Logs::writeLog("ATTEMPT TO SAVE NEW OFFER FAILED - HOST´S TYPE OF THE OFFER IS INVALID", true);
            return array("result" => 'invalid_host_type');
        }

        // VALIDATES LANGUAGES
        $languagesResult['languages'] = self::validateLanguages($OfferValues->getProposal()->getProposalLanguages(), $mysqlDB);

        if (count($languagesResult['languages']) > 0) {


            Logs::writeLog("ATTEMPT TO SAVE NEW OFFER FAILED - PROPOSAL´S LANGUAGE(S) OF THE OFFER IS(ARE) INVALID", true);
            return array("result" => 'invalid_languages_proposal', 'languages' => $languagesResult['languages']);
        }

        // VALIDATES STUDY DEGREES
        $studyDegreeResult['study_degrees'] = self::validateStudyDegrees($OfferValues->getProposal()->getProposalStudyDegrees(), $mysqlDB);

        if (count($studyDegreeResult['study_degrees']) > 0) {


            Logs::writeLog("ATTEMPT TO SAVE NEW OFFER FAILED - PROPOSAL´S STUDY DEGREE(S) OF THE OFFER IS(ARE) INVALID", true);
            return array("result" => 'invalid_study_degrees_proposal', 'degrees' => $studyDegreeResult['study_degrees']);
        }

        // VALIDATES STUDY AREAS
        $studyAreasResult['study_areas'] = self::validateStudyAreas($OfferValues->getProposal()->getProposalStudyAreas(), $mysqlDB);

        if (count($studyAreasResult['study_areas']) > 0) {


            Logs::writeLog("ATTEMPT TO SAVE NEW OFFER FAILED - PROPOSAL´S STUDTY AREA(S) OF THE OFFER IS(ARE) INVALID", true);
            return array("result" => 'invalid_study_areas_proposal', 'areas' => $studyAreasResult['study_areas']);
        }

        // VALIDATES TARGETS
        $targetsResult['targets'] = self::validateTargets($OfferValues->getProposal()->getProposalTargets(), $mysqlDB);

        if (count($targetsResult['targets']) > 0) {


            Logs::writeLog("ATTEMPT TO SAVE NEW OFFER FAILED - PROPOSAL´S TARGET(S) OF THE OFFER IS(ARE) INVALID", true);
            return array("result" => 'invalid_targets_proposal', 'targets' => $targetsResult['targets']);
        }

        // VALIDATES PROMOTED SKILLS
        $promotedSkills['promoted_skills'] = self::validateSkills($OfferValues->getProposal()->getProposalPromotedSkills(), $mysqlDB);

        if (count($promotedSkills['promoted_skills']) > 0) {


            Logs::writeLog("ATTEMPT TO SAVE NEW OFFER FAILED - PROPOSAL´S SKILL(S) TO PROMOTE OF THE OFFER IS(ARE) INVALID", true);
            return array("result" => 'invalid_promoted_skills_proposal', 'skills' => $promotedSkills['promoted_skills']);
        }

        // VALIDATES REQUIRED SKILLS
        $requiredSkills['required_skills'] = self::validateSkills($OfferValues->getProposal()->getProposalRequiredSkills(), $mysqlDB);

        if (count($requiredSkills['required_skills']) > 0) {


            Logs::writeLog("ATTEMPT TO SAVE NEW OFFER FAILED - PROPOSAL´S SKILL(S) REQUIRED OF THE OFFER IS(ARE) INVALID", true);
            return array("result" => 'invalid_required_skills_proposal', 'skills' => $requiredSkills['required_skills']);
        }

        return array("result" => 'valid');
    }

    /**
     * 
     * VALIDATES IF A COUNTRY AND CITY ARE VALID AND IF THE CITY BELONGS TO THE COUNTRY
     * 
     * @param String $country - Country´s name
     * @param type $city - City´s name
     * @return String - 'no_country' if the country is not valid; 'no_city' if the city is not valid
     *                  'no_match' if the city doesn´t belong to the country
     */
    static private function validateCountryCity($country, $city, $mysqlDB) {

        $countryID = "";
        $cities;

        // IF COUNTRY ARGUMENT IS NOT EMPTY TESTS IF COUNTRY NAME EXISTS
        if (!empty($country)) {

            $SQL = "SELECT * FROM country WHERE TRIM(name) = '" . $country . "'";

            $countries = $mysqlDB->select($SQL);

            if (count($countries) == 0)
                return "no_country";

            $countryID = $countries[0]['id'];
        }

        // IF CITY ARGUMENT IS NOT EMPTY TESTS IF CITY NAME EXISTS
        if (!empty($city)) {

            $SQL = "SELECT * FROM city WHERE TRIM(name) = '" . $city . "'";

            $cities = $mysqlDB->select($SQL);

            if (count($cities) == 0)
                return "no_city";
        }

        // IF BOTH COUNTRY AND CITY ARE NOT EMPTY TESTS IF CITY BELONGS TO THE COUNTRY
        if (!empty($country) && !empty($city)) {

            if ($countryID != $cities[0]['country_id'])
                return "no_match";
        }

        return "valid";
    }

    /**
     * 
     * VALIDATES THE INSTITUTION OR HOST TYPE NAME
     * 
     * @param String $type - Institution/Host type´s name
     * @return String - 'invalid' if is not valid type name; 'valid' if the type´s name is valid
     */
    static private function validateInstitutionType($type, $mysqlDB) {

        // IF $TYPE ARGUMENT IS NOT EMPTY TESTS IF INSTITUTION TYPE NAME EXISTS
        if (!empty($type)) {

            $SQL = "SELECT * FROM institutiontype WHERE TRIM(name) = '" . $type . "'";

            $institutions = $mysqlDB->select($SQL);

            if (count($institutions) == 0)
                return "invalid";
        }

        return "valid";
    }

    /**
     * 
     * VALIDATES AN ARRAY OF LANGUAGES´ NAMES
     * 
     * @param Array $languages - Array of languages
     * @return Array/String - 'Array of invalid languages; Empty array if all languages are valid
     */
    static private function validateLanguages($languages, $mysqlDB) {

        $invalidLanguages = array();
        $invalid = false;

        // IF THERE´S LANGUAGES TESTS IF THEY ARE VALID
        if (count($languages) > 0) {

            foreach ($languages as $language) {

                if (!empty($language)) {

                    $SQL = "SELECT * FROM language WHERE TRIM(name) = '" . $language . "'";

                    $allLanguages = $mysqlDB->select($SQL);

                    if (count($allLanguages) == 0) {

                        $invalid = true;
                        array_push($invalidLanguages, $language);
                    }
                }
            }
        }

        // TESTS IF THERE AREA INVALID LANGUAGES
        if ($invalid)
            return array_unique($invalidLanguages);

        return array();
    }

    /**
     * 
     * VALIDATES AN ARRAY OF STUDY DEGREES´ NAMES
     * 
     * @param Array $degrees - Array of study degrees
     * @return Array/String - Array of invalid sutdy degrees; Empty array if all study degrees are valid
     */
    static private function validateStudyDegrees($degrees, $mysqlDB) {

        $invalidDegrees = array();
        $invalid = false;

        // IF THERE´S STUDY DEGREES TESTS IF THEY ARE VALID
        if (count($degrees) > 0) {

            foreach ($degrees as $degree) {

                if (!empty($degree)) {

                    $SQL = "SELECT * FROM studydegree WHERE TRIM(name) = '" . $degree . "'";

                    $allDegrees = $mysqlDB->select($SQL);

                    if (count($allDegrees) == 0) {

                        $invalid = true;
                        array_push($invalidDegrees, $degree);
                    }
                }
            }
        }

        // TESTS IF THERE ARE INVALID DEGREES
        if ($invalid)
            return array_unique($invalidDegrees);

        return array();
    }

    /**
     * 
     * VALIDATES AN ARRAY OF STUDY AREAS´ NAMES
     * 
     * @param Array $areas - Array of study degrees
     * @return Array/String - Array of invalid study areas; Empty array if all study degrees are valid
     */
    static private function validateStudyAreas($areas, $mysqlDB) {

        $invalidAreas = array();
        $invalid = true;

        // IF THERE´S STUDY AREAS TESTS IF THEY ARE VALID
        if (count($areas) > 0) {

            foreach ($areas as $area) {

                if (!empty($area)) {

                    $SQL = "SELECT * FROM studyarea WHERE TRIM(name) = '" . $area . "'";

                    $allAreas = $mysqlDB->select($SQL);

                    if (count($allAreas) == 0) {

                        $invalid = true;
                        array_push($invalidAreas, $area);
                    }
                }
            }
        }

        // TESTS IF THERE ARE INVALID AREAS
        if ($invalid)
            return array_unique($invalidAreas);

        return array();
    }

    /**
     * 
     * VALIDATES AN ARRAY OF TARGET´ NAMES
     * 
     * @param Array $targets - Array of targets
     * @return Array/String - Array of invalid targets; Empty array if all targets are valid
     */
    static private function validateTargets($targets, $mysqlDB) {

        $invalidTargets = array();
        $invalid = true;

        // IF THERE´S TARGETS TESTS IF THEY ARE VALID
        if (count($targets) > 0) {

            foreach ($targets as $target) {

                if (!empty($target)) {

                    $SQL = "SELECT * FROM target WHERE TRIM(name) = '" . $target . "'";

                    $allTargets = $mysqlDB->select($SQL);

                    if (count($allTargets) == 0) {

                        $invalid = true;
                        array_push($invalidTargets, $target);
                    }
                }
            }
        }

        // TESTS IF THERE ARE INVALID TARGETS
        if ($invalid)
            return array_unique($invalidTargets);

        return array();
    }

    /**
     * 
     * VALIDATES AN ARRAY OF SKILLS´ NAMES
     * 
     * @param Array $skills - Array of skills
     * @return Array/String - Array of invalid skills; Empty array if all skills are valid
     */
    static private function validateSkills($skills, $mysqlDB) {

        $invalidSkills = array();
        $invalid = true;

        // IF THERE´S SKILLS TESTS IF THEY ARE VALID
        if (count($skills) > 0) {

            foreach ($skills as $skill) {

                if (!empty($skill)) {

                    $SQL = "SELECT * FROM skill WHERE TRIM(name) = '" . $skill . "'";

                    $allSkills = $mysqlDB->select($SQL);

                    if (count($allSkills) == 0) {

                        $invalid = true;
                        array_push($invalidSkills, $skill);
                    }
                }
            }
        }

        // TESTS IF THERE ARE INVALID TARGETS
        if ($invalid)
            return array_unique($invalidSkills);

        return array();
    }

    /**
     * 
     * VALIDATES A DATA IN THE FORMAT 'YYYY-MM-DD'
     * 
     * @param String $date
     * @return String|boolean - 'valid' if success / 'wrong_date_format' if incorrect format / 'invalid_date' if it´s not a valid date
     */
    static private function validateDateFomat($date) {

        if (!empty($date)) {

            // CHECK DATE FORMAT('YYYY-MM-DD')
            if (!preg_match('/^\d{4}-\d{2}-\d{2}$/i', $date)) {

                return "wrong_date_format";
            }

            // CHECK IF IS A GREGORIAN DATE
            $ymd = explode("-", $date);

            if (!checkdate($ymd[1], $ymd[2], $ymd[0])) {
                return "invalid_date";
            }
        }

        return "valid";
    }

    /**
     * 
     * TESTS IF THE PERIOD BY THE START DATE AND END DATE IS VALID
     * 
     * @param String $startDate
     * @param String $endDate
     * @return string|boolean - 'valid' if success / 'invalid_date_range' if fails
     */
    static private function validateStartEndDatePeriod($startDate, $endDate) {

        if (!empty($startDate) && !empty($endDate)) {

            $date1 = strtotime($startDate);
            $date2 = strtotime($endDate);

            if ($date2 - $date1 < 0) {
                return "invalid_date_period";
            }
        }

        return "valid";
    }

    /**
     * 
     * VALIDATES A DATE AGAINST THE CURRENT DATE OF SUBMISSON
     * 
     * @param String $date - date
     * @return String - 'invalid_submission_date' if fails | 'valid' if success
     */
    static private function validateSubmissionDate($date) {

        if (!empty($date)) {

            // GET CURRENT DATE
            $currentDate = date("Y-m-d");
            $currentDate = strtotime($currentDate);

            $dateTest = strtotime($date);

            if ($dateTest - $currentDate < 0) {
                return "invalid_submission_date";
            }
        }

        return "valid";
    }

    /**
     * 
     * TESTS IF EXPIRATION DATE IS INFERIOR TO START DATE FOR THE PROPOSAL
     * 
     * @param String $expirationDate - Validation date for the proposal
     * @param String $startDate - Start date for the proposal
     * @return String - 'invalid_expiration_start_date' if expiration date is superior to start date | 'valid' if otherwise
     */
    static private function validatePriorExpirationDateToStartDate($expirationDate, $startDate) {

        if (!empty($expirationDate) && !empty($startDate)) {

            $date1 = strtotime($expirationDate);
            $date2 = strtotime($startDate);

            if ($date2 - $date1 < 0) {

                return "invalid_expiration_start_date";
            }
        }

        return "valid";
    }

    /**
     * 
     * VALIDATES THE NUMBER OF MONTHS IN THE OFFER
     * 
     * @param String $startDate - Proposal start date
     * @param String $endDate - Proposal end date
     * @param String $monthsNumber - Number of months of the proposal
     * @return String - 'valid' if the number of months is correct | 
     * 'invalid_duration_proposal' if the number of months is incorrect
     */
    static private function validateMonthNumbers($startDate, $endDate, $monthsNumber) {

        $d1 = new DateTime($startDate);
        $d2 = new DateTime($endDate);
        $difference = $d1->diff($d2);
        $monthsDiff = ceil($difference->y * 12 + $difference->m + $difference->d / 30);


        if ($monthsDiff != $monthsNumber)
            return "invalid_duration_proposal";

        return "valid";
    }

}
