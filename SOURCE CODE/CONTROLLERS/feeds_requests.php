<?php

/**
 * IMPORTS
 */
require_once '../LOGS/Logs.php';
require_once '../DAL/DATABASES/FactoryDataBaseAccess.php';
require_once '../CONFIGS/twig_loader.php';

/**
 * RSS FEEDS AGGREGATOR CLIENT'S REQUESTS
 */
if (isset($_REQUEST['info']) && $_REQUEST['info'] == "rss") {

    Logs::writeLog("(XML RSS FEED UPDATE) RSS FEED UPDATE REQUEST", false);

    // INSTANTIATES MYSQL ACCESS HANDLER
    $mySQL = FactoryDataBaseAccess::getSGDBConnection("mysql", "localhost:3306", "praxis", "root", "");

    // GETS THE FILE PATH OF THE XML RSS FEED FILE UPDATE
    $SQL = "SELECT feed_xml_path FROM feed_information";

    $info = $mySQL->select($SQL);
    
    if(!$info && !is_array($info)){
        
        $mySQL->closeDBConnection();
        Logs::writeLog("(XML RSS FEED UPDATE) ERROR GETTING INFORMATION ABOUT THE XML RSS FEED FILE IN DATABASE", true);
        exit;
    }
    
    $mySQL->closeDBConnection();

    // TEST IF THERE IS A RECORD INFORMATION ABOUT XML RSS FEED FILE
    if (count($info) == 0) {

        Logs::writeLog("(XML RSS FEED UPDATE) THERE'S NO INFORMATION ABOUT THE XML RSS FEED FILE IN DATABASE", true);
        exit;
    }

    $xmlFilePath = $info[0]['feed_xml_path'];

    // TESTS IF XML RSS FEED FILE PATH IS SET
    if ($xmlFilePath == "") {

        Logs::writeLog("(XML RSS FEED UPDATE) XML RSS FEED FILE PATH IN DATABASE IS NOT SET", true);
        exit;
    }

    // TESTS IF THE XML RSS FEED FILE EXISTS IN THE FILE SYSTEM
    if (!file_exists($xmlFilePath)) {

        Logs::writeLog("(XML RSS FEED UPDATE) XML RSS FEED FILE DOES NOT EXIST IN FILE SYSTEM", true);
        exit;
    }

    $rssFeedContent = file_get_contents($xmlFilePath);

    header("Content-type: application/rss+xml; charset=utf-8");

    echo $rssFeedContent;
    exit;
}
?>
