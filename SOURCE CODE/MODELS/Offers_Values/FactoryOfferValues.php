<?php

/**
 * IMPORTS
 */
require_once 'Txt_Offer.php';
require_once 'Xml_Offer.php';

/**
 * FACTORY TO CREATE A OBJECT TO HANDLE THE DATA OF AN OFFER
 */
class FactoryOfferValues {

    /**
     * 
     * OBJECT TO TO HANDLE THE DATA OF AN OFFER
     * 
     * @param String $format - file format extension
     * @return \Xml_Offer|\Txt_Offer - Object to handle the data of the offer
     */
    static function offerValues($format) {

        if ($format == "txt") {

            return new Txt_Offer();
        }

        if ($format == "xml") {

            return new Xml_Offer();
        }
    }

}
